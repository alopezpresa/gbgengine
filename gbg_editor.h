#ifndef GBG_EDITOR_H
#define GBG_EDITOR_H

//From where is this selection coming from?
//From the editor imprints or the in game entities?
typedef enum selection_mode
{
	SM_Imprint,
	SM_EntityLevel,
} selection_mode;

typedef struct selected_imprint
{
	imprint Imprint;
	selection_mode Mode;
} selected_imprint;

typedef struct editor_config
{
	b32 HideEntityCreation;
	b32 HideEntityEdit;
	b32 AllowOnlyGameDataEdit;
	char **HideImprints;
} editor_config;

typedef struct editor
{
	b32 Show;
	//TODO: Is that still needed?
	struct rtti_record *NewSelected;

	char NameOut[64];
	void *TempObject;
	u32 *ListBoxIndices;
	u32 ListBoxCurIndex;
	char **RecordNames;
	char **TextureNames;
	char *LevelsDir;

	selected_imprint LastEntityClicked;
	
	imprint *SavedImprints;

	//For every entity placed in the world is needed to know from which imprint, 
	//if any is that one coming from.
	imprint *InWorldImprints;

	char *CurrentWorldName;

	b32 *CollapsedStatus;
	u32 CollapseCount;

	editor_config Config;
}editor;

void editor_init(struct engine *Engine);
void editor_update(struct engine *Engine);
void editor_show_record(struct engine *Engine, struct rtti_record *Struct, void *Obj, vec2 *StartPos);

#endif
