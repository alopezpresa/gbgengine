#ifndef GBG_TYPES_H
#define GBG_TYPES_H

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef char s8;
typedef short s16;
typedef int s32;
typedef long long s64;

typedef int b32;

typedef float f32;
typedef double f64;

typedef unsigned short wchar;

#define KB(X) (1024 * X)
#define MB(X) (1024 * KB(X))
#define GB(X) (1024 * MB(X))

//TODO: Make assert that prints a message
#define assert(Condition) {if (!(Condition)) *((u32*)0) = 0;}

#define GOBJECT(...)
#define GENUM(...)

#endif
