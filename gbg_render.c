
#include "gbg_engine.h"
#include "gbg_render.h"
#include "gbg_utils.h"
#include "gbg_platform.h"
#include "gbg_math.h"
#include "gbg_string.h"
#include "gbg_imgui.h"

#include <stddef.h>

static void GLCheckErrors(engine *Engine)
{
	struct opengl *GL = &Engine->Render->OpenGL;
	GLenum Err;
	while(1)
	{
		Err = GL->GetError();
		switch(Err)
		{
			case GL_NO_ERROR:
				return;
			case GL_INVALID_ENUM:
				Engine->Platform->Log("GL Error GL_INVALID_ENUM");
				break;
			case GL_INVALID_VALUE:
				Engine->Platform->Log("GL Error GL_INVALID_VALUE");
				break;
			case GL_INVALID_OPERATION:
				Engine->Platform->Log("GL Error GL_INVALID_OPERATION");
				break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				Engine->Platform->Log("GL Error GL_INVALID_FRAMEBUFFER_OPERATION");
				break;
			case GL_OUT_OF_MEMORY:
				Engine->Platform->Log("GL Error GL_OUT_OF_MEMORY");
				break;
//TODO: Handle better this defines. Maybe a defines.h 
#ifndef RENDER_GLES
			case GL_STACK_UNDERFLOW:
				Engine->Platform->Log("GL Error GL_STACK_UNDERFLOW");
				break;
			case GL_STACK_OVERFLOW:
				Engine->Platform->Log("GL Error GL_STACK_OVERFLOW");
				break;
#endif
			default:
				Engine->Platform->Log("GL Error UNKNOWN >[");
				break;
		}
	}
}

static void render_create_mesh_program(engine *Engine)
{
	render *Render = Engine->Render;
	if (Render->Shaders[ShaderProgramType_Mesh])
		return;

#ifdef RENDER_GLES
	shader_def Shaders[] = {{"shaders/gles/basic.frag", GL_FRAGMENT_SHADER},
		{"shaders/gles/basic.vert", GL_VERTEX_SHADER}};
#else
	shader_def Shaders[] = {{"shaders/gl/basic.frag", GL_FRAGMENT_SHADER},
		{"shaders/gl/basic.vert", GL_VERTEX_SHADER}};
#endif

	Render->Shaders[ShaderProgramType_Mesh] = render_create_program(Engine, Shaders, static_array_len(Shaders));

	GLCheckErrors(Engine);
}

void render_create_quad_mesh(engine *Engine)
{
	render *Render = Engine->Render;
	render_create_mesh_program(Engine);
	vec4 White = Vec4(1, 1, 1, 1);

	vertex_pc Vertices[] = {{Vec3(-0.5f, -0.5f, 0), White},
		{Vec3(-0.5f, 0.5f, 0), White},
		{Vec3(0.5f, 0.5f, 0), White},
		{Vec3(0.5f, -0.5f, 0), White}};
	u16 Elements[] = {0, 1, 3,
					3, 1, 2	};

	Render->QuadMesh = render_create_mesh(Engine, Vertices, static_array_len(Vertices), Elements, static_array_len(Elements));

	GLCheckErrors(Engine);
}

void render_create_circle_mesh(engine *Engine)
{
	render *Render = Engine->Render;
	render_create_mesh_program(Engine);

	vertex_pc Vertices[64] = {0};
	u32 VerticesLen = 0;
	u16 Elements[128] = {0};
	u32 ElementsLen = 0;

	vec4 White = Vec4(1, 1, 1, 1);

	vertex_pc *Vertex = Vertices + VerticesLen++;
	Vertex->Pos = Vec3(0, 0, 0);
	Vertex->Color = White;

	Elements[ElementsLen++] = 0;

	u32 StepCount = 20;
	f32 StepInc = PI_2 / StepCount;

	f32 StartX = cosf(0);
	f32 StartY = sinf(0);
	Vertex = Vertices + VerticesLen++;
	Vertex->Pos = Vec3(StartX, StartY, 0);
	Vertex->Color = White;

	Elements[ElementsLen++] = 2;
	Elements[ElementsLen++] = 1;

	u16 ElementPos = 2;
	for (u32 i=1; i<StepCount; ++i)
	{
		f32 y = sinf(i*StepInc);
		f32 x = cosf(i*StepInc);

		Vertex = Vertices + VerticesLen++;
		Vertex->Pos = Vec3(x, y, 0);
		Vertex->Color = White;

		if (i != StepCount-1)
		{
			Elements[ElementsLen++] = 0;
			Elements[ElementsLen++] = ++ElementPos;
			Elements[ElementsLen++] = ElementPos-1;
		}
	}

	Elements[ElementsLen++] = 0;
	Elements[ElementsLen++] = 1;
	Elements[ElementsLen++] = ElementPos;

	Render->CircleMesh = render_create_mesh(Engine, Vertices, VerticesLen, Elements, ElementsLen);

	GLCheckErrors(Engine);
}

#ifdef RENDER_GLES
static void render_bind_gles_functions(struct opengl *GL)
{
	GL->CreateShader = glCreateShader;
	GL->ShaderSource = glShaderSource;
	GL->CompileShader = glCompileShader;
	GL->GetShaderiv = glGetShaderiv;
	GL->GetString = glGetString;
	GL->GetStringi = glGetStringi;
	GL->GetShaderInfoLog = glGetShaderInfoLog;
	GL->DeleteShader = glDeleteShader;
	GL->DeleteVertexArrays = glDeleteVertexArrays;
	GL->DeleteBuffers = glDeleteBuffers;
	GL->CreateProgram = glCreateProgram;
	GL->AttachShader = glAttachShader;
	GL->LinkProgram = glLinkProgram;
	GL->GetProgramiv = glGetProgramiv;
	GL->GetActiveUniform = glGetActiveUniform;
	GL->GetProgramInfoLog = glGetProgramInfoLog;
	GL->DeleteProgram = glDeleteProgram;
	GL->DetachShader = glDetachShader;
	GL->UseProgram = glUseProgram;
	GL->BindBuffer = glBindBuffer;
	GL->EnableVertexAttribArray = glEnableVertexAttribArray;
	GL->VertexAttribPointer = glVertexAttribPointer;
	GL->DisableVertexAttribArray = glDisableVertexAttribArray;
	GL->GenBuffers = glGenBuffers;
	GL->BufferData = glBufferData;
	GL->BufferSubData = glBufferSubData;
	GL->DrawArrays = glDrawArrays;
	GL->DrawElements = glDrawElements;
	GL->DrawElementsInstanced = glDrawElementsInstanced;
	GL->DrawRangeElements = glDrawRangeElements;
	GL->GetUniformLocation = glGetUniformLocation;
	GL->Uniform4f = glUniform4f;
	GL->Uniform4fv = glUniform4fv;
	GL->UniformMatrix4fv = glUniformMatrix4fv;
	GL->ClearColor = glClearColor;
	GL->ClearDepthf = glClearDepthf;
	GL->Enable = glEnable;
	GL->CullFace = glCullFace;
	GL->FrontFace = glFrontFace;
	GL->DepthFunc = glDepthFunc;
	GL->DepthMask = glDepthMask;
	GL->DepthRangef = glDepthRangef;
	GL->GenVertexArrays = glGenVertexArrays;
	GL->BindVertexArray = glBindVertexArray;
	GL->GetError = glGetError;
	GL->MapBufferRange = glMapBufferRange;
	GL->UnmapBuffer = glUnmapBuffer;
	GL->BlendFunc = glBlendFunc;
	GL->GenTextures = glGenTextures;
	GL->BindTexture = glBindTexture;
	GL->TexImage2D = glTexImage2D;
	GL->CompressedTexImage2D = glCompressedTexImage2D;
	GL->ActiveTexture = glActiveTexture;
	GL->TexParameteri = glTexParameteri;
	GL->DeleteTextures = glDeleteTextures;
	GL->GetIntegerv = glGetIntegerv;
	GL->UniformBlockBinding = glUniformBlockBinding;
	GL->GetUniformBlockIndex = glGetUniformBlockIndex;
	GL->BindBufferBase = glBindBufferBase;
	GL->GetUniformIndices = glGetUniformIndices;
	GL->TransformFeedbackVaryings = glTransformFeedbackVaryings;
	GL->BeginTransformFeedback = glBeginTransformFeedback;
	GL->EndTransformFeedback = glEndTransformFeedback;
	GL->Flush = glFlush;
}
#else

static void render_bind_gl_functions(struct engine *Engine)
{
	struct opengl *GL = &Engine->Render->OpenGL;
	GL->CreateShader = (PFNGLCREATESHADERPROC)Engine->Platform->LoadGLFunction("glCreateShader");
	GL->ShaderSource = (PFNGLSHADERSOURCEPROC)Engine->Platform->LoadGLFunction("glShaderSource");
	GL->CompileShader = (PFNGLCOMPILESHADERPROC)Engine->Platform->LoadGLFunction("glCompileShader");
	GL->GetShaderiv = (PFNGLGETSHADERIVPROC)Engine->Platform->LoadGLFunction("glGetShaderiv")   ;
	GL->GetString = (PFNGLGETSTRINGPROC)Engine->Platform->LoadGLFunction("glGetString")   ;
	GL->GetStringi = (PFNGLGETSTRINGIPROC)Engine->Platform->LoadGLFunction("glGetStringi")   ;
	GL->GetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)Engine->Platform->LoadGLFunction("glGetShaderInfoLog")  ;
	GL->DeleteShader = (PFNGLDELETESHADERPROC)Engine->Platform->LoadGLFunction("glDeleteShader");
	GL->DeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)Engine->Platform->LoadGLFunction("glDeleteVertexArrays");
	GL->DeleteBuffers = (PFNGLDELETEVERTEXARRAYSPROC)Engine->Platform->LoadGLFunction("glDeleteBuffers");
	GL->CreateProgram = (PFNGLCREATEPROGRAMPROC)Engine->Platform->LoadGLFunction("glCreateProgram");
	GL->AttachShader = (PFNGLATTACHSHADERPROC)Engine->Platform->LoadGLFunction("glAttachShader");
	GL->LinkProgram = (PFNGLLINKPROGRAMPROC)Engine->Platform->LoadGLFunction("glLinkProgram");
	GL->GetProgramiv = (PFNGLGETPROGRAMIVPROC)Engine->Platform->LoadGLFunction("glGetProgramiv");
	GL->GetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)Engine->Platform->LoadGLFunction("glGetProgramInfoLog");
	GL->DeleteProgram = (PFNGLDELETEPROGRAMPROC)Engine->Platform->LoadGLFunction("glDeleteProgram");
	GL->DetachShader = (PFNGLDETACHSHADERPROC)Engine->Platform->LoadGLFunction("glDetachShader");
	GL->UseProgram = (PFNGLUSEPROGRAMPROC)Engine->Platform->LoadGLFunction("glUseProgram");
	GL->BindBuffer = (PFNGLBINDBUFFERPROC)Engine->Platform->LoadGLFunction("glBindBuffer");
	GL->EnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC)Engine->Platform->LoadGLFunction("glEnableVertexAttribArray");

	GL->Viewport = (PFNGLVIEWPORTPROC)Engine->Platform->LoadGLFunction("glViewport");
	GL->VertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC)Engine->Platform->LoadGLFunction("glVertexAttribPointer");
	GL->DisableVertexAttribArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)Engine->Platform->LoadGLFunction("glDisableVertexAttribArray");
	GL->GenBuffers = (PFNGLGENBUFFERSPROC)Engine->Platform->LoadGLFunction("glGenBuffers");
	GL->BufferData = (PFNGLBUFFERDATAPROC)Engine->Platform->LoadGLFunction("glBufferData");
	GL->BufferSubData = (PFNGLBUFFERSUBDATAPROC)Engine->Platform->LoadGLFunction("glBufferSubData");
	GL->DrawArrays = (PFNGLDRAWARRAYSPROC)Engine->Platform->LoadGLFunction("glDrawArrays");
	GL->DrawElements = (PFNGLDRAWELEMENTSPROC)Engine->Platform->LoadGLFunction("glDrawElements");
	GL->DrawElementsInstanced = (PFNGLDRAWELEMENTSINSTANCEDPROC)Engine->Platform->LoadGLFunction("glDrawElementsInstanced");
	GL->DrawRangeElements = (PFNGLDRAWRANGEELEMENTSPROC)Engine->Platform->LoadGLFunction("glDrawRangeElements");
	GL->GetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)Engine->Platform->LoadGLFunction("glGetUniformLocation");
	GL->Uniform4f = (PFNGLUNIFORM4FPROC)Engine->Platform->LoadGLFunction("glUniform4f");
	GL->Uniform4fv = (PFNGLUNIFORM4FVPROC)Engine->Platform->LoadGLFunction("glUniform4fv");
	GL->UniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC)Engine->Platform->LoadGLFunction("glUniformMatrix4fv");
	GL->Clear = (PFNGLCLEARPROC)Engine->Platform->LoadGLFunction("glClear");
	GL->ClearColor = (PFNGLCLEARCOLORPROC)Engine->Platform->LoadGLFunction("glClearColor");
	GL->ClearDepthf = (PFNGLCLEARDEPTHFPROC)Engine->Platform->LoadGLFunction("glClearDepthf");
	GL->Enable = (PFNGLENABLEPROC)Engine->Platform->LoadGLFunction("glEnable");
	GL->CullFace = (PFNGLCULLFACEPROC)Engine->Platform->LoadGLFunction("glCullFace");
	GL->FrontFace = (PFNGLFRONTFACEPROC)Engine->Platform->LoadGLFunction("glFrontFace");
	GL->DepthFunc = (PFNGLDEPTHFUNCPROC)Engine->Platform->LoadGLFunction("glDepthFunc");
	GL->DepthMask = (PFNGLDEPTHMASKPROC)Engine->Platform->LoadGLFunction("glDepthMask");
	GL->DepthRangef = (PFNGLDEPTHRANGEFPROC)Engine->Platform->LoadGLFunction("glDepthRangef");
	GL->GenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)Engine->Platform->LoadGLFunction("glGenVertexArrays");
	GL->BindVertexArray = (PFNGLBINDVERTEXARRAYPROC)Engine->Platform->LoadGLFunction("glBindVertexArray");
	GL->GetError = (PFNGLGETERRORPROC)Engine->Platform->LoadGLFunction("glGetError");
	GL->MapBufferRange = (PFNGLMAPBUFFERRANGEPROC)Engine->Platform->LoadGLFunction("glMapBufferRange");
	GL->UnmapBuffer = (PFNGLUNMAPBUFFERPROC)Engine->Platform->LoadGLFunction("glUnmapBuffer");
	GL->BlendFunc = (PFNGLBLENDFUNCPROC)Engine->Platform->LoadGLFunction("glBlendFunc");
	GL->GenTextures = (PFNGLGENTEXTURESPROC)Engine->Platform->LoadGLFunction("glGenTextures");
	GL->BindTexture = (PFNGLBINDTEXTUREPROC)Engine->Platform->LoadGLFunction("glBindTexture");
	GL->TexImage2D = (PFNGLTEXIMAGE2DPROC)Engine->Platform->LoadGLFunction("glTexImage2D");
	GL->CompressedTexImage2D = (PFNGLCOMPRESSEDTEXIMAGE2DPROC)Engine->Platform->LoadGLFunction("glCompressedTexImage2D");
	GL->ActiveTexture = (PFNGLACTIVETEXTUREPROC)Engine->Platform->LoadGLFunction("glActiveTexture");
	GL->GetCompressedTexImage = (PFNGLGETCOMPRESSEDTEXIMAGEPROC)Engine->Platform->LoadGLFunction("glGetCompressedTexImage");
	GL->TexParameteri = (PFNGLTEXPARAMETERIPROC)Engine->Platform->LoadGLFunction("glTexParameteri");
	GL->DeleteTextures = (PFNGLDELETETEXTURESPROC)Engine->Platform->LoadGLFunction("glDeleteTextures");
	GL->GetIntegerv = (PFNGLGETINTEGERVPROC)Engine->Platform->LoadGLFunction("glGetIntegerv");
	GL->UniformBlockBinding = (PFNGLUNIFORMBLOCKBINDINGPROC)Engine->Platform->LoadGLFunction("glUniformBlockBinding");
	GL->GetUniformBlockIndex = (PFNGLGETUNIFORMBLOCKINDEXPROC)Engine->Platform->LoadGLFunction("glGetUniformBlockIndex");
	GL->BindBufferBase = (PFNGLBINDBUFFERBASEPROC)Engine->Platform->LoadGLFunction("glBindBufferBase");
	GL->GetUniformIndices = (PFNGLGETUNIFORMINDICESPROC)Engine->Platform->LoadGLFunction("glGetUniformIndices");
	GL->GetActiveUniform = (PFNGLGETACTIVEUNIFORMPROC)Engine->Platform->LoadGLFunction("glGetActiveUniform");
	GL->TransformFeedbackVaryings = (PFNGLTRANSFORMFEEDBACKVARYINGSPROC)Engine->Platform->LoadGLFunction("glTransformFeedbackVaryings");
	GL->BeginTransformFeedback = (PFNGLBEGINTRANSFORMFEEDBACKPROC)Engine->Platform->LoadGLFunction("glBeginTransformFeedback");
	GL->EndTransformFeedback = (PFNGLENDTRANSFORMFEEDBACKPROC)Engine->Platform->LoadGLFunction("glEndTransformFeedback");
	GL->Flush = (PFNGLFLUSHPROC)Engine->Platform->LoadGLFunction("glFlush");
	GL->GetBufferSubData = (PFNGLGETBUFFERSUBDATAPROC)Engine->Platform->LoadGLFunction("glGetBufferSubData");
}
#endif

struct sprite_info
{
	u32 TexId;
	vec3 Pos;
	vec2 Size;
	f32 ZRot;
	vec2 UvPos;
	vec2 UvSize;
	vec4 Color;
};

void render_gpu_driver_init(engine *Engine)
{
	render *Render = Engine->Render;
	struct opengl *GL = &Render->OpenGL;

#ifdef RENDER_GLES
	render_bind_gles_functions(GL);
#else
	render_bind_gl_functions(Engine);
#endif

	Engine->Platform->Log("Opengl vendor %s\n", GL->GetString(GL_VENDOR));
	Engine->Platform->Log("Opengl renderer %s\n", GL->GetString(GL_RENDERER));
	Engine->Platform->Log("Opengl version %s\n", GL->GetString(GL_VERSION));
	s32 Value;
	GL->GetIntegerv(GL_NUM_EXTENSIONS, &Value);
	Engine->Platform->Log("Opengl extensions %d\n", Value);
	for (s32 i=0; i<Value; ++i)
		Engine->Platform->Log("[%d] %s , ", i, GL->GetStringi(GL_EXTENSIONS, i));
	GLCheckErrors(Engine);
	GL->GetIntegerv(GL_MAX_UNIFORM_LOCATIONS, &Value);
	Engine->Platform->Log("Opengl max uniform locations %d\n", Value);
	GL->GetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &Value);
	Engine->Platform->Log("Opengl max uniform block size %d\n", Value);

	GLCheckErrors(Engine);
	GL->Enable(GL_CULL_FACE);
	GL->Enable(GL_DEPTH_TEST);
	GL->Enable(GL_BLEND);
	GL->BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	GL->CullFace(GL_BACK);
	GLCheckErrors(Engine);
	GL->FrontFace(GL_CW);
	GL->DepthMask(GL_TRUE);
	GL->DepthFunc(GL_LEQUAL);
	GL->DepthRangef(0.0f, 1.0f);
	GL->ClearDepthf(1.0f);

	GLCheckErrors(Engine);

	GL->GenVertexArrays(1, &Render->LineVao);
	GL->GenBuffers(1, &Render->LineBufferObject);

	GL->BindBuffer(GL_ARRAY_BUFFER, Render->LineBufferObject);
	GL->BufferData(GL_ARRAY_BUFFER, sizeof(Render->LineVertices), 0, GL_STREAM_DRAW);

	GL->BindVertexArray(Render->LineVao);
		GL->EnableVertexAttribArray(0);
		GL->VertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_pc), 0);

		GL->EnableVertexAttribArray(1);
		GL->VertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(vertex_pc), (void*)sizeof(vec3));

	GLCheckErrors(Engine);

	GL->GenVertexArrays(1, &Render->MeshVbo.Vao);
	GL->BindVertexArray(Render->MeshVbo.Vao);
	GL->GenBuffers(1, &Render->MeshVbo.Vertices);
	GL->BindBuffer(GL_ARRAY_BUFFER, Render->MeshVbo.Vertices);
	GL->BufferData(GL_ARRAY_BUFFER, 1024*sizeof(vertex_pc), 0, GL_DYNAMIC_DRAW);

	GL->GenBuffers(1, &Render->MeshVbo.Elements);
	GL->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, Render->MeshVbo.Elements);
	GL->BufferData(GL_ELEMENT_ARRAY_BUFFER, 1024*sizeof(u16), 0, GL_DYNAMIC_DRAW);
	
	GL->GenVertexArrays(1, &Render->SpriteVbo.Vao);
	GL->BindVertexArray(Render->SpriteVbo.Vao);
	GL->GenBuffers(1, &Render->SpriteVbo.Vertices);
	GL->BindBuffer(GL_ARRAY_BUFFER, Render->SpriteVbo.Vertices);
	GL->BufferData(GL_ARRAY_BUFFER, 32*sizeof(vertex_pcuv), 0, GL_STREAM_DRAW);

	GL->EnableVertexAttribArray(0);
	GL->VertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_pcuv), 0);

	GL->EnableVertexAttribArray(1);
	GL->VertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(vertex_pcuv), (void*)offsetof(vertex_pcuv, Color));

	GL->EnableVertexAttribArray(2);
	GL->VertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_pcuv), (void*)offsetof(vertex_pcuv, UV));

	GL->BindVertexArray(0);
	GL->BindBuffer(GL_ARRAY_BUFFER, 0);

	GL->GenBuffers(1, &Render->SpriteVbo.Elements);
	GL->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, Render->SpriteVbo.Elements);
	GL->BufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(u16), 0, GL_DYNAMIC_DRAW);
	GL->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	GLCheckErrors(Engine);
	
#if 0
	GL->GenBuffers(1, &Render->TransformFeedbackObject);
	GL->BindBuffer(GL_ARRAY_BUFFER, Render->TransformFeedbackObject);
	GL->BufferData(GL_ARRAY_BUFFER, 512, 0, GL_STATIC_READ);
#endif
}

void render_init(engine *Engine)
{
	render *Render = Engine->Render;
	if (Render->Initialized) {
		return;
	}

	render_gpu_driver_init(Engine);

	Render->SpriteBatches = dynamic_array_create(Engine, sprite_batch);

	Render->Initialized = 1;
}

static void render_delete_asset_texture(engine *Engine, asset *Asset)
{
	Engine->Render->OpenGL.DeleteTextures(1, &Asset->TextureAsset.TextureId);
	Asset->Sid = 0;
	GLCheckErrors(Engine);
}

void render_gpu_driver_shutdown(engine *Engine)
{
	render *Render = Engine->Render;
	struct opengl *GL = &Render->OpenGL;

	for (int i=0; i<ShaderProgramType_Max; ++i)
	{
		GL->DeleteProgram(Render->Shaders[i]);
		Render->Shaders[i] = 0;
	}
	GL->DeleteBuffers(1, &Render->LineBufferObject);
	GL->DeleteBuffers(1, &Render->MeshVbo.Vertices);
	GL->DeleteBuffers(1, &Render->MeshVbo.Elements);
	GL->DeleteBuffers(1, &Render->SpriteVbo.Vertices);
	GL->DeleteBuffers(1, &Render->SpriteVbo.Elements);
	GL->DeleteVertexArrays(1, &Render->LineVao);
	GL->DeleteVertexArrays(1, &Render->MeshVbo.Vao);
	GL->DeleteVertexArrays(1, &Render->SpriteVbo.Vao);

	asset_get_all_of_type(Engine, AssetType_Texture, render_delete_asset_texture);
}

void render_shutdown(engine *Engine)
{
	render *Render = Engine->Render;
	if (!Render->Initialized)
		return;

	render_gpu_driver_shutdown(Engine);
	Render->Initialized = 0;
}

void render_set_clear_color(engine *Engine, vec4 Color)
{
	Engine->Render->OpenGL.ClearColor(Color.r, Color.g, Color.b, Color.a);
}

static mesh render_create_mesh_and_upload_buffer(engine *Engine, buffer_object *Vbo, void *Vertices, u32 VerticesLen, u32 VertexSize, u16 *Elements, u32 ElementsLen)
{
	render *Render = Engine->Render;
	struct opengl *GL = &Render->OpenGL;

	mesh Ret = {0};
	Ret.ElementsStart = Vbo->ElementsLen;
	Ret.ElementsLen = ElementsLen;

	for (u32 i=0; i<ElementsLen; ++i)
	{
		Elements[i] += Vbo->VerticesLen;
	}

	GL->BindBuffer(GL_ARRAY_BUFFER, Vbo->Vertices);
	GL->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, Vbo->Elements);

	//TODO: Naive approach, only send data, never delete anything
	GL->BufferSubData(GL_ARRAY_BUFFER, Vbo->VerticesLen*VertexSize, VerticesLen*VertexSize, Vertices);
	GL->BufferSubData(GL_ELEMENT_ARRAY_BUFFER, Vbo->ElementsLen*sizeof(u16), ElementsLen*sizeof(u16), Elements);

	GL->BindBuffer(GL_ARRAY_BUFFER, 0);
	GL->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	Vbo->VerticesLen += VerticesLen;
	Vbo->ElementsLen += ElementsLen;

	return Ret;
}

mesh render_create_mesh(engine *Engine, vertex_pc *Vertices, u32 VerticesLen, u16 *Elements, u32 ElementsLen)
{
	render *Render = Engine->Render;
	struct opengl *GL = &Render->OpenGL;

	mesh Ret = render_create_mesh_and_upload_buffer(Engine, &Render->MeshVbo, Vertices, VerticesLen, sizeof(vertex_pc), Elements, ElementsLen);

	GL->BindVertexArray(Render->MeshVbo.Vao);

	GL->BindBuffer(GL_ARRAY_BUFFER, Render->MeshVbo.Vertices);

	GL->EnableVertexAttribArray(0);
	GL->VertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_pc), 0);

	GL->EnableVertexAttribArray(1);
	GL->VertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(vertex_pc), (void*)sizeof(vec3));

	GL->BindBuffer(GL_ARRAY_BUFFER, 0);
	GL->BindVertexArray(0);

	GLCheckErrors(Engine);

	return Ret;
}

u32 render_create_program(engine *Engine, shader_def *ShaderDefs, u32 ShaderDefsLen)
{
	struct opengl *GL = &Engine->Render->OpenGL;
	GLuint ProgramId = 0;

	GLuint CompiledShaderIds[16];
	u32 CompiledShaderIdIndex = 0;

	for (u32 Index = 0; Index < ShaderDefsLen; ++Index)
	{
		shader_def *ShaderDef = ShaderDefs + Index;
		u32 Size;

		GLuint ShaderId = GL->CreateShader(ShaderDef->Type);
		if (ShaderId == 0)
		{
			return 0;
		}

		u8* FileContent = asset_load_file(Engine, ShaderDef->Name, &Size);
		if (FileContent == 0)
		{
			Engine->Platform->Log("Error reading shader %s\n", ShaderDef->Name);
			return 0;
		}
		GL->ShaderSource(ShaderId, 1, (const GLchar *const *)&FileContent, &Size);
		GL->CompileShader(ShaderId);

		memory_free(&Engine->Memory, FileContent);
		//Check for errors
		GLint Success;
		GL->GetShaderiv(ShaderId, GL_COMPILE_STATUS, &Success);
		if (!Success)
		{
			char ErrorMsg[4096];
			GLint Log;
			GLsizei MsgSize;
			GL->GetShaderiv(ShaderId, GL_INFO_LOG_LENGTH, &Log);

			GL->GetShaderInfoLog(ShaderId, 4096, &MsgSize, ErrorMsg);

			Engine->Platform->Log(ErrorMsg);

			GL->DeleteShader(ShaderId);

			return 0;
		}

		CompiledShaderIds[CompiledShaderIdIndex++] = ShaderId;
		if (CompiledShaderIdIndex == static_array_len(CompiledShaderIds))
		{
			Engine->Platform->Log("ERROR: Max shaders! %s, %s, %d\n", __FILE__, __func__, __LINE__);
			return 0;
		}
	}

	ProgramId = GL->CreateProgram();

	for (u32 Ix = 0; Ix < CompiledShaderIdIndex; ++Ix)
	{
		GL->AttachShader(ProgramId, CompiledShaderIds[Ix]);
	}

#if 0
	const GLchar* feedbackVaryings[] = { "DebugValue" };
	GL->TransformFeedbackVaryings(ProgramId, 1, feedbackVaryings, GL_INTERLEAVED_ATTRIBS);
#endif

	GL->LinkProgram(ProgramId);

	GLint IsLinked;
	GL->GetProgramiv(ProgramId, GL_LINK_STATUS, &IsLinked);
	if (!IsLinked)
	{
		char ErrorMsg[4096];
		GLint MaxLenght;
		GL->GetProgramiv(ProgramId, GL_INFO_LOG_LENGTH, &MaxLenght);
		GL->GetProgramInfoLog(ProgramId, 4096, &MaxLenght, ErrorMsg);
		Engine->Platform->Log(ErrorMsg);
		for (u32 Ix = 0; Ix < CompiledShaderIdIndex; ++Ix)
		{
			GL->DeleteShader(CompiledShaderIds[Ix]);
		}

		GL->DeleteProgram(ProgramId);

		return 0;
	}

	for (u32 Ix = 0; Ix < CompiledShaderIdIndex; ++Ix)
	{
		GL->DetachShader(ProgramId, CompiledShaderIds[Ix]);
		GL->DeleteShader(CompiledShaderIds[Ix]);
	}

	GLCheckErrors(Engine);

	return ProgramId;
}

static GLuint render_get_program(engine *Engine, GLuint ProgramId)
{
	struct shader_program *Ret = Engine->Render->ShaderList;
	while (Ret)
	{
		if (Ret->Id == ProgramId)
			return Ret->Id;

		Ret = Ret->Next;
	}

	return 0;
}
void render_draw_line(engine *Engine, vec3 From, vec3 To)
{
	render *Render = Engine->Render;
	u32 LineProgram = Render->Shaders[ShaderProgramType_Line];
	if (LineProgram)
	{
		vertex_pc *CurVec = Render->LineVertices + Render->LineIndex++;
		CurVec->Pos = From;
		CurVec->Color = Render->DefaultColor;

		CurVec = Render->LineVertices + Render->LineIndex++;
		CurVec->Pos = To;
		CurVec->Color = Render->DefaultColor;
	}
	else
	{
#ifdef RENDER_GLES
		shader_def Shaders[] = {{"shaders/gles/line.frag", GL_FRAGMENT_SHADER},
			{"shaders/gles/line.vert", GL_VERTEX_SHADER}};
#else
		shader_def Shaders[] = {{"shaders/gl/line.frag", GL_FRAGMENT_SHADER},
			{"shaders/gl/line.vert", GL_VERTEX_SHADER}};
#endif

		Render->Shaders[ShaderProgramType_Line] = render_create_program(Engine, Shaders, static_array_len(Shaders));
	}

	GLCheckErrors(Engine);
}

void render_draw_sprite(struct engine *Engine, sprite Sprite, vec3 Pos, vec2 Size, f32 ZRot, blend_mode BlendMode)
{
	f64 T0 = Engine->Platform->GetLocalTime();

	render *Render = Engine->Render;
	struct opengl *GL = &Render->OpenGL;
	{
		asset *Asset = asset_get(Engine, Sprite.TextureName);
		if (Asset)
		{
			vec2 Orig = Vec2(Sprite.TexturePos.x/Asset->TextureAsset.Width,
							Sprite.TexturePos.y/Asset->TextureAsset.Height);
			vec2 Extends = Vec2(Sprite.TextureSize.x/Asset->TextureAsset.Width,
							Sprite.TextureSize.y/Asset->TextureAsset.Height);

			struct sprite_info SInfo = {Asset->TextureAsset.TextureId, Pos, Size, ZRot, Orig, Extends, Sprite.Color};

			b32 Found = 0;
			for (u32 i=0; i<dynamic_array_len(Render->SpriteBatches); ++i)
			{
				sprite_batch *Batch = Render->SpriteBatches + i;
				if (Batch->TextureId == 0)
				{
					if (string_compare(Batch->TextureName, Sprite.TextureName))
						Batch->TextureId = Asset->TextureAsset.TextureId;
				}
				if (Batch->TextureId == SInfo.TexId && Batch->BlendMode == BlendMode)
				{
					Found = 1;
					dynamic_array_add(&Batch->SpriteInfo, SInfo);
					break;
				}
			}

			if (!Found)
			{
				Engine->Platform->Log("Error, unable to find %s texture on the batch groups\n", Sprite.TextureName);
				//TODO: add properly?
				/*
				sprite_batch SB = {SInfo.TexId, BlendMode};
				SB.SpriteInfo = dynamic_array_create(Engine, struct sprite_info);
				dynamic_array_add(&SB.SpriteInfo, SInfo);
				dynamic_array_add(&Render->SpriteBatches, SB);
				*/
			}
		}
		else
		{
			asset_load_texture(Engine, Sprite.TextureName);
		}
	}

	GLCheckErrors(Engine);

	Render->CPUFrameTime += Engine->Platform->GetLocalTime() - T0;
}

void render_draw_quad(engine *Engine, vec2 Pos, f32 Scale)
{
	if (Engine->Render->QuadMesh.ElementsLen == 0)
		render_create_quad_mesh(Engine);
		
	render_draw_mesh(Engine, &Engine->Render->QuadMesh, Vec3(Pos.x, Pos.y, 0), Vec2(Scale, Scale));
}

void render_draw_rect(struct engine *Engine, vec3 Pos, vec2 Scale)
{
	if (Engine->Render->QuadMesh.ElementsLen == 0)
		render_create_quad_mesh(Engine);
		
	render_draw_mesh(Engine, &Engine->Render->QuadMesh, Pos, Scale);
}

void render_draw_circle(engine *Engine, vec3 Pos, f32 Rad)
{
	if (Engine->Render->CircleMesh.ElementsLen == 0)
		render_create_circle_mesh(Engine);

	render_draw_mesh(Engine, &Engine->Render->CircleMesh, Pos, Vec2(Rad, Rad));
}

//TODO: Unify that stuff and only draw elements instead of doing bindings per each call
void render_draw_mesh(engine *Engine, mesh *Mesh, vec3 Pos, vec2 Scale)
{
	render *Render = Engine->Render;
	struct opengl *GL = &Render->OpenGL;

	u32 Program = Render->Shaders[ShaderProgramType_Mesh];
	if (Program)
	{
		GL->UseProgram(Program);
		s32 Uni = GL->GetUniformLocation(Program, "ProjectionMatrix");
		if (Uni>=0)
		{
			GL->UniformMatrix4fv(Uni, 1, GL_FALSE, Render->ProjectionMatrix);
		}
		Uni = GL->GetUniformLocation(Program, "MV");
		if (Uni >= 0)
		{
			f32 MV[16] = {0};
			MV[0] = Scale.x;
			MV[5] = Scale.y;
			MV[10] = 1;
			MV[12] = Pos.x;
			MV[13] = Pos.y;
			MV[14] = Pos.z;
			MV[15] = 1;

			GL->UniformMatrix4fv(Uni, 1, GL_FALSE, MV);
		}

		Uni = GL->GetUniformLocation(Program, "TintColor");
		if (Uni >= 0)
		{
			GL->Uniform4fv(Uni, 1, &Render->DefaultColor.r);
		}

		GL->BindVertexArray(Render->MeshVbo.Vao);

		u64 Offset = Mesh->ElementsStart*sizeof(u16);
		GL->DrawRangeElements(GL_TRIANGLES, Mesh->ElementsStart, Mesh->ElementsStart + Mesh->ElementsLen, Mesh->ElementsLen, GL_UNSIGNED_SHORT, (void*)Offset);

		GL->BindVertexArray(0);
		GL->UseProgram(0);

		GLCheckErrors(Engine);
	}
	else
	{
		render_create_mesh_program(Engine);
	}
}

void render_create_texture(engine *Engine, asset *Asset, u32 PixelSize)
{
	render *Render = Engine->Render;
	struct opengl *GL = &Render->OpenGL;
	GL->GenTextures(1, &Asset->TextureAsset.TextureId);
	GL->BindTexture(GL_TEXTURE_2D, Asset->TextureAsset.TextureId);

	u32 Width = Asset->TextureAsset.Width;
	u32 Height = Asset->TextureAsset.Height;
	u32 Size = ((Width+3)/4)*((Height+3)/4)*PixelSize;
	GL->CompressedTexImage2D(GL_TEXTURE_2D, 0, Asset->TextureAsset.Format, Width, Height, 0, Size, Asset->TextureAsset.Data);

	GL->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	GL->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	GL->BindTexture(GL_TEXTURE_2D, 0);
	GLCheckErrors(Engine);
}

#if DEBUG
void render_reload_asset(struct engine *Engine, asset *Asset)
{
	assert(Asset->Type == AssetType_Texture);

	Engine->Render->OpenGL.DeleteTextures(1, &Asset->TextureAsset.TextureId);
	Asset->Sid = 0;
	//TODO: What to do with string name? I'm not duplicating it
}
#endif

void render_set_default_color(engine *Engine, vec4 Color)
{
	Engine->Render->DefaultColor = Color;
}

void render_set_ortographic_projection(engine *Engine, f32 Left, f32 Right, f32 Bottom, f32 Top)
{
	Engine->Render->DesignSize = Vec2(Right - Left, Top - Bottom);
	f32 *Mat = Engine->Render->ProjectionMatrix;
	Mat[0] = 2.0f/(Right-Left);
	Mat[5] = 2.0f/(Top-Bottom);
	Mat[10] = 1.0f; //Don't scale z coords
	Mat[12] = -((Right+Left)/(Right-Left));
	Mat[13] = -((Top+Bottom)/(Top-Bottom));
	Mat[14] = 0;// Don't translate z coords 
	Mat[15] = 1.0f;
}

static s32 render_sprite_alpha_partition(struct sprite_info *SpriteInfo, int Left, int Right)
{
	s32 LocalLeft = Left-1;
	s32 LocalRight = Right;
	s32 Pivot = Right;
	while(1)
	{
		while (SpriteInfo[++LocalLeft].Pos.z < SpriteInfo[Pivot].Pos.z);

		while (LocalRight > 0 && SpriteInfo[--LocalRight].Pos.z > SpriteInfo[Pivot].Pos.z);

		if (LocalLeft >= LocalRight)
			break;

		struct sprite_info aux = SpriteInfo[LocalLeft];
		SpriteInfo[LocalLeft] = SpriteInfo[LocalRight];
		SpriteInfo[LocalRight] = aux;
	}
	
	struct sprite_info aux = SpriteInfo[Right];
	SpriteInfo[Right] = SpriteInfo[LocalLeft];
	SpriteInfo[LocalLeft] = aux;

	return LocalLeft;
}

static void render_sprite_alpha_sort(struct sprite_info *SpriteInfo, s32 Left, s32 Right)
{
	if (Left > Right)
		return;

	s32 Partition = render_sprite_alpha_partition(SpriteInfo, Left, Right);
	render_sprite_alpha_sort(SpriteInfo, Left, Partition-1);
	render_sprite_alpha_sort(SpriteInfo, Partition+1, Right);
}

void render_update(engine *Engine)
{
	f64 T0 = Engine->Platform->GetLocalTime();

	render *Render = Engine->Render;
	struct opengl *GL = &Engine->Render->OpenGL;
	u32 LineProgram = Render->Shaders[ShaderProgramType_Line];
	if (LineProgram)
	{
		GL->UseProgram(LineProgram);

		s32 UniformLocation = GL->GetUniformLocation(LineProgram, "ProjectionMatrix");
		if (UniformLocation >= 0)
		{
			GL->UniformMatrix4fv(UniformLocation, 1, GL_FALSE, Render->ProjectionMatrix);
		}

		GL->BindVertexArray(Render->LineVao);
		GL->BindBuffer(GL_ARRAY_BUFFER, Engine->Render->LineBufferObject);
		GL->BufferSubData(GL_ARRAY_BUFFER, 0, Render->LineIndex*sizeof(vertex_pc), Render->LineVertices);

		GL->DrawArrays(GL_LINES, 0, Render->LineIndex);

		GL->BindVertexArray(0);
		GL->UseProgram(0);
	}

	Render->LineIndex = 0;

	GLCheckErrors(Engine);

	{
	u32 Program = Render->Shaders[ShaderProgramType_Sprite];
	if (!Program)
	{
#ifdef RENDER_GLES
		shader_def Shaders[] = {{"shaders/gles/sprite.frag", GL_FRAGMENT_SHADER},
			{"shaders/gles/sprite.vert", GL_VERTEX_SHADER}};
#else
		shader_def Shaders[] = {{"shaders/gl/sprite.frag", GL_FRAGMENT_SHADER},
			{"shaders/gl/sprite.vert", GL_VERTEX_SHADER}};
#endif

		Program = Render->Shaders[ShaderProgramType_Sprite] = render_create_program(Engine, Shaders, static_array_len(Shaders));

	}

	//Draw sprites
	f32 MV[16] = {0};
	MV[10] = 1;
	MV[15] = 1;
	
	GL->UseProgram(Program);

	s32 Uni = GL->GetUniformLocation(Program, "ProjectionMatrix");
	if (Uni>=0)
	{
		GL->UniformMatrix4fv(Uni, 1, GL_FALSE, Render->ProjectionMatrix);
	}

	GL->BindVertexArray(Render->SpriteVbo.Vao);

	for (u32 GroupIndex = 0; GroupIndex<dynamic_array_len(Render->SpriteBatches); ++GroupIndex)
	{
		sprite_batch *SB = Render->SpriteBatches + GroupIndex;
		render_sprite_alpha_sort(SB->SpriteInfo, 0, dynamic_array_len(SB->SpriteInfo)-1);

		u32 VerticesSize = dynamic_array_len(SB->SpriteInfo)*sizeof(vertex_pcuv)*4;
		vertex_pcuv *Vertices = memory_alloc(&Engine->Memory, VerticesSize);
		u32 j=0;

		u32 ElementsSize = dynamic_array_len(SB->SpriteInfo)*sizeof(u16)*6;
		u16 *Elements = memory_alloc(&Engine->Memory, ElementsSize);

		for (s32 i=dynamic_array_len(SB->SpriteInfo)-1; i>=0; --i)
		{
			struct sprite_info *SInfo = SB->SpriteInfo + i;
			MV[0] = SInfo->Size.x * cosf(SInfo->ZRot);
			MV[1] = SInfo->Size.y * sinf(SInfo->ZRot);
			MV[4] = SInfo->Size.x * -sinf(SInfo->ZRot);
			MV[5] = SInfo->Size.y * cosf(SInfo->ZRot);
			MV[12] = SInfo->Pos.x;
			MV[13] = SInfo->Pos.y;
			MV[14] = SInfo->Pos.z;
			
			vec2 Orig = SInfo->UvPos;
			vec2 Extends = SInfo->UvSize;
			
			vec4 VPos = mat4_mul_vec4(MV, Vec4(-0.5f, -0.5f, 0, 1));
			vec3 VP = {VPos.x, VPos.y, VPos.z};
			vertex_pcuv Vertex1 = {VP, SInfo->Color, Vec2(Orig.x, Orig.y + Extends.y)};
			Vertices[j*4] = Vertex1;

			VPos = mat4_mul_vec4(MV, Vec4(0.5f, -0.5f, 0, 1));
			VP = Vec3(VPos.x, VPos.y, VPos.z);
			vertex_pcuv Vertex2 = {VP, SInfo->Color, Vec2(Orig.x + Extends.x, Orig.y + Extends.y)};
			Vertices[j*4+1] = Vertex2;

			VPos = mat4_mul_vec4(MV, Vec4(0.5f, 0.5f, 0, 1));
			VP = Vec3(VPos.x, VPos.y, VPos.z);
			vertex_pcuv Vertex3 = 	{VP, SInfo->Color, Vec2(Orig.x + Extends.x, Orig.y)};
			Vertices[j*4+2] = Vertex3;

			VPos = mat4_mul_vec4(MV, Vec4(-0.5f, 0.5f, 0, 1));
			VP = Vec3(VPos.x, VPos.y, VPos.z);
			vertex_pcuv Vertex4 = {VP, SInfo->Color, Vec2(Orig.x, Orig.y)};
			Vertices[j*4+3] = Vertex4;

			u16 *Element = Elements + j*6;
			*Element++ = 0 + j*4;
			*Element++ = 3 + j*4;
			*Element++ = 2 + j*4;
			*Element++ = 0 + j*4;
			*Element++ = 2 + j*4;
			*Element++ = 1 + j*4;

			++j;
		}

		GL->BindBuffer(GL_ARRAY_BUFFER, Render->SpriteVbo.Vertices);
		GL->BufferData(GL_ARRAY_BUFFER, VerticesSize, 0, GL_STREAM_DRAW);
		GL->BufferSubData(GL_ARRAY_BUFFER, 0, VerticesSize, Vertices);

		GL->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, Render->SpriteVbo.Elements);
		GL->BufferData(GL_ELEMENT_ARRAY_BUFFER, ElementsSize, 0, GL_STREAM_DRAW);
		GL->BufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, ElementsSize, Elements);

		GL->BindTexture(GL_TEXTURE_2D, SB->TextureId);
		GL->ActiveTexture(GL_TEXTURE0);

#if 0
	GL->Enable(GL_RASTERIZER_DISCARD);
	GL->BindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, Render->TransformFeedbackObject);
	GL->BeginTransformFeedback(GL_TRIANGLES);
#endif

		if (SB->BlendMode == BlendMode_Normal)
		{
			GL->BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else if (SB->BlendMode == BlendMode_Additive)
		{
			GL->BlendFunc(GL_SRC_ALPHA, GL_ONE);
		}

		GL->DrawElements(GL_TRIANGLES, dynamic_array_len(SB->SpriteInfo)*6, GL_UNSIGNED_SHORT, 0);
		GLCheckErrors(Engine);
		memory_free(&Engine->Memory, Vertices);
		memory_free(&Engine->Memory, Elements);

		dynamic_array_clear(SB->SpriteInfo);
	}

	GL->BindVertexArray(0);
	GL->BindTexture(GL_TEXTURE_2D, 0);

#if 0
	GL->EndTransformFeedback();
	GL->Flush();

	f32 TransFeed[512];
	GL->GetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 512, TransFeed);
#endif

	}

	Render->CPUFrameTime += Engine->Platform->GetLocalTime() - T0;
//	Engine->Platform->Log("cpu render time %ld\n", Render->CPUFrameTime);
	Render->CPUFrameTime = 0;
}

void render_draw_text_bmf_(struct engine *Engine, bmp_font *Font, u32 FontSize, const char *Text, u32 TextLen, vec2 Pos, vec4 Color)
{
	imgui *ImGui = Engine->ImGui;
	f32 Scale = FontSize/(f32)Font->GlyphH;
	f32 Width = Font->GlyphW*Scale;
	asset *Asset = asset_get(Engine, Font->AssetName);
	if (Asset)
	{
		for (u32 i=0; i<TextLen; ++i)
		{
			char C = Text[i];
			if (C == ' ')
			{
				Pos.x += Width;
				continue;
			}

			if (C >= '!' && C <= '~')
			{
				u32 Index = C - '!';
				u32 Row = Index/Font->Cols;
				u32 Col = Index - (Row*Font->Cols);

				vec2 GlyphSize = {Font->GlyphW, Font->GlyphH};
				sprite Sprite = {Font->AssetName, Vec2(Col*Font->GlyphW, Row*Font->GlyphH), GlyphSize, Color};
				render_draw_sprite(Engine, Sprite, vec2_to3(Pos, ImGui->ZLayer), vec2_mul(GlyphSize, Scale), 0, 0);
				Pos.x += Width;
			}
		}
	}
	else
	{
		//TODO: Is this threaded?
		//TODO: Why here, render_draw_sprite is also doing it internally
		asset_load_texture(Engine, Font->AssetName);
	}
}

void render_sprite_batch_reset(struct engine *Engine)
{
	for (u32 i=0; i<dynamic_array_len(Engine->Render->SpriteBatches); ++i)
	{
		sprite_batch *Batch = Engine->Render->SpriteBatches + i;
		Batch->TextureId = 0;
	}
}

void render_sprite_batch_add(engine *Engine, const char *TextureName, u32 Order, blend_mode BlendMode)
{
	sprite_batch Batch = {0};
	Batch.RenderOrder = Order;
	Batch.TextureName = string_dup(Engine, TextureName);
	Batch.SpriteInfo = dynamic_array_create(Engine, struct sprite_info);
	Batch.BlendMode = BlendMode;

	u32 IxFound = 0;
	u32 Len = dynamic_array_len(Engine->Render->SpriteBatches);
	for (u32 i=0; i<Len; ++i)
	{
		sprite_batch *B = Engine->Render->SpriteBatches + i;
		if (B->RenderOrder > Order)
		{
			IxFound = i-1;
			break;	
		}
	}
	if (IxFound == 0)
		IxFound = Len;

	dynamic_array_add_at(&Engine->Render->SpriteBatches, Batch, IxFound);
}
