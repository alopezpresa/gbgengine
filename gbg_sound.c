#include "gbg_sound.h"
#include "gbg_assets.h"

#include <limits.h>
#include <string.h>

typedef struct wave_format
{
	u16 Format;
	u16 Channels;
	u32 SampleRate;
	u32 AvgSampleRate;
	u16 BlockAlign;
	u16 BitsPerSample;
	u16 ExtraSize;
}wave_format;

void sound_init(engine *Engine)
{
	Engine->SoundSystem = memory_alloc_type(&Engine->Memory, sound_system);
	memset(Engine->SoundSystem, 0, sizeof(sound_system));
	Engine->SoundSystem->Channels = 2;
	Engine->SoundSystem->SampleRate = 44100;
	Engine->SoundSystem->MasterVolume = 1.0f;
	Engine->SoundSystem->StreamSoundData = dynamic_array_create(Engine, stream_sound_data);
}

void sound_set_loop(engine *Engine, const char *Name)
{
	asset *Asset = asset_get(Engine, Name);
	if (Asset)
	{
		Asset->SoundAsset.Loop = 1;
	}
}

void sound_add_to_stream(engine *Engine, sound_asset *SoundAsset)
{
	if (Engine->SoundSystem == 0)
		return;

	stream_sound_data SoundData = {0};
	SoundData.SoundAsset = SoundAsset;
	SoundData.Header = 0;

	Engine->Platform->Mutex(&Engine->SoundSystem->StreamLock);

	dynamic_array_add(&Engine->SoundSystem->StreamSoundData, SoundData);
	Engine->SoundSystem->StreamLock = 0;
}

void sound_set_master_volume(engine *Engine, f32 Volume)
{
	if (Engine->SoundSystem)
		Engine->SoundSystem->MasterVolume = Volume;
}

static void sound_copy_to_buffer(engine *Engine, s8* StreamBuffer, u32 BufferLen, stream_sound_data *SoundData)
{
	u32 Samples = BufferLen/sizeof(s16);

	for (u32 i=0; i<Samples; ++i)
	{
		s16 Sample16 = *(((s16*)StreamBuffer)+i);
		s16 NewSample = *(((s16*)(SoundData->SoundAsset->Data + SoundData->Header)) + i);
		s32 Sample32 = Sample16 + (s16)(NewSample*Engine->SoundSystem->MasterVolume * SoundData->SoundAsset->Gain);
		if (Sample32 > SHRT_MAX)
			Sample32 = SHRT_MAX;
		else if (Sample32 < SHRT_MIN)
			Sample32 = SHRT_MIN;

		*(((s16*)StreamBuffer)+i) = ((s16)Sample32);
	}

	SoundData->Header += BufferLen;
}

void sound_request_buffer_mix(engine *Engine, s8 *StreamBuffer, u32 BufferLen)
{
	memset(StreamBuffer, 0, BufferLen);

	Engine->Platform->Mutex(&Engine->SoundSystem->StreamLock);

	for (u32 SoundIndex = 0; SoundIndex < dynamic_array_len(Engine->SoundSystem->StreamSoundData); ++SoundIndex)
	{
		struct stream_sound_data *SoundData = Engine->SoundSystem->StreamSoundData + SoundIndex;

		u32 LoopOverLen = 0;
		u32 BytesToCopy = BufferLen;
		if (BufferLen > SoundData->SoundAsset->DataSize - SoundData->Header)
		{
			if (SoundData->SoundAsset->Loop || SoundData->SoundAsset->StreamFromDisk)
			{
				LoopOverLen = BufferLen - (SoundData->SoundAsset->DataSize - SoundData->Header);
			}
			BytesToCopy = SoundData->SoundAsset->DataSize - SoundData->Header;
		}
		sound_copy_to_buffer(Engine, StreamBuffer, BytesToCopy, SoundData);

		if (LoopOverLen > 0)
		{
			if (SoundData->SoundAsset->StreamFromDisk)
			{
				SoundData->SoundAsset->StreamRequest = 1;
				if (SoundData->SoundAsset->Data == SoundData->SoundAsset->StreamBuffer)
				{
					SoundData->SoundAsset->Data = SoundData->SoundAsset->StreamBuffer + SoundData->SoundAsset->DataSize;
				}
				else
				{
					SoundData->SoundAsset->Data = SoundData->SoundAsset->StreamBuffer;
				}
			}
			SoundData->Header = 0;
			sound_copy_to_buffer(Engine, StreamBuffer+BytesToCopy, LoopOverLen, SoundData);
		}

		if (!SoundData->SoundAsset->Loop)
		{
			if (SoundData->Header == SoundData->SoundAsset->DataSize)
			{
				dynamic_array_remove_at(Engine->SoundSystem->StreamSoundData, SoundIndex--);
			}
		}
	}

	Engine->SoundSystem->StreamLock = 0;
}

static void sound_asset_callback(engine *Engine, asset *Asset)
{
	sound_asset *S = &Asset->SoundAsset;
	if (S->StreamFromDisk && S->StreamRequest)
	{
		S->StreamRequest = 0;
		u8* WritePtr = 0;

		if (S->Data == S->StreamBuffer)
		{
			WritePtr = S->StreamBuffer + S->DataSize;
		}	
		else
		{
			WritePtr = S->StreamBuffer;
		}

		u32 Read = Engine->Platform->FileRead(S->StreamFile, WritePtr, S->DataSize, S->StreamFileOffset);

		S->StreamFileOffset += Read;
		if (Read < S->DataSize)
		{
			if (S->Loop)
			{
				S->StreamFileOffset = S->StreamBeginFileOffset;
				S->StreamFileOffset += Engine->Platform->FileRead(S->StreamFile, WritePtr + Read, S->DataSize - Read, S->StreamFileOffset);
			}
			else
			{
				for (u64 i=Read; i<S->DataSize; ++i)
				{
					WritePtr[i] = 0;
				}

				for (s32 i=0; i<static_array_len(Engine->SoundSystem->StreamSoundData); ++i)
				{
					stream_sound_data *Stream = Engine->SoundSystem->StreamSoundData + i;
					if (Stream->SoundAsset == S)
					{
						dynamic_array_remove_at(Engine->SoundSystem->StreamSoundData, i);
						break;
					}
				}
			}
		}
	}
}

void sound_update(engine *Engine)
{
	asset_get_all_of_type(Engine, AssetType_Sound, sound_asset_callback);
}

void sound_play(engine *Engine, const char *Name)
{
	asset *Asset = asset_get(Engine, Name);
	if (Asset)
	{
		sound_add_to_stream(Engine, &Asset->SoundAsset);
	}
	else
	{
		//TODO: Can't load it here, it may be a streamed from disk sound
		Engine->Platform->Log("Sound %s is not loaded, not playing\n", Name);
	}
}

void sound_set_gain(struct engine *Engine, const char *Name, f32 Gain)
{
	asset *Asset = asset_get(Engine, Name);
	if (Asset)
	{
		Asset->SoundAsset.Gain = Gain;
	}
	else
	{
		Engine->Platform->Log("Sound %s is not loaded\n", Name);
	}
}

f32 sound_get_gain(struct engine *Engine, const char *Name)
{
	asset *Asset = asset_get(Engine, Name);
	if (Asset)
	{
		return Asset->SoundAsset.Gain;
	}
	else
	{
		Engine->Platform->Log("Sound %s is not loaded\n", Name);
	}

	return -1;
}
