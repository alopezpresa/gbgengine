
#include <android_native_app_glue.h>
#include <android/log.h>
#include <EGL/egl.h>
#include <GLES/gl.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

#include "game.c"
#include "gbg_engine.c"
#include "gbg_input.h"

#ifndef DEBUG
#define LOGI(...)
#define LOG(...)
#else
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "gbg-engine", __VA_ARGS__))
#define LOG(...) ((void)__android_log_print(ANDROID_LOG_INFO, "gbg-engine", __VA_ARGS__))
#endif
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "gbg-engine", __VA_ARGS__))

static engine Engine = {};
static struct AAssetManager* AssetManager = 0;
static struct android_app *AndroidApp = 0;

static int AndroidWindowHeight = 0;
static int AndroidWindowWidth = 0;

//TODO: Fullscreen
//TODO: Manage back button in every screen

//Platform def

static void platform_switch_to_fullscreen()
{
	LOGE("Switch to fullscreen undefined for Android platform");
}

void platform_get_config_path(char *Path, u32 Size)
{
	snprintf(Path, Size, "%s", AndroidApp->activity->externalDataPath);
}

static void platform_get_user_name(char *UserName, u32 Len)
{
	//TODO: This is kind of complicated on android
	snprintf(UserName, Len, "USERNAME");
}

static void platform_get_ipv4_address(char *Buffer)
{
	int Sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (Sock < 0)
	{
		LOGE("Error creating socket %d\n", Sock);
		return;
	}

	struct ifreq IfReq;
	IfReq.ifr_addr.sa_family = AF_INET;

	//Iface 1 is loopback, take the second one
	IfReq.ifr_ifindex = 2;
	ioctl(Sock, SIOCGIFNAME, &IfReq);

	ioctl(Sock, SIOCGIFADDR, &IfReq);

	char *Addr = inet_ntoa(((struct sockaddr_in *)&IfReq.ifr_addr)->sin_addr);
	string_copy(Addr, Buffer);

	int s = string_len(Addr);
	int r = string_len(Buffer);

	close(Sock);
}

static b32 create_socket_and_bind(const char *Host, const char *Port)
{
	int Sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (Sock < 0)
	{
		LOGE("Error creating socket %d\n", Sock);
		return 0;
	}

	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		LOGE("error on getaddrinfo %d\n", Err);
		close(Sock);
		return 0;
	}

	Engine.Net->Socket.Descriptor = Sock;

	char LHost[NI_MAXHOST], Service[NI_MAXSERV];
	getnameinfo(AddrInfo->ai_addr, AddrInfo->ai_addrlen, LHost, NI_MAXHOST, Service, NI_MAXSERV, NI_NUMERICSERV);

    Err = bind(Sock, AddrInfo->ai_addr, AddrInfo->ai_addrlen);

	freeaddrinfo(AddrInfo);
	return 1;
}

static void platform_client_exit()
{
	memory_free(&Engine.Memory, Engine.Net->Host.Address);	
	memory_free(&Engine.Memory, Engine.Net->Host.Port);	
	shutdown(Engine.Net->Socket.Descriptor, SHUT_RD);
	close(Engine.Net->Socket.Descriptor);
	Engine.Net->Socket.Descriptor = 0;
}

static b32 platform_client_join(const char *Host, const char *Port)
{
	//TODO: Check all paths will free this memory up
	Engine.Net->Host.Address = string_dup(&Engine, Host);
	Engine.Net->Host.Port = string_dup(&Engine, Port);

	char LocalHostIp[64];
	platform_get_ipv4_address(LocalHostIp);
	b32 Ret = create_socket_and_bind(LocalHostIp, Port);

	return Ret;
}

static u32 platform_socket_write(file_handle Handle, u8 *Data, u32 DataLen, const char *Host, const char *Port)
{
	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		LOGE("error on getaddrinfo %d. Can't send message\n", Err);
		return 0;
	}

	int Bytes = sendto(Handle.Descriptor, Data, DataLen, 0, AddrInfo->ai_addr, AddrInfo->ai_addrlen);
	freeaddrinfo(AddrInfo);

	return Bytes;
}

static u32 platform_socket_read(file_handle Handle, u8 *Data, u32 DataLen, char *OutHost, char *OutPort)
{
	struct sockaddr SockAddr;
	socklen_t SockAddrLen = sizeof(SockAddr);	

	int Bytes = recvfrom(Handle.Descriptor, Data, DataLen, 0, &SockAddr, &SockAddrLen);

	if (OutHost != 0 && OutPort != 0)
	{
		getnameinfo(&SockAddr, SockAddrLen, OutHost, NI_MAXHOST, OutPort, NI_MAXSERV, NI_NUMERICSERV);
	}

	if (Bytes < 0)
	{
		shutdown(Handle.Descriptor, SHUT_RD);
		close(Handle.Descriptor);
	}

	return Bytes;
}

static u16 platform_net_to_host_16(u16 NetShort)
{
	return ntohs(NetShort);
}

static u32 platform_net_to_host_32(u32 NetInt)
{
	return ntohl(NetInt);
}

static u16 platform_host_to_net_16(u16 HostShort)
{
	return htons(HostShort);
}

static u32 platform_host_to_net_32(u32 HostInt)
{
	return htonl(HostInt);
}

static b32 platform_server_start(const char *Host, const char *Port)
{
	return create_socket_and_bind(Host, Port);
}

static void platform_server_stop()
{
	int fd = Engine.Net->Socket.Descriptor;
	shutdown(fd, SHUT_RDWR);
	close(fd);
	Engine.Net->Socket.Descriptor = 0;
}

static void platform_create_thread(void *(*Func)(void *Param), void *Param)
{
	pthread_t ServerThreadId;
	pthread_create(&ServerThreadId, 0, Func, Param);
}

static void platform_exit()
{
	//NOTE: Not exit in android, just hit back
}

static void platform_exit_thread()
{
	pthread_exit(0);
}

static file_handle platform_file_open(const char *FileName)
{
	file_handle Ret = {};

	if (FileName[0] == '/')
	{
		Ret.Descriptor = open(FileName, O_RDONLY);
		if (Ret.Descriptor < 0)
		{
			LOGE("Couldn't open file %s", FileName);
		}
	}
	else
	{
		Ret.Handle = AAssetManager_open(AssetManager, FileName, AASSET_MODE_STREAMING);
		if (Ret.Handle == 0)
		{
			LOGE("Couldn't open file %s", FileName);
		}
	}
	return Ret;
}

static u32 platform_file_read(file_handle FileHandle, u8 *Buffer, u32 BufferLen, u32 From)
{
	int Read = 0;
	if (FileHandle.Handle)
	{
		int SeekPos = AAsset_seek(FileHandle.Handle, From, SEEK_SET);
		if (SeekPos != From)
		{
			LOGE("Not able to seek to pos %d", From);
		}
		Read = AAsset_read(FileHandle.Handle, Buffer, BufferLen);
	}
	else
	{
		Read = pread(FileHandle.Descriptor, Buffer, BufferLen, From);
	}
	return Read;
}

static void platform_file_close(file_handle FileHandle)
{
	if (FileHandle.Descriptor >= 0)
		close(FileHandle.Descriptor);
	else if (FileHandle.Handle)
		AAsset_close(FileHandle.Handle);
}

void platform_file_write_fully(const char *FullPath, u8 *Content, u32 Size)
{
	int fd = open(FullPath, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
	if (fd >= 0)
	{
		write(fd, Content, Size);
		close(fd);
	}
	else
	{
		LOGE("Write fully failed");
	}
}

static u8* platform_file_read_fully(const char *Name, u32 *Size)
{
	u8 *Buffer = 0;

	//Check if we want to open a file inside the apk or a full path from the device
	if (Name[0] == '/')
	{
		int fd = open(Name, O_RDONLY);
		if (fd >= 0)
		{
			struct stat FileStat;
			fstat(fd, &FileStat);
			*Size = FileStat.st_size;
			Buffer = memory_alloc(&Engine.Memory, FileStat.st_size);
			u32 ReadSize = read(fd, Buffer, FileStat.st_size);
			if (ReadSize != *Size)
			{
				close(fd);
				return 0;
			}
			close(fd);
		}
		else
		{
			LOGE("Couldn't open file %s", Name);
		}

		return Buffer;
	}
	else
	{
		AAsset *file = AAssetManager_open(AssetManager, Name, AASSET_MODE_BUFFER);
		if (file == 0) {
			LOGE("Couldn't open file %s", Name);
			return Buffer;
		}

		*Size = AAsset_getLength(file);
		Buffer = memory_alloc(&Engine.Memory, *Size);

		int Read = AAsset_read(file, Buffer, *Size);
		if (Read != *Size) {
			AAsset_close(file);
			*Size = 0;
			memory_free(&Engine.Memory, Buffer);
			return 0;
		}
	}

	return Buffer;
}

static void platform_mutex(b32 *Lock)
{
	while (__sync_val_compare_and_swap(Lock, 0, 1))
		;
}

static void platform_log(const char *Fmt, ...)
{
	va_list args;
	va_start(args, Fmt);

	__android_log_vprint(ANDROID_LOG_INFO, "gbg-engine", Fmt, args);
}

static float UserAspectRatio = 0.75f;
static u32 WindowWidth = 0;
static u32 WindowHeight = 0;
static void platform_set_window_size(u32 Width, u32 Height)
{
	WindowWidth = Width;
	WindowHeight = Height;
	UserAspectRatio = Height/(float)Width;
	GLsizei w = AndroidWindowWidth;
	GLsizei h = AndroidWindowHeight;

	GLsizei newh = w*UserAspectRatio;
	GLsizei neww = h/UserAspectRatio;

	Engine.Render->ScreenOrigin = Vec2(0, 0);
	Engine.Render->DesignScreenOrigin = Vec2(0, 0);

	if (newh > h)
	{
		s32 DiffH = newh - h;
		Engine.Render->ScreenOrigin.y = (DiffH/2);
		Engine.Render->DesignScreenOrigin.y = (DiffH/2)*(Engine.Render->DesignSize.y/newh);

		//TODO: use glViewport from render?
		glViewport(0, -DiffH/2, w, h+DiffH);
	}
	else if (neww > w)
	{
		s32 DiffW = neww - w;
		Engine.Render->ScreenOrigin.x = (DiffW/2);
		Engine.Render->DesignScreenOrigin.x = (DiffW/2)*(Engine.Render->DesignSize.x/neww);
		glViewport(-DiffW/2, 0, w+DiffW, h);
	}
	else
	{
		glViewport(0, 0, w, h);
	}
}

static void platform_hide_cursor()
{
	//NOT use for this here
}

static f64 platform_get_local_time()
{
	struct timeval Time;
	gettimeofday(&Time, 0);

	return ((Time.tv_sec*1000.0) + (Time.tv_usec/1000.0))/1000.0f;
}

static void platform_set_window_title(const char *Name)
{
	//Not use for this here
}

static u64  platform_get_file_modified_date(const char *Name)
{
	//This function will not be used here... I think
	return 0;
}

void platform_open_web_browser(char *Url)
{
    JNIEnv* lJNIEnv = AndroidApp->activity->env;
	JavaVM* JavaVM = AndroidApp->activity->vm;
    jobject NativeActivity = AndroidApp->activity->clazz;

	JavaVMAttachArgs lJavaVMAttachArgs;
	lJavaVMAttachArgs.version = JNI_VERSION_1_6;
	lJavaVMAttachArgs.name = "NativeThread";
	lJavaVMAttachArgs.group = NULL;

	jint Result = (*JavaVM)->AttachCurrentThread(JavaVM, &lJNIEnv,
											  &lJavaVMAttachArgs);
	if (Result == JNI_ERR) {
		return;
	}

	jclass ActClass = (*lJNIEnv)->GetObjectClass(lJNIEnv, NativeActivity);

	jmethodID OpenUrlId = (*lJNIEnv)->GetMethodID(lJNIEnv, ActClass, "openURL", "(Ljava/lang/String;)V");
	jstring SUrl = (*lJNIEnv)->NewStringUTF(lJNIEnv, Url);
	(*lJNIEnv)->CallVoidMethod(lJNIEnv, NativeActivity, OpenUrlId, SUrl);
	(*lJNIEnv)->DeleteLocalRef(lJNIEnv, SUrl);
}

//Stolen from: https://groups.google.com/g/android-ndk/c/Tk3g00wLKhk
void displayKeyboard(bool pShow) {
// Attaches the current thread to the JVM.
	jint lResult;
	jint lFlags = 0;

	JavaVM* lJavaVM = AndroidApp->activity->vm;
	JNIEnv* lJNIEnv = AndroidApp->activity->env;

	JavaVMAttachArgs lJavaVMAttachArgs;
	lJavaVMAttachArgs.version = JNI_VERSION_1_6;
	lJavaVMAttachArgs.name = "NativeThread";
	lJavaVMAttachArgs.group = NULL;

	lResult = (*lJavaVM)->AttachCurrentThread(lJavaVM, &lJNIEnv,
										 &lJavaVMAttachArgs);
	if (lResult == JNI_ERR) {
		return;
	}

// Retrieves NativeActivity.
	jobject lNativeActivity = AndroidApp->activity->clazz;
	jclass ClassNativeActivity = (*lJNIEnv)->GetObjectClass(lJNIEnv, lNativeActivity);

// Retrieves Context.INPUT_METHOD_SERVICE.
	jclass ClassContext = (*lJNIEnv)->FindClass(lJNIEnv, "android/content/Context");
	jfieldID FieldINPUT_METHOD_SERVICE =
			(*lJNIEnv)->GetStaticFieldID(lJNIEnv, ClassContext,
									  "INPUT_METHOD_SERVICE", "Ljava/lang/String;");
	jobject INPUT_METHOD_SERVICE =
			(*lJNIEnv)->GetStaticObjectField(lJNIEnv, ClassContext,
										  FieldINPUT_METHOD_SERVICE);
	//jniCheck(INPUT_METHOD_SERVICE);

// Runs getSystemService(Context.INPUT_METHOD_SERVICE).
	jclass ClassInputMethodManager = (*lJNIEnv)->FindClass(lJNIEnv,
			"android/view/inputmethod/InputMethodManager");
	jmethodID MethodGetSystemService = (*lJNIEnv)->GetMethodID(lJNIEnv,
			ClassNativeActivity, "getSystemService",
			"(Ljava/lang/String;)Ljava/lang/Object;");
	jobject lInputMethodManager = (*lJNIEnv)->CallObjectMethod(lJNIEnv,
			lNativeActivity, MethodGetSystemService,
			INPUT_METHOD_SERVICE);

// Runs getWindow().getDecorView().
	jmethodID MethodGetWindow = (*lJNIEnv)->GetMethodID(lJNIEnv,
			ClassNativeActivity, "getWindow",
			"()Landroid/view/Window;");
	jobject lWindow = (*lJNIEnv)->CallObjectMethod(lJNIEnv, lNativeActivity,
												MethodGetWindow);
	jclass ClassWindow = (*lJNIEnv)->FindClass(lJNIEnv,
			"android/view/Window");
	jmethodID MethodGetDecorView = (*lJNIEnv)->GetMethodID(lJNIEnv,
			ClassWindow, "getDecorView", "()Landroid/view/View;");
	jobject lDecorView = (*lJNIEnv)->CallObjectMethod(lJNIEnv, lWindow,
												   MethodGetDecorView);

	if (pShow) {
// Runs lInputMethodManager.showSoftInput(...).
		jmethodID MethodShowSoftInput = (*lJNIEnv)->GetMethodID(lJNIEnv,
				ClassInputMethodManager, "showSoftInput",
				"(Landroid/view/View;I)Z");
		jboolean lResult = (*lJNIEnv)->CallBooleanMethod(lJNIEnv,
				lInputMethodManager, MethodShowSoftInput,
				lDecorView, lFlags);
	} else {
// Runs lWindow.getViewToken()
		jclass ClassView = (*lJNIEnv)->FindClass(lJNIEnv,
				"android/view/View");
		jmethodID MethodGetWindowToken = (*lJNIEnv)->GetMethodID(lJNIEnv,
				ClassView, "getWindowToken", "()Landroid/os/IBinder;");
		jobject lBinder = (*lJNIEnv)->CallObjectMethod(lJNIEnv, lDecorView,
													MethodGetWindowToken);

// lInputMethodManager.hideSoftInput(...).
		jmethodID MethodHideSoftInput = (*lJNIEnv)->GetMethodID(lJNIEnv,
				ClassInputMethodManager, "hideSoftInputFromWindow",
				"(Landroid/os/IBinder;I)Z");
		jboolean lRes = (*lJNIEnv)->CallBooleanMethod(lJNIEnv,
				lInputMethodManager, MethodHideSoftInput,
				lBinder, lFlags);
	}

// Finished with the JVM.
	(*lJavaVM)->DetachCurrentThread(lJavaVM);
}


static void platform_keyboard_show()
{
	displayKeyboard(1);
}

static void platform_keyboard_hide()
{
	displayKeyboard(0);
}

//SOUND
//
SLPlayItf SoundPlayer;
SLAndroidSimpleBufferQueueItf SoundPlayerBufQ;
static bool is_done_buffer = true;
char StreamBuffer[4096];

static void audio_update()
{
	if (is_done_buffer)
	{
		sound_request_buffer_mix(&Engine, StreamBuffer, 4096);
		(*SoundPlayerBufQ)->Enqueue(SoundPlayerBufQ, StreamBuffer, 4096);
		(*SoundPlayer)->SetPlayState(SoundPlayer, SL_PLAYSTATE_PLAYING);
		is_done_buffer = false;
	}
	sound_update(&Engine);
}

static void SLAPIENTRY sound_play_callback(SLPlayItf Player, void *Context, SLuint32 Event)
{
	if (Event & SL_PLAYEVENT_HEADATEND)
	{
		is_done_buffer = 1;
	}
}

static void init_sles()
{
	SLObjectItf SoundEngineObj;
	SLEngineItf SoundEngine;
	slCreateEngine(&SoundEngineObj, 0, 0, 0, 0, 0);
	(*SoundEngineObj)->Realize(SoundEngineObj, SL_BOOLEAN_FALSE);
	(*SoundEngineObj)->GetInterface(SoundEngineObj, SL_IID_ENGINE, &SoundEngine);

	SLObjectItf SoundOutputMixObj;
	SLVolumeItf SoundOutputMix;

	const SLInterfaceID Ids[] = {SL_IID_VOLUME};
	const SLboolean Req[] = {SL_BOOLEAN_FALSE};
	(*SoundEngine)->CreateOutputMix(SoundEngine, &SoundOutputMixObj, 1, Ids, Req);
	(*SoundOutputMixObj)->Realize(SoundOutputMixObj, SL_BOOLEAN_FALSE);

	SLDataLocator_AndroidSimpleBufferQueue InLoc;
	InLoc.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
	InLoc.numBuffers = 1;

	SLDataFormat_PCM Format;
	Format.formatType = SL_DATAFORMAT_PCM;
	Format.numChannels = 2;
	Format.samplesPerSec = SL_SAMPLINGRATE_44_1;
	Format.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
	Format.containerSize = 16;
	Format.channelMask = SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT;
	Format.endianness = SL_BYTEORDER_LITTLEENDIAN;
	 
	SLDataSource Src;
	Src.pLocator = &InLoc;
	Src.pFormat = &Format;

	SLDataLocator_OutputMix OutLoc;
	OutLoc.locatorType = SL_DATALOCATOR_OUTPUTMIX;
	OutLoc.outputMix = SoundOutputMixObj;
	 
	SLDataSink Sink;
	Sink.pLocator = &OutLoc;
	Sink.pFormat = 0;

	const SLInterfaceID ids[] = { SL_IID_VOLUME, SL_IID_ANDROIDSIMPLEBUFFERQUEUE };
	const SLboolean req[] = { SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE };

	SLObjectItf SoundPlayerObj;
	(*SoundEngine)->CreateAudioPlayer(SoundEngine, &SoundPlayerObj, &Src, &Sink, 2, ids, req);
	(*SoundPlayerObj)->Realize(SoundPlayerObj, SL_BOOLEAN_FALSE);
	(*SoundPlayerObj)->GetInterface(SoundPlayerObj, SL_IID_PLAY, &SoundPlayer);
	(*SoundPlayerObj)->GetInterface(SoundPlayerObj, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &SoundPlayerBufQ);

	(*SoundPlayer)->RegisterCallback(SoundPlayer, sound_play_callback, 0);
	(*SoundPlayer)->SetCallbackEventsMask(SoundPlayer, SL_PLAYEVENT_HEADATEND);
}

static EGLDisplay Display = 0;
static EGLSurface Surface = 0;
static EGLContext Context = 0;

static void close_egl()
{
	LOG("Closing display");
	if (Display != EGL_NO_DISPLAY) {
		eglMakeCurrent(Display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		if (Context != EGL_NO_CONTEXT) {
			eglDestroyContext(Display, Context);
		}
		if (Surface != EGL_NO_SURFACE) {
			eglDestroySurface(Display, Surface);
		}
		eglTerminate(Display);
	}
	Display = EGL_NO_DISPLAY;
	Context = EGL_NO_CONTEXT;
	Surface = EGL_NO_SURFACE;
}

static void init_egl(struct android_app* state)
{
	Display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	EGLint Major, Minor;
	if (eglInitialize(Display, &Major, &Minor))
		LOGI("Initialized EGL Display %d.%d", Major, Minor);
	else
		LOGI("ERROR Initializing Display");

	EGLint Dc = 8;
	EGLint Dd = 24;
	//First to get a matching number
	EGLint AttribList[] = {EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
							EGL_BLUE_SIZE, Dc,
							EGL_RED_SIZE, Dc,
							EGL_GREEN_SIZE, Dc,
							EGL_ALPHA_SIZE, Dc,
							EGL_DEPTH_SIZE, Dd,
						EGL_NONE};
	EGLint MatchedConfigSize;
	EGLBoolean Ret = eglChooseConfig(Display, AttribList, 0, 0, &MatchedConfigSize);
	if (!Ret)
	{
		LOGE("eglChooseConfig returned false");
		return;
	}

	EGLConfig Configs[MatchedConfigSize];
	Ret = eglChooseConfig(Display, AttribList, &Configs, MatchedConfigSize, &MatchedConfigSize);
	if (!Ret)
	{
		LOGE("eglChooseConfig returned false");
		return;
	}

	EGLConfig MatchedConfig;
	for (int i=0; i<MatchedConfigSize; ++i)
	{
		EGLint r, g, b, a, d;
		eglGetConfigAttrib(Display, Configs[i], EGL_BLUE_SIZE, &b);
		eglGetConfigAttrib(Display, Configs[i], EGL_RED_SIZE, &r);
		eglGetConfigAttrib(Display, Configs[i], EGL_GREEN_SIZE, &g);
		eglGetConfigAttrib(Display, Configs[i], EGL_ALPHA_SIZE, &a);
		eglGetConfigAttrib(Display, Configs[i], EGL_DEPTH_SIZE, &d);
		if (Dc == b && Dc == r && Dc == g && Dc == a && Dd == d)
		{
			MatchedConfig = Configs[i];
			break;
		}
	}

	Surface = eglCreateWindowSurface(Display, MatchedConfig, state->window, 0);

	eglGetConfigAttrib(Display, MatchedConfig, EGL_MIN_SWAP_INTERVAL, &Minor);
	eglGetConfigAttrib(Display, MatchedConfig, EGL_MAX_SWAP_INTERVAL, &Major);

	GLint ContextAttr[] = {EGL_CONTEXT_MAJOR_VERSION, 3,
					EGL_CONTEXT_MINOR_VERSION, 0,
					EGL_NONE};

	Context = eglCreateContext(Display, MatchedConfig, 0, ContextAttr);

	if (eglMakeCurrent(Display, Surface, Surface, Context) == EGL_FALSE)
	{
		LOGE("error making display current"); 
	}

	eglSwapInterval(Display, 1);

	EGLint w, h;
	eglQuerySurface(Display, Surface, EGL_WIDTH, &w);
    eglQuerySurface(Display, Surface, EGL_HEIGHT, &h);
	
	AndroidWindowWidth = w;
	AndroidWindowHeight = h;
	UserAspectRatio = h/(float)w;

	LOGI(" -> EGL Succesfully initialized!");
}

static void *android_handle_app_cmd(struct android_app* android_app, int8_t cmd)
{
	switch(cmd) {
		case APP_CMD_INIT_WINDOW:
			init_egl(android_app);
			if (Engine.HasBeenSuspended)
			{
				platform_set_window_size(WindowWidth, WindowHeight);
				render_gpu_driver_init(&Engine);
			}
			return 1;

		case APP_CMD_TERM_WINDOW:
            // The window is being hidden or closed, clean it up.
			render_gpu_driver_shutdown(&Engine);
			render_sprite_batch_reset(&Engine);
			close_egl();
			Engine.HasBeenSuspended = 1;
			return 1;

		case APP_CMD_GAINED_FOCUS:
			(*SoundPlayer)->SetPlayState(SoundPlayer, SL_PLAYSTATE_PLAYING);
			return 1;

		case APP_CMD_LOST_FOCUS:
			(*SoundPlayer)->SetPlayState(SoundPlayer, SL_PLAYSTATE_STOPPED);
			return 1;
	}

	return 0;
}

static void android_set_input_motion(AInputEvent *Event, button_state State)
{
	input *Input = Engine.Input;
	float xPos = AMotionEvent_getX(Event, 0);
	float yPos = AndroidWindowHeight - AMotionEvent_getY(Event, 0);

	vec2 Origin = Engine.Render->ScreenOrigin;
	vec2 Scale = Vec2(Engine.Render->DesignSize.x / (AndroidWindowWidth + Origin.x*2), Engine.Render->DesignSize.y / (AndroidWindowHeight + Origin.y*2));
	xPos = xPos * Scale.x;
	xPos += Origin.x * Scale.x;
	yPos = yPos * Scale.y;
	yPos += Origin.y * Scale.y;

	struct motion_event Ev = {State, Vec2(xPos, yPos)};
	dynamic_array_add(&Input->MotionEvents, Ev);

	//Touch also treated as mouse, and always forward as first button
	Input->MouseState.MousePos = Vec2(xPos, yPos);
	Input->MouseState.LeftButton.CurState = State;
}

#define STICK_DEAD_ZONE 0.004*0.004

vec2 get_axis_movement(AInputEvent *Event, int AxisX, int AxisY, b32 *GotInput)
{
	float x = AMotionEvent_getAxisValue(Event, AMOTION_EVENT_AXIS_X,0);
	float y = AMotionEvent_getAxisValue(Event, AMOTION_EVENT_AXIS_Y, 0);

	if (x*x + y*y < STICK_DEAD_ZONE)
	{
		*GotInput = true;
		return Vec2(x, -y);
	}

	return Vec2(0, 0);
}

static int32_t android_handle_input(struct android_app *app, AInputEvent *Event){
	input *Input = Engine.Input;

	int Source = AInputEvent_getSource(Event);

	if ((Source & AINPUT_SOURCE_TOUCHSCREEN) == AINPUT_SOURCE_TOUCHSCREEN)
	{
		int Type = AInputEvent_getType(Event);
		if (Type == AINPUT_EVENT_TYPE_MOTION)
		{
			int Action = AMotionEvent_getAction(Event);
			if (Action == AMOTION_EVENT_ACTION_DOWN)
			{
				android_set_input_motion(Event, ButtonState_Down);
			}
			else if(Action == AMOTION_EVENT_ACTION_UP)
			{
				android_set_input_motion(Event, ButtonState_Up);
			}
			else if (Action == AMOTION_EVENT_ACTION_MOVE)
			{
				android_set_input_motion(Event, ButtonState_Down);
			}
		}
	}
	else if ((Source & AINPUT_SOURCE_CLASS_BUTTON) == AINPUT_SOURCE_CLASS_BUTTON)
	{
		int Type = AInputEvent_getType(Event);
		if (Type == AINPUT_EVENT_TYPE_KEY)
		{
			int Action = AKeyEvent_getAction(Event);
			int KeyCode = AKeyEvent_getKeyCode(Event);

			if (KeyCode == AKEYCODE_BACK)
			{
				//If OverrideBackBehavior is set, then the system doesn't close the app, but send a ESCAPE key to the game
				if (Engine.Platform->Android.OverrideBackBehavior)
				{
					key Key = {KeyCode_Escape, 0, ButtonState_Down};
					process_key(&Engine, &Key);
					Key.State = ButtonState_Up;
					process_key(&Engine, &Key);
				}

				return Engine.Platform->Android.OverrideBackBehavior;
			}
			//Do i have to do something with the volume or will this work automatically?
			else if (KeyCode == AKEYCODE_VOLUME_DOWN)
			{

			}
			else if (KeyCode == AKEYCODE_VOLUME_UP)
			{

			}
			else
			{
				key Key = KeyFromCode(KeyCode, Action == AKEY_EVENT_ACTION_DOWN ? ButtonState_Down : ButtonState_Up);
				process_key(&Engine, &Key);
			}
		}
	}
	else if (Source == AINPUT_SOURCE_JOYSTICK)
	{
		gamepad *Gamepad = Input->Gamepads;
		Gamepad->IsConnected = false;
        int Type = AInputEvent_getType(Event);
        if (Type == AINPUT_EVENT_TYPE_MOTION)
		{
			Gamepad->LeftThumb = get_axis_movement(Event, AMOTION_EVENT_AXIS_X, AMOTION_EVENT_AXIS_Y, &Gamepad->IsConnected);
			Gamepad->RightThumb = get_axis_movement(Event, AMOTION_EVENT_AXIS_Z, AMOTION_EVENT_AXIS_RZ, &Gamepad->IsConnected);

			float x = AMotionEvent_getAxisValue(Event, AMOTION_EVENT_AXIS_HAT_X,0);
			float y = AMotionEvent_getAxisValue(Event, AMOTION_EVENT_AXIS_HAT_Y, 0);
			if (x > 0)
			{
				Gamepad->Buttons[Gamepad_DpadRight].CurState = ButtonState_Down;
				Gamepad->IsConnected = true;
			}
			else if (x < 0)
			{
				Gamepad->Buttons[Gamepad_DpadLeft].CurState = ButtonState_Down;
				Gamepad->IsConnected = true;
			}
			else
			{
				Gamepad->IsConnected = true;
				Gamepad->Buttons[Gamepad_DpadRight].CurState = ButtonState_Up;
				Gamepad->Buttons[Gamepad_DpadLeft].CurState = ButtonState_Up;
			}

			if (y > 0)
			{
				Gamepad->Buttons[Gamepad_DpadDown].CurState = ButtonState_Down;
				Gamepad->IsConnected = true;
			}
			else if (y < 0)
			{
				Gamepad->Buttons[Gamepad_DpadUp].CurState = ButtonState_Down;
				Gamepad->IsConnected = true;
			}
			else
			{
				Gamepad->IsConnected = true;
				Gamepad->Buttons[Gamepad_DpadDown].CurState = ButtonState_Up;
				Gamepad->Buttons[Gamepad_DpadUp].CurState = ButtonState_Up;
			}
		}
	}
	else if ((Source & AINPUT_SOURCE_GAMEPAD) == AINPUT_SOURCE_GAMEPAD)
	{
		Input->Gamepads->IsConnected = true;
		int KCode = AKeyEvent_getKeyCode(Event);
		int Action = AMotionEvent_getAction(Event);
		switch (KCode)
		{
			case AKEYCODE_BUTTON_A:
				Input->Gamepads->Buttons[Gamepad_A].CurState = Action == AMOTION_EVENT_ACTION_DOWN ? ButtonState_Down : ButtonState_Up;
				break;
			case AKEYCODE_BUTTON_B:
				Input->Gamepads->Buttons[Gamepad_B].CurState = Action == AMOTION_EVENT_ACTION_DOWN ? ButtonState_Down : ButtonState_Up;;
				break;
			case AKEYCODE_BUTTON_X:
				Input->Gamepads->Buttons[Gamepad_X].CurState = Action == AMOTION_EVENT_ACTION_DOWN ? ButtonState_Down : ButtonState_Up;;
				break;
			case AKEYCODE_BUTTON_Y:
				Input->Gamepads->Buttons[Gamepad_Y].CurState = Action == AMOTION_EVENT_ACTION_DOWN ? ButtonState_Down : ButtonState_Up;;
				break;
			case AKEYCODE_BUTTON_L1:
				Input->Gamepads->Buttons[Gamepad_LeftShoulder].CurState = Action == AMOTION_EVENT_ACTION_DOWN ? ButtonState_Down : ButtonState_Up;;
				break;
			case AKEYCODE_BUTTON_R1:
				Input->Gamepads->Buttons[Gamepad_RightShoulder].CurState = Action == AMOTION_EVENT_ACTION_DOWN ? ButtonState_Down : ButtonState_Up;;
				break;
		}
	}
	else if (Source == 0) //Unknown source, could be using the soft keyboard
	{
		//TODO: see link for unicode support: https://stackoverflow.com/questions/21124051/receive-complete-android-unicode-input-in-c-c

		int Type = AInputEvent_getType(Event);
		if (Type == AINPUT_EVENT_TYPE_KEY)
		{
			int Action = AKeyEvent_getAction(Event);
			int KeyCode = AKeyEvent_getKeyCode(Event);
			key Key = KeyFromCode(KeyCode, Action);
			process_key(&Engine, &Key);
		}
	}

    return 0;
}

JNIEXPORT void JNICALL Java_com_gbgengine_puyoonline_GbGActivity_onUnicode(JNIEnv *Env, jobject Obj, jint unicode)
{
	if (unicode)
	{
		key K = KeyFromChar(unicode);
		process_key(&Engine, &K);
	}
}

/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app* state) {
	AndroidApp = state;
	state->onAppCmd = android_handle_app_cmd;
	state->onInputEvent = android_handle_input;

	AssetManager = state->activity->assetManager;

    platform Platform = {};
	Platform.FileReadFully = platform_file_read_fully;
	Platform.FileWriteFully = platform_file_write_fully;
	Platform.FileOpen = platform_file_open;
	Platform.FileRead = platform_file_read;
	Platform.FileClose = platform_file_close;
	Platform.Log = platform_log;
	Platform.SetWindowSize = platform_set_window_size;
	Platform.HideCursor = platform_hide_cursor;
	Platform.GetLocalTime = platform_get_local_time;
	Platform.SetWindowTitle = platform_set_window_title;
	Platform.GetFileModifiedDate = platform_get_file_modified_date;
	Platform.Exit = platform_exit;
	Platform.SwitchFullScreen = platform_switch_to_fullscreen;
	Platform.Mutex = platform_mutex;
	Platform.ServerStart = platform_server_start;
	Platform.ServerStop = platform_server_stop;
	Platform.ClientJoin = platform_client_join;
	Platform.ClientExit = platform_client_exit;
	Platform.SocketRead = platform_socket_read;
	Platform.SocketWrite = platform_socket_write;
	Platform.ThreadCreate = platform_create_thread;
	Platform.ThreadExit = platform_exit_thread;
	Platform.NetToHost16 = platform_net_to_host_16;
	Platform.NetToHost32 = platform_net_to_host_32;
	Platform.HostToNet16 = platform_host_to_net_16;
	Platform.HostToNet32 = platform_host_to_net_32;
	Platform.GetIp4Address = platform_get_ipv4_address;
	Platform.GetUserName = platform_get_user_name;
	Platform.GetConfigPath = platform_get_config_path;
	Platform.KeyboardShow = platform_keyboard_show;
	Platform.KeyboardHide = platform_keyboard_hide;
	Platform.OpenUrl = platform_open_web_browser;
	Engine.Platform = &Platform;

	render Render = {};
	Engine.Render = &Render;

	input Input = {};
	Engine.Input = &Input;

	imgui ImGui;
	Engine.ImGui = &ImGui;

	net Net = {};
	Engine.Net = &Net;

	Engine.AssetDatabase.RootPath = "";

	u32 MemSize = MB(10);
	void *LocalMem = mmap((void*)0x12341234, MemSize, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	Engine.Memory = memory_init(LocalMem, MemSize, platform_mutex);

	engine_init(&Engine);

	init_sles();

	struct timeval Time0;
	struct timeval Time1;
	gettimeofday(&Time0, 0);
    while(state->destroyRequested == 0)
    {
		gettimeofday(&Time1, 0);
		f32 DeltaTime = (((Time1.tv_sec - Time0.tv_sec)*1000) + ((Time1.tv_usec - Time0.tv_usec)/1000.0))/1000.0f;
		gettimeofday(&Time0, 0);

		int events;
		struct android_poll_source *PollSource = 0;

		if (Display)
		{
			glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		}

		while (ALooper_pollAll(0, 0, &events, &PollSource) >= 0)
		{
			if (PollSource)
			{
				PollSource->process(state, PollSource);
			}
		}

		if (Display)
		{
			game_update(&Engine, DeltaTime);
			engine_update(&Engine, DeltaTime);
			audio_update();
			eglSwapBuffers(Display, Surface);
		}
    }
}

