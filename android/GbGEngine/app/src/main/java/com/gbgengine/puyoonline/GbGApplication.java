package com.gbgengine.puyoonline;

import android.app.Application;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

public class GbGApplication extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        GbGApplication.context = getApplicationContext();
    }

    static public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
