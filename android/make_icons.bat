
mkdir mipmap-hdpi
mkdir mipmap-xhdpi
mkdir mipmap-xxhdpi
mkdir mipmap-xxxhdpi
mkdir mipmap-mdpi
magick ..\android\GbGEngine\app\src\main\assets\res\appicon.ico -resize 192x192 mipmap-xxxhdpi/ic_launcher.webp
magick ..\android\GbGEngine\app\src\main\assets\res\appicon.ico -resize 144x144 mipmap-xxhdpi/ic_launcher.webp
magick ..\android\GbGEngine\app\src\main\assets\res\appicon.ico -resize 96x96 mipmap-xhdpi/ic_launcher.webp
magick ..\android\GbGEngine\app\src\main\assets\res\appicon.ico -resize 72x72 mipmap-hdpi/ic_launcher.webp
magick ..\android\GbGEngine\app\src\main\assets\res\appicon.ico -resize 48x48 mipmap-mdpi/ic_launcher.webp
