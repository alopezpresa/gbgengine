
#ifndef GBG_KEYCODES_H
#define GBG_KEYCODES_H

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef GetUserName

enum key_code 
{
	KeyCode_BackSpace	= 	VK_BACK,
	KeyCode_Tab		= 	VK_TAB,

	KeyCode_Clear	= 	VK_CLEAR,
	KeyCode_Enter	= 	VK_RETURN,

	KeyCode_Shift	=	VK_SHIFT,
	KeyCode_Control	=	VK_CONTROL,
	KeyCode_Alt		=	VK_MENU,
	KeyCode_Pause	=	VK_PAUSE,
	KeyCode_CapsLock=	VK_CAPITAL,

	KeyCode_Escape	=	VK_ESCAPE,

	KeyCode_Spacebar=	VK_SPACE,

	KeyCode_PageUp	=	VK_PRIOR,
	KeyCode_PageDown=	VK_NEXT,

	KeyCode_End		=	VK_END,
	KeyCode_Home	= 	VK_HOME,

	KeyCode_Left 	= 	VK_LEFT,
	KeyCode_Up 	 	=	VK_UP ,
	KeyCode_Right  	=	VK_RIGHT,
	KeyCode_Down  	=	VK_DOWN,

	KeyCode_Select	=  	VK_SELECT,
	KeyCode_Print	=   VK_PRINT,
	KeyCode_Execute	=   VK_EXECUTE,
	KeyCode_Snapshot=   VK_SNAPSHOT,
	KeyCode_Insert	=   VK_INSERT,
	KeyCode_Delete	=   VK_DELETE,
	KeyCode_Help	=   VK_HELP,

	KeyCode_0       =   '0', 
	KeyCode_1       =   '1',
	KeyCode_2       =   '2',
	KeyCode_3       =   '3',
	KeyCode_4       =   '4',
	KeyCode_5       =   '5',
	KeyCode_6       =   '6',
	KeyCode_7       =   '7',
	KeyCode_8       =   '8',
	KeyCode_9       =   '9',
	KeyCode_A       =   'A',
	KeyCode_B       =   'B',
	KeyCode_C       =   'C',
	KeyCode_D       =   'D',
	KeyCode_E       =   'E',
	KeyCode_F       =   'F',
	KeyCode_G       =   'G',
	KeyCode_H       =   'H',
	KeyCode_I       =   'I',
	KeyCode_J       =   'J',
	KeyCode_K       =   'K',
	KeyCode_L       =   'L',
	KeyCode_M       =   'M',
	KeyCode_N       =   'N',
	KeyCode_O       =   'O',
	KeyCode_P       =   'P',
	KeyCode_Q       =   'Q',
	KeyCode_R       =   'R',
	KeyCode_S       =   'S',
	KeyCode_T       =   'T',
	KeyCode_U       =   'U',
	KeyCode_V       =   'V',
	KeyCode_W       =   'W',
	KeyCode_X       =   'X',
	KeyCode_Y       =   'Y',
	KeyCode_Z       =   'Z',

	KeyCode_LeftWin	= 	VK_LWIN,
	KeyCode_RightWin=  	VK_RWIN,
	KeyCode_Apps	=  	VK_APPS,

	KeyCode_Sleep	=  	VK_SLEEP,

	KeyCode_NP_0	= 	VK_NUMPAD0, 
	KeyCode_NP_1	= 	VK_NUMPAD1, 
	KeyCode_NP_2	= 	VK_NUMPAD2, 
	KeyCode_NP_3	= 	VK_NUMPAD3, 
	KeyCode_NP_4	= 	VK_NUMPAD4, 
	KeyCode_NP_5	= 	VK_NUMPAD5, 
	KeyCode_NP_6	= 	VK_NUMPAD6, 
	KeyCode_NP_7	= 	VK_NUMPAD7, 
	KeyCode_NP_8	= 	VK_NUMPAD8, 
	KeyCode_NP_9	= 	VK_NUMPAD9, 
	KeyCode_Multiply= 	VK_MULTIPLY, 
	KeyCode_Add		= 	VK_ADD, 
	KeyCode_Sep		= 	VK_SEPARATOR,
	KeyCode_Sub		= 	VK_SUBTRACT, 
	KeyCode_Decimal	= 	VK_DECIMAL, 
	KeyCode_Div		= 	VK_DIVIDE, 
	KeyCode_F1		= 	VK_F1, 
	KeyCode_F2		= 	VK_F2, 
	KeyCode_F3		= 	VK_F3, 
	KeyCode_F4		= 	VK_F4, 
	KeyCode_F5		= 	VK_F5, 
	KeyCode_F6		= 	VK_F6, 
	KeyCode_F7		= 	VK_F7, 
	KeyCode_F8		= 	VK_F8, 
	KeyCode_F9		= 	VK_F9, 
	KeyCode_F10		= 	VK_F10, 
	KeyCode_F11		= 	VK_F11, 
	KeyCode_F12		= 	VK_F12, 
	KeyCode_F13		= 	VK_F13, 
	KeyCode_F14		= 	VK_F14, 
	KeyCode_F15		= 	VK_F15, 
	KeyCode_F16		= 	VK_F16, 
	KeyCode_F17		= 	VK_F17, 
	KeyCode_F18		= 	VK_F18, 
	KeyCode_F19		= 	VK_F19, 
	KeyCode_F20		= 	VK_F20, 
	KeyCode_F21		= 	VK_F21, 
	KeyCode_F22		= 	VK_F22, 
	KeyCode_F23		= 	VK_F23, 
	KeyCode_F24		= 	VK_F24      
};

#endif
