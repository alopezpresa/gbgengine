#version 330

in vec4 theColor;
out vec4 outputColor;

uniform vec4 TintColor;

void main()
{
   outputColor = theColor * TintColor;
}
