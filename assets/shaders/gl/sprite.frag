#version 330

in vec2 OutUV;
in vec4 OutColor;
out vec4 FinalColor;

uniform sampler2D TextureAtlas;

void main()
{
	FinalColor = texture(TextureAtlas, OutUV) * OutColor;
}

