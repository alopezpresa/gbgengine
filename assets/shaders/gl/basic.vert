#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
uniform mat4 ProjectionMatrix;
uniform mat4 MV;

out vec4 theColor;
void main()
{
    gl_Position = ProjectionMatrix * MV * vec4(position.xyz, 1);
	theColor = color;
}
