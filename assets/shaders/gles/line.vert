#version 300 es

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
uniform mat4 ProjectionMatrix;

out vec4 theColor;
void main()
{
    gl_Position = ProjectionMatrix * vec4(position.xyz, 1);
	theColor = color;
}
