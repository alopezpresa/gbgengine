#version 300 es

in lowp vec4 theColor;
out lowp vec4 outputColor;

uniform lowp vec4 TintColor;

void main()
{
   outputColor = theColor * TintColor;
}
