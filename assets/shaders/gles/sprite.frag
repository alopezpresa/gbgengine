#version 300 es

in lowp vec2 OutUV;
in mediump vec4 OutColor;
out mediump vec4 FinalColor;

uniform sampler2D TextureAtlas;

void main()
{
	FinalColor = texture(TextureAtlas, OutUV) * OutColor;
}

