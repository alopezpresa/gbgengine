#include "gbg_assets.h"
#include "gbg_engine.h"
#include "gbg_string.h"
#include "gbg_utils.h"

#include <string.h> //for memset

#define CheckCC(Ptr, A, B, C, D) (Ptr[0] == A && Ptr[1] == B && Ptr[2] == C && Ptr[3] == D)

static asset *asset_get_raw(struct engine *Engine, const char *Name, u32 *StringId)
{
	u32 Sid = string_sid(Name);
	*StringId = Sid;
	u32 Index = Sid % static_array_len(Engine->AssetDatabase.Assets);
	asset *Asset = Engine->AssetDatabase.Assets + Index;
	return Asset;
}

static b32 asset_find_in_list(asset **Asset, u32 Sid)
{
	asset *ItAsset = *Asset;
	// Lookup
	while(1)
	{
		//TODO: What happens if the Sid collides? how often may that happen?
		//Should I also check for the pathname to be sure? But I don't what to check it every frame
		//Is it safe to keep the Asset pointer? The asset database is stable, but what can happen with an invalid asset?
		if (ItAsset->Sid == Sid)
		{
			*Asset = ItAsset;
			return 1;
		}

		if (ItAsset->Next == 0)
			break;

		ItAsset = ItAsset->Next;
	}

	*Asset = ItAsset;
	return 0;
}

asset *asset_get(struct engine *Engine, const char *Name)
{
	u32 Sid;
	asset *Asset = asset_get_raw(Engine, Name, &Sid);

	if (Asset->Sid == 0)
	{
		return 0;
	}

	asset_find_in_list(&Asset, Sid);
	return Asset;
}

static asset *asset_get_or_create(engine *Engine, const char *Name, u32 *Sid)
{
	asset *Asset = asset_get_raw(Engine, Name, Sid);

	//1. Asset is empty, it can be used
	if (Asset->Sid == 0)
	{
		Asset->Sid = *Sid;
		return Asset;
	}

	if (asset_find_in_list(&Asset, *Sid))
		return Asset;

	//2. Didn't find it, create a new one
	asset *NewAsset = memory_alloc_type(&Engine->Memory, asset);
	memset(NewAsset, 0, sizeof(asset));
	NewAsset->Sid = *Sid;
	Asset->Next = NewAsset;

	return NewAsset;
}

static char *asset_get_full_path(engine *Engine, const char *Name)
{
	char *Ret = 0;
#if PLAT_ANDROID
	Ret = string_dup(Engine, Name);
#else
	Ret = string_new(Engine, "%s/%s", Engine->AssetDatabase.RootPath, Name);
#endif

	return Ret;
}

u8 *asset_load_file(struct engine *Engine, const char *Name, u32 *Size)
{
	char *FullPath = asset_get_full_path(Engine, Name);
	char *Ret = Engine->Platform->FileReadFully(FullPath, Size);
	memory_free(&Engine->Memory, FullPath);
	
	return Ret;
}

static asset *prepare_asset(engine *Engine, const char *Name)
{
	u32 Sid = 0;
	asset *Asset = asset_get_or_create(Engine, Name, &Sid);

	Asset->Sid = Sid;
	Asset->PathName = string_dup(Engine, Name);

	return Asset;
}

static u8 *prepare_asset_and_get_file(struct engine *Engine, const char *Name, asset **OutAsset)
{
	*OutAsset = prepare_asset(Engine, Name); 

	u32 Size;
	u8 *File = asset_load_file(Engine, Name, &Size);

	return File;
}

b32 asset_initialize_sound(engine *Engine, asset *Asset, u8 *Data)
{
	if (CheckCC(Data, 'R', 'I', 'F', 'F'))
	{
		u32 *DataSize = (u32*)(Data + 4);
		u8 *FileType = Data + 8;
		if (CheckCC(FileType, 'W', 'A', 'V', 'E'))
		{
			FileType += 4;
			if (CheckCC(FileType, 'f', 'm', 't', ' '))
			{
				u32 *FormatLength = (u32*)(Data + 16);
				u16 *Format = (u16*)(Data + 20);
				u16 *Channels = (u16*)(Data + 22);
				if (*Channels != Engine->SoundSystem->Channels)
				{
					Engine->Platform->Log("Sound file has incorrect channels info %d\n", *Channels);
					return 0;
				}
				u32 *SampleRate = (u32*)(Data + 24);
				if (*SampleRate != Engine->SoundSystem->SampleRate)
				{
					Engine->Platform->Log("Sound file has incorrect samplerate info %d\n", *SampleRate);
					return 0;
				}

				u32 *AvgSampleRate = (u32*)(Data + 28);
				u16 *BlockAlign = (u16*)(Data + 32);
				u16 *BitsPerSamle = (u16*)(Data + 34);
				u8 *DataChunk = Data + 36;
				while (!CheckCC(DataChunk, 'd', 'a', 't', 'a'))
				{
					DataSize = (u32*)(DataChunk+4);
					DataChunk += *DataSize + 8; //+8: 4 of the header + 4 of the chunksize
				}

				Asset->SoundAsset.DataSize = *(u32*)(DataChunk+4);
				Asset->SoundAsset.Data = DataChunk+8;
				Asset->SoundAsset.Gain = 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	return 1;
}

static void asset_clean_asset(engine *Engine, asset *Asset)
{
	Asset->Sid = 0;
	memory_free(&Engine->Memory, Asset->PathName);
}

void asset_load_sound(struct engine *Engine, const char *Name)
{
	asset *Asset = 0;
	u8 *File = prepare_asset_and_get_file(Engine, Name, &Asset);
	if (File == 0)
		return;

	if (asset_initialize_sound(Engine, Asset, File) == 0)
	{
		Engine->Platform->Log("Error loading sound file %s\n", Name);
		memory_free(&Engine->Memory, File);
		asset_clean_asset(Engine, Asset);
	}
}

void asset_load_sound_streamed(struct engine *Engine, const char *Name)
{
	asset *Asset = prepare_asset(Engine, Name);

	char *FullPath = asset_get_full_path(Engine, Name);
	file_handle FileH = Engine->Platform->FileOpen(FullPath);
	u8 Buffer[256];
	u32 Read = Engine->Platform->FileRead(FileH, Buffer, sizeof(Buffer), 0);
	if (Read != sizeof(Buffer))
	{
		Engine->Platform->FileClose(FileH);
		asset_clean_asset(Engine, Asset);
		return;
	}

	if (asset_initialize_sound(Engine, Asset, Buffer) == 0)
	{
		Engine->Platform->Log("Error loading sound file %s\n", Name);
		Engine->Platform->FileClose(FileH);
		asset_clean_asset(Engine, Asset);
		return;
	}

	u32 BufferSize = 60000;
	Asset->SoundAsset.StreamBuffer = memory_alloc(&Engine->Memory, BufferSize);
	u32 Offset = (u32)(Asset->SoundAsset.Data - Buffer);
	Asset->SoundAsset.StreamBeginFileOffset = Offset;
	Asset->SoundAsset.StreamFileOffset = Offset;
	Asset->SoundAsset.DataSize = BufferSize/2;
	Asset->SoundAsset.StreamFromDisk = 1;
	Asset->SoundAsset.StreamFile = FileH;
	Asset->SoundAsset.StreamFileOffset += Engine->Platform->FileRead(FileH, Asset->SoundAsset.StreamBuffer, BufferSize/2, Offset);
	Asset->SoundAsset.Data = Asset->SoundAsset.StreamBuffer;
	Asset->SoundAsset.StreamRequest = 1;
}

void asset_load_texture(struct engine *Engine, const char *Name)
{
	asset *Asset = 0;
	u8 *File = prepare_asset_and_get_file(Engine, Name, &Asset);

	u32 PixelSize = 16;
#ifdef PLAT_ANDROID
	if (CheckCC(File, 'P', 'K', 'M', ' '))
	{
	    u16 Type = *(u16*)(File+6);
		if (Type == 256) //ETC_RGB8 No alpha channel
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGB8_ETC2;
			PixelSize = 8;
		}
		else //768
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGBA8_ETC2_EAC;
		}

		Asset->TextureAsset.Height = *(u16*)(File + 9);
		Asset->TextureAsset.Width = *(u16*)(File + 11);
		Asset->TextureAsset.Data = File + 16;
	}
#else

	//DDS Format
	if (CheckCC(File, 'D', 'D', 'S', ' '))
	{
		Asset->TextureAsset.Height = *(u32*)(File + 12);
		Asset->TextureAsset.Width = *(u32*)(File + 16);
		u8 *FourCC = (u8*)(File + 84);
		if (CheckCC(FourCC, 'D', 'X', 'T', '1'))
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
			PixelSize = 8;
		}
		else if (CheckCC(FourCC, 'D', 'X', 'T', '5'))
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		}
		Asset->TextureAsset.Data = File + 128;
	}
#endif

	Asset->Type = AssetType_Texture;
	render_create_texture(Engine, Asset, PixelSize);
	//Done with the file I can delete it from memory
	memory_free(&Engine->Memory, File);

#if DEBUG
	asset_timestamp AT;
	char *FullPath = string_new(Engine, "%s/%s", Engine->AssetDatabase.RootPath, Asset->PathName);
	AT.Time = Engine->Platform->GetFileModifiedDate(FullPath);
	memory_free(&Engine->Memory, FullPath);
	AT.Asset = Asset;
	dynamic_array_add(&Engine->AssetDatabase.Timestamps, AT);
#endif
}

void asset_init(engine *Engine)
{
#if DEBUG
	Engine->AssetDatabase.Timestamps = dynamic_array_create(Engine, asset_timestamp);
#endif
}

void asset_reload(struct engine *Engine)
{
#if DEBUG
	for (u32 i=0; i<dynamic_array_len(Engine->AssetDatabase.Timestamps); ++i)
	{
		asset_timestamp *AT = Engine->AssetDatabase.Timestamps + i;
		char *FullPath = string_new(Engine, "%s/%s", Engine->AssetDatabase.RootPath, AT->Asset->PathName);
		if (AT->Time < Engine->Platform->GetFileModifiedDate(FullPath))
		{
			if (AT->Asset->Type == AssetType_Texture)
			{
				render_reload_asset(Engine, AT->Asset);
				dynamic_array_remove_at(Engine->AssetDatabase.Timestamps, i--);
			}
		}
		memory_free(&Engine->Memory, FullPath);
	}
#endif
}

void asset_update(struct engine *Engine)
{
	asset_reload(Engine);
}

void asset_get_all_of_type(struct engine *Engine, asset_type Type, asset_query_callback Callback)
{
	for (u32 i=0; i<static_array_len(Engine->AssetDatabase.Assets); ++i)
	{
		asset *Asset = Engine->AssetDatabase.Assets + i;
		if (Asset->Sid == 0)
			continue;

		if (Asset->Type == Type)
			Callback(Engine, Asset);
	}
}
