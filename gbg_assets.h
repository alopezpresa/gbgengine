#ifndef GBG_ASSETS_H
#define GBG_ASSETS_H

#include "gbg_types.h"
#include "gbg_sound.h"

struct texture_asset
{
	u32 TextureId;
	u32 Height;
	u32 Width;
	u32 Format;
	u8 *Data;
};

GENUM()
typedef enum asset_type
{
	AssetType_Sound,
	AssetType_Texture,

	AssetType_Max
} asset_type;

//TODO: add sound hot reloading
typedef struct asset
{
	u32 Sid;
	char *PathName;
	asset_type Type;
	union 
	{
		sound_asset SoundAsset;	
		struct texture_asset TextureAsset;
	};

	struct asset *Next;
} asset;

GOBJECT()
typedef struct texture_ref
{
	char *PathName;
	asset *Asset;
} texture_ref;

#if DEBUG
typedef struct asset_timestamp
{
	u64 Time;
	asset *Asset;
} asset_timestamp;
#endif

typedef struct asset_database
{
	asset Assets[64];
	char *RootPath;

#if DEBUG
	asset_timestamp *Timestamps;
#endif
} asset_database;

struct engine;

//Attempts to load the sound specified with Name. It will return early if sound is already loaded
void asset_load_sound(struct engine *Engine, const char *Name);

void asset_load_sound_streamed(struct engine *Engine, const char *Name);

//Return valid asset or null if not found. 
asset *asset_get(struct engine *Engine, const char *Name);

u8 *asset_load_file(struct engine *Engine, const char *Name, u32 *Size);

void asset_load_texture(struct engine *Engine, const char *Name);

void asset_init(struct engine *Engine);
void asset_update(struct engine *Engine);

typedef void (*asset_query_callback)(struct engine *Engine, asset *Asset);

void asset_get_all_of_type(struct engine *Engine, asset_type Type, asset_query_callback Callback);

#endif
