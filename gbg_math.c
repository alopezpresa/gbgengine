#include "gbg_math.h"
#include <limits.h>
#include <float.h>
#include <math.h>

f32 clamp(f32 Val, f32 Min, f32 Max)
{
	return fmaxf(fminf(Val, Max), Min);
}

rect Rect(vec2 Pos, vec2 Rad)
{
	rect R = {Pos, Rad};
	return R;
}

b32 collision_test_circle_point(circle Circle, vec2 Pos)
{
	f32 Dist = vec2_distance(Circle.Pos, Pos);
	return Dist <= Circle.Rad;
}

b32 collision_test_rect_point(rect Rect, vec2 Pos)
{
	if (Pos.x < Rect.Pos.x - Rect.Rad.x)
		return 0;
	if (Pos.x > Rect.Pos.x + Rect.Rad.x)
		return 0;
	if (Pos.y < Rect.Pos.y - Rect.Rad.y)
		return 0;
	if (Pos.y > Rect.Pos.y + Rect.Rad.y)
		return 0;

	return 1;
}

b32 collision_ray_rect(vec2 StartPos, vec2 Direction, rect Rect, f32* TMin, vec2* HitP, vec2 *HitN)
{
	Direction = vec2_normalize(Direction);
	*TMin = 0;
	f32 TMax = FLT_MAX;

	vec2 NormalHit = {0};

	for (u32 i=0; i<2; ++i)
	{
		f32 AxisMin = Rect.Pos.E[i] - Rect.Rad.E[i];
		f32 AxisMax = Rect.Pos.E[i] + Rect.Rad.E[i];

		if (fabs(Direction.E[i]) < GBG_EPSILON)
		{
			if (StartPos.E[i] < AxisMin || StartPos.E[i] > AxisMax)
				return 0;
		}
		else
		{
			f32 InvD = 1.0f / Direction.E[i];
			f32 T1 = (AxisMin - StartPos.E[i]) * InvD;
			f32 T2 = (AxisMax - StartPos.E[i]) * InvD;

			if (T1 > T2)
			{
				f32 Aux = T1;
				T1 = T2;
				T2 = Aux;
			}

			if (T1 > *TMin)
			{
				*TMin = fmaxf(*TMin, T1);
				//TODO: What happens if the two axis collide at the same time?
				NormalHit = Vec2(0, 0);
				NormalHit.E[i] = Direction.E[i] > 0 ? -1.f : 1.f;
			}
			TMax = fminf(TMax, T2);

			if (*TMin > TMax)
				return 0;
		}
	}

	*HitP = vec2_add(StartPos, vec2_mul(Direction, *TMin));
	*HitN = NormalHit;

	return 1;
}

random_state Random(u64 Seed)
{
	random_state Ret;
	Ret.state0 = Seed;
	Ret.state1 = Seed*2;

	return Ret;
}

u64 random_next(random_state *Random)
{
	u64 s1 = Random->state0;
	u64 s0 = Random->state1;
	Random->state0 = s0;
	s1 ^= s1 << 23;
	s1 ^= s1 >> 17;
	s1 ^= s0;
	s1 ^= s0 >> 26;
	Random->state1 = s1;
	return Random->state0 + Random->state1;
}

u32 random_range(random_state *Random, u32 From, u32 To)
{
	u64 Rnd = random_next(Random);
	return From + (Rnd % (To - From));
}

f32 random_01(random_state *Random)
{
	u64 Rnd = random_next(Random);
	return (f32)(Rnd/(f64)ULLONG_MAX);
}

vec2 random_vec2(random_state *Random)
{
	f32 x = (random_01(Random) * 2) -1.0f;
	f32 y = (random_01(Random) * 2) -1.0f;

	return vec2_normalize(Vec2(x, y));
}

vec2 Vec2(f32 X, f32 Y)
{
	vec2 Ret = {X, Y};
	return Ret;
}

vec2 vec2_add(vec2 A, vec2 B)
{
	vec2 Ret = Vec2(A.x+B.x, A.y+B.y);
	return Ret;
}

vec2 vec2_sub(vec2 A, vec2 B)
{
	vec2 Ret = Vec2(A.x-B.x, A.y-B.y);
	return Ret;
}

vec2 vec2_mul(vec2 V, f32 M)
{
	vec2 Ret = Vec2(V.x*M, V.y*M);
	return Ret;
}

vec2 vec2_ewise_mul(vec2 A, vec2 B)
{
	vec2 Ret = Vec2(A.x*B.x, A.y*B.y);
	return Ret;
}

vec2 vec2_div(vec2 V, f32 M)
{
	vec2 Ret = Vec2(V.x/M, V.y/M);
	return Ret;
}

f32 vec2_length(vec2 V)
{
	return sqrtf(V.x*V.x + V.y*V.y);
}

f32 vec2_length_squared(vec2 V)
{
	return V.x*V.x + V.y*V.y;
}

f32 vec2_distance(vec2 A, vec2 B)
{
	vec2 C = Vec2(A.x-B.x, A.y-B.y);
	return vec2_length(C);
}

f32 vec2_distance_squared(vec2 A, vec2 B)
{
	vec2 C = Vec2(A.x-B.x, A.y-B.y);
	return vec2_length_squared(C);
}

vec2 vec2_normalize(vec2 V)
{
	vec2 Ret = {0};
	f32 Length = vec2_length(V);

	Ret.x = V.x/Length;
	Ret.y = V.y/Length;

	return Ret;
}

vec3 Vec3(f32 X, f32 Y, f32 Z)
{
	vec3 Ret = {X, Y, Z};
	return Ret;
}

vec3 vec2_to3(vec2 V2, f32 z)
{
	vec3 Ret = {V2.x, V2.y, z};
	return Ret;
}

vec3 vec3_add(vec3 A, vec3 B)
{
	vec3 Ret = {A.x + B.x, A.y + B.y, A.z + B.z};
	return Ret;
}

vec2 vec3_to2(vec3 V3)
{
	vec2 Ret = {V3.x, V3.y};
	return Ret;
}

vec4 Vec4(f32 X, f32 Y, f32 Z, f32 A)
{
	vec4 Ret = {X, Y, Z, A};
	return Ret;
}

//(1-t)*A + T*B;
vec4 vec4_lerp(vec4 From, vec4 To, f32 T)
{
	f32 T1 = 1-T;
	//TODO: Use smmd
	vec4 A = Vec4(T1*From.x, T1*From.y, T1*From.z, T1*From.a);
	vec4 B = Vec4(T*To.x, T*To.y, T*To.z, T*To.a);

	return Vec4(A.x + B.x, A.y + B.y, A.z + B.z, A.w + B.w);
}

vec2 vec2_lerp(vec2 From, vec2 To, f32 T)
{
	f32 T1 = 1-T;
	//TODO: Use smmd
	vec2 A = vec2_mul(From, T1);
	vec2 B = vec2_mul(To, T);

	return vec2_add(A, B);
}

f32 lerp(f32 From, f32 To, f32 T)
{
	return ((1-T)*From) + (T*To);
}

vec4 mat4_mul_vec4(f32 *Mat, vec4 Pos)
{
	//TODO: use simmd
	vec4 Ret;
	Ret.x = Pos.x*Mat[0] + Pos.y*Mat[4] + Pos.z*Mat[8]  + Pos.w*Mat[12];
	Ret.y = Pos.x*Mat[1] + Pos.y*Mat[5] + Pos.z*Mat[9]  + Pos.w*Mat[13];
	Ret.z = Pos.x*Mat[2] + Pos.y*Mat[6] + Pos.z*Mat[10] + Pos.w*Mat[14];
	Ret.w = Pos.x*Mat[3] + Pos.y*Mat[7] + Pos.z*Mat[11] + Pos.w*Mat[15];

	return Ret;
}
