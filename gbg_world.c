#include "gbg_world.h"
#include "gbg_rtti.h"

rigid_body RigidBody_Circle(vec2 Pos, f32 Rad)
{
	rigid_body Ret = {.Type = RB_Circle, .Circle = {Pos, Rad}};
	return Ret;
}

rigid_body RigidBody_Rect(vec2 Pos, vec2 Rad)
{
	rigid_body Ret = {.Type = RB_Rect, .Rect = {Pos, Rad}};
	return Ret;
}

static entity *entity_make_valid(world *World, entity *Ent)
{
	entity New = {.Scale={1, 1}, .IsValid=1};

	*Ent = New;

	return Ent;
}

void *_entity_get_game_data(entity *Ent, const char *TypeName)
{
	if (Ent->GameData.Ptr)
	{
		if (string_compare(Ent->GameData.RecordName, TypeName))
			return Ent->GameData.Ptr;
	}

	return 0;
}

b32 collision_vs(collision_info *ColInfo, u32 UserTag1, u32 UserTag2, entity **Ent1, entity **Ent2)
{
	b32 IsType1 = ColInfo->EntA->RigidBody.UserTag == UserTag1;
	b32 IsType2 = 0;
	if (IsType1)
	{
		*Ent1 = ColInfo->EntA;
		IsType2 = ColInfo->EntB->RigidBody.UserTag == UserTag2;
		if (IsType2)
		{
			*Ent2 = ColInfo->EntB;
			return 1;
		}
	}
	else
	{
		IsType1 = ColInfo->EntB->RigidBody.UserTag == UserTag1;
		if (IsType1)
		{
			*Ent1 = ColInfo->EntB;
			IsType2 = ColInfo->EntA->RigidBody.UserTag == UserTag2;
			if (IsType2)
			{
				*Ent2 = ColInfo->EntA;
				return 1;
			}
		}
	}

	return 0;
}

entity *_world_entity_create_in(engine *Engine, entity_cluster *EntityCluster)
{
	world *World = Engine->World;
	entity_cluster *Cluster = EntityCluster;

	for (;;)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (!Ent->IsValid)
			{
				return entity_make_valid(World, Ent);
			}
		}	

		if (Cluster->Next)
		{
			Cluster = Cluster->Next;
		}
		else
		{
			entity_cluster *NewCluster = memory_alloc_type(&Engine->Memory, entity_cluster);
			memset(NewCluster, 0, sizeof(entity_cluster));
			Cluster->Next = NewCluster;
			Cluster = NewCluster;
		}
	}

	assert(0);
	return 0;
}

entity *_world_entity_create_from_imprint_using(engine *Engine, imprint *Imprint, entity_cluster *EntityCluster)
{
	entity *Ret = _world_entity_create_in(Engine, EntityCluster);
	rtti_record_copy(Engine, Engine->Rtti->CachedEntityRecord, Imprint->Entity, Ret);
	return Ret;
}

entity *world_entity_create_from_imprint(engine *Engine, imprint *Imprint)
{
	return _world_entity_create_from_imprint_using(Engine, Imprint, &Engine->World->Entities);
}

entity *world_entity_create_from_imprint_in_editor(struct engine *Engine, struct imprint *Imprint)
{
	return _world_entity_create_from_imprint_using(Engine, Imprint, &Engine->World->EntitiesFromEditor);
}

entity *world_entity_create(struct engine *Engine)
{
	return _world_entity_create_in(Engine, &Engine->World->Entities);
}

entity *world_entity_create_in_editor(struct engine *Engine)
{
	return _world_entity_create_in(Engine, &Engine->World->EntitiesFromEditor);
}

void entity_destroy(struct engine *Engine, entity *Entity)
{
	Entity->IsValid = 0;
	//TODO: Think on how to notify the user that the entity is being deleted. Maybe more cleanup is needed. Mayne a deeper clean up with reflection, gamedata inside gamedata
	//Maybe it's not needed at all
	memory_free(&Engine->Memory, Entity->GameData.Ptr);
	Entity->GameData.Ptr = 0;
	if (Entity->GameData.RecordName)
	{
		string_delete(Engine, Entity->GameData.RecordName);
	}
}

void entity_render(struct engine *Engine, entity *Ent)
{
	render_set_default_color(Engine, Ent->Material.Color);
	if (Ent->Material.Texture.PathName)
	{
		if (Ent->Material.Texture.Asset)
		{
			sprite Sprite = {Ent->Material.Texture.PathName, Ent->Material.TexturePos, Ent->Material.TextureSize, Ent->Material.Color};
			vec2 *RadSize = &Ent->RigidBody.Rect.Rad;
			vec2 ScaledSize = Vec2(Ent->Scale.x * RadSize->x, Ent->Scale.y * RadSize->y);
			//Fix this, sprites should have a size independent from the rigid body
			if (Ent->RigidBody.Type == RB_Circle)
				ScaledSize.y = Ent->Scale.y * RadSize->x;
			render_draw_sprite(Engine, Sprite, Ent->Pos, vec2_mul(ScaledSize, 2.0f), Ent->Rot, Ent->Material.BlendMode);
		}
		else
		{
			Ent->Material.Texture.Asset = asset_get(Engine, Ent->Material.Texture.PathName);
		}
	}
	else
	{
		if (Ent->RigidBody.Type == RB_Circle)
		{
			//TODO: Use scale
			render_draw_circle(Engine, Ent->Pos, Ent->RigidBody.Circle.Rad);
		}
		else
		{
			vec2 *RadSize = &Ent->RigidBody.Rect.Rad;
			vec2 ScaledSize = Vec2(Ent->Scale.x * RadSize->x, Ent->Scale.y * RadSize->y);
			render_draw_rect(Engine, Ent->Pos, vec2_mul(ScaledSize, 2));
		}
	}
}

void world_init(engine *Engine)
{
	Engine->World = memory_alloc_type(&Engine->Memory, world);
	Engine->World->FrameCollisions = dynamic_array_create(Engine, collision_info);
	Engine->World->CollisionFilters = dynamic_array_create(Engine, collision_filter);
	Engine->World->Extent = Rect(Vec2(500, 250), Vec2(600, 300));
}

static void world_clear_level(engine *Engine, entity_cluster *Cluster)
{
	entity_cluster *CurCluster = Cluster;
	while(CurCluster)
	{
		for (u32 i=0; i<static_array_len(CurCluster->E); ++i)
		{
			entity *EditorEnt = CurCluster->E + i;
			if (EditorEnt->IsValid)
			{
				entity_destroy(Engine, EditorEnt);
			}
		}

		CurCluster = CurCluster->Next;
	}

	//Delete clusters
	CurCluster = Cluster->Next;
	while (CurCluster)
	{
		if (CurCluster->Next)
		{
			Cluster->Next = CurCluster->Next;
			memory_free(&Engine->Memory, CurCluster);
		}
		else
		{
			memory_free(&Engine->Memory, CurCluster);
			Cluster->Next = 0;
		}
		CurCluster = Cluster->Next;
	}
}

//TODO: Cap DeltaTime to use a fixed time step
void world_update(struct engine *Engine, f32 DeltaTime)
{
	world *World = Engine->World;
	f32 MaxDT = clamp(DeltaTime, 0.0f, 0.16667f);
	dynamic_array_clear(World->FrameCollisions);

	//This order is important. 
	//1. Current transition to state
	if (World->SetTransition == WorldTransition_Play)
	{
		entity_cluster *Cluster = &World->EntitiesFromEditor;
		while(Cluster)
		{
			for (u32 i=0; i<static_array_len(Cluster->E); ++i)
			{
				entity *EditorEnt = Cluster->E + i;
				if (EditorEnt->IsValid)
				{
					entity *NewEnt = world_entity_create(Engine);
					rtti_record_copy(Engine, Engine->Rtti->CachedEntityRecord, EditorEnt, NewEnt);
				}
			}

			Cluster = Cluster->Next;
		}
	}
	else if (World->SetTransition == WorldTransition_Stop)
	{
		world_clear_level(Engine, &World->Entities);
	}

	if (World->CurrentTransition == WorldTransition_Play)
	{
		World->CurrentState = WorldState_Play;
	}
	else if (World->CurrentTransition == WorldTransition_Stop)
	{
		World->CurrentState = WorldState_Stop;
	}

	//2. Clear transition
	if (World->CurrentTransition != WorldTransition_None)
	{
		World->CurrentTransition = WorldTransition_None;
	}

	//3. Set current transition
	World->CurrentTransition = World->SetTransition;

	//4. Clear set transition
	if (World->SetTransition != WorldTransition_None)
	{
		World->SetTransition = WorldTransition_None;
	}

	entity_cluster *Cluster = 0;

	if (World->CurrentState == WorldState_Stop)
	{
		Cluster = &World->EntitiesFromEditor;
		while(Cluster)
		{
			for (u32 i=0; i<static_array_len(Cluster->E); ++i)
			{
				//Only render editor, it should be in its own state
				entity *EdEnt = Cluster->E + i;
				if (EdEnt->IsValid)
				{
					entity_render(Engine, EdEnt);
				}
			}

			Cluster = Cluster->Next;
		}	
	}

	Cluster = &World->Entities;
	while(Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				if (!collision_test_rect_point(World->Extent, vec3_to2(Ent->Pos)))
				{
					entity_destroy(Engine, Ent);
					continue;
				}
				entity_render(Engine, Ent);

				//physics
				{
					if (Ent->Vel.x != 0 || Ent->Vel.y != 0)
					{
						vec2 MoveDir = vec2_mul(Ent->Vel, MaxDT);
						f32 MoveBudget = vec2_length(MoveDir);
						f32 TotalMove = MoveBudget;
						while (MoveBudget > 0)
						{
							collision_info FirstHit = {.Distance = FLT_MAX};
							entity_cluster *HitCluster = &World->Entities;
							while (HitCluster)
							{
								for (u32 e=0; e<static_array_len(HitCluster->E); ++e)
								{
									entity *HitEnt = HitCluster->E + e;

									b32 IgnoreCollision = 0;
									//Check filters
									for (u32 FilterIx=0; FilterIx<dynamic_array_len(World->CollisionFilters); ++FilterIx)
									{
										collision_filter *ColFilter = World->CollisionFilters + FilterIx;
										if ((ColFilter->Tag1 == Ent->RigidBody.UserTag && ColFilter->Tag2 == HitEnt->RigidBody.UserTag) || (ColFilter->Tag2 == Ent->RigidBody.UserTag && ColFilter->Tag1 == HitEnt->RigidBody.UserTag))
										{
											if (ColFilter->Type == CFT_Ignore)
											{
												IgnoreCollision = 1;
												break;
											}
										}
									}

									if (IgnoreCollision)
										continue;

									if (HitEnt->IsValid && HitEnt != Ent)
									{
										if (Ent->RigidBody.Type == RB_Circle)
										{
											if (HitEnt->RigidBody.Type == RB_Rect)
											{
												rect TestRect = HitEnt->RigidBody.Rect;
												TestRect.Pos = vec2_add(TestRect.Pos, vec3_to2(HitEnt->Pos));
												TestRect.Rad = vec2_add(TestRect.Rad, Vec2(Ent->RigidBody.Circle.Rad, Ent->RigidBody.Circle.Rad));
												f32 TMin;
												vec2 HitP, HitN;
												if (collision_ray_rect(vec3_to2(Ent->Pos), MoveDir, TestRect, &TMin, &HitP, &HitN))
												{
													if (MoveBudget >= TMin)
													{
														collision_info ColInfo = {.EntA = Ent, .EntB = HitEnt, 
															.Distance = TMin, .NormalizedDistance = TMin/TotalMove,
															.P = HitP, .N = HitN, .IsInside = TMin < GBG_EPSILON ? 1: 0	
														};

														if (!HitEnt->IsSensor && FirstHit.Distance > TMin)
														{
															FirstHit = ColInfo;
														}
														else if (HitEnt->IsSensor)
														{
															dynamic_array_add(&World->FrameCollisions, ColInfo);
														}
													}
												}
											}
										}
										else if (Ent->RigidBody.Type == RB_Rect)
										{
											if (HitEnt->RigidBody.Type == RB_Rect)
											{
												rect TestRect = HitEnt->RigidBody.Rect;
												TestRect.Pos = vec2_add(TestRect.Pos, vec3_to2(HitEnt->Pos));
												TestRect.Rad = vec2_add(TestRect.Rad, Ent->RigidBody.Rect.Rad);
												f32 TMin;
												vec2 HitP, HitN;
												if (collision_ray_rect(vec3_to2(Ent->Pos), MoveDir, TestRect, &TMin, &HitP, &HitN))
												{
													if (MoveBudget >= TMin)
													{
														collision_info ColInfo = {.EntA = Ent, .EntB = HitEnt, 
															.Distance = TMin, .NormalizedDistance = TMin/TotalMove,
															.P = HitP, .N = HitN, .IsInside = TMin < GBG_EPSILON ? 1: 0	
														};

														if (!HitEnt->IsSensor && FirstHit.Distance > TMin)
														{
															FirstHit = ColInfo;
														}
														else if (HitEnt->IsSensor)
														{
															dynamic_array_add(&World->FrameCollisions, ColInfo);
														}
													}
												}
											}
										}
									}
								}

								HitCluster = HitCluster->Next;
							}

							if (FirstHit.EntB != 0)
							{
								for (u32 a=0; a<2; ++a)
								{
									if (FirstHit.N.E[a] != 0)
									{
										Ent->Vel.E[a] = Ent->Vel.E[a] * -1;
									}
								}

								Ent->Pos.x = FirstHit.P.x;
								Ent->Pos.y = FirstHit.P.y;

								//Trying to separate object so they don't penetrate among them
								Ent->Pos = vec3_add(Ent->Pos, vec2_to3(FirstHit.N, 0));
								MoveBudget -= FirstHit.Distance;
								//Velocity should have changed with collision response
								MoveDir = vec2_mul(Ent->Vel, MaxDT*(MoveBudget/TotalMove));

								dynamic_array_add(&World->FrameCollisions, FirstHit);
							}
							else
							{
								MoveBudget = 0;
								Ent->Pos = vec3_add(Ent->Pos, vec2_to3(MoveDir, 0));
							}
						}
					}
				}
			}
		}

		Cluster = Cluster->Next;
	}
}

void world_clear_level_editor(engine *Engine)
{
	world_clear_level(Engine, &Engine->World->EntitiesFromEditor);
}

//TODO: Change this, i don't like a function call inside an engine loop. The user can stall everything
void world_query_entities(engine *Engine, u32 RigidBodyUserTag, query_entity_fn Func)
{
	entity_cluster *Cluster = &Engine->World->Entities;
	while(Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				if (Ent->RigidBody.UserTag == RigidBodyUserTag)
				{
					Func(Engine, Ent);
				}
			}
		}
		Cluster = Cluster->Next;
	}
}
