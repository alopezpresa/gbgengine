#ifndef GBG_NET_H
#define GBG_NET_H

typedef enum net_message_type
{
	NetMessageType_ConnectionRequest,
	NetMessageType_ConnectionResponse,
	NetMessageType_Sync,
	NetMessageType_Action,
	NetMessageType_KeepAlive,
}net_message_type;

typedef struct net_message_connection_response
{
	b32 Response;
} net_message_connection_response;

typedef struct net_message_sync
{
	u8 *Data;
	u32 DataLen;
} net_message_sync;

//Data types
//String:
//Size: short
//Data
//
//Protocol
//
//Header
//Message
//
//HEADER
//Size short
//Message Type(net_message_type): byte . 
//
//CONNECT REQUEST MESSAGE
//Name : String
//
//Type:	NetMessateType_ConnectionResponse,
//DATA
//byte: 0-denied, 1-accepted

typedef enum net_state
{
	NetState_WaitingForPlayers,
	NetState_Playing,
	NetState_ConnectionLost,
} net_state;

typedef struct net_player
{
	char *Name;
	char *Address;
	char *Port;
	f64 LastTimeSeen;
	b32 Connected;
} net_player;

typedef struct net_message
{
	net_message_type Type;
	union
	{
		net_message_connection_response ConnectionResponse;
		//TODO: Is there any difference between these two??
		net_message_sync Sync;
		net_message_sync Action;
	};
} net_message;

typedef struct net_message_ring
{
	net_message Messages[64];
	u16 ReadHeader;
	u16 WriteHeader;
	b32 Lock;
}net_message_ring;

b32 network_message_ring_read(engine *Engine, net_message *Message);

//Sync callbacks, they will be called when time is right
typedef void (*network_server_sync_func)(engine *Engine, u8 **SyncMessage);

typedef struct net
{
	net_state State;
	file_handle Socket;
	net_player *Players;
	net_player Host;
	net_message_ring MessageRing;
	b32 IsHost;
	u32 Frequency;
	f32 SyncTimer;

	f64 LastMessageSentTime;

	network_server_sync_func ServerSyncFunc;
}net;

b32 network_start_server(engine *Engine, const char *PlayerName, const char *Host, const char *Port, network_server_sync_func ServerSyncFunc);
void network_stop_server(engine *Engine);
b32 network_join_server(engine *Engine, const char *PlayerName, const char *Host, const char *Port);

u32 network_message_send(engine *Engine, u8 *Message, net_player *Player);

u8 *net_message_begin(engine *Engine, net_message_type Type);
void net_message_add8(engine *Engine, u8 **Message, u8 Value);
void net_message_add16(engine *Engine, u8 **Message, u16 Value);
void net_message_add32(engine *Engine, u8 **Message, u32 Value);
void net_message_add_string(engine *Engine, u8 **Message, const char *Str);

typedef struct byte_stream_reader
{
	const u8 *Data;
	u32 Header;
	u32 Len;
} byte_stream_reader;

byte_stream_reader ByteStreamReader(const u8 *Data, u32 Len);
u8 byte_stream_read8(engine *Engine, byte_stream_reader *Reader);
u16 byte_stream_read16(engine *Engine, byte_stream_reader *Reader);
u32 byte_stream_read32(engine *Engine, byte_stream_reader *Reader);
//There's no try string version, this function will return null on end of stream
char *byte_stream_read_string(engine *Engine, byte_stream_reader *Reader);

b32 byte_stream_try_read8(engine *Engine, byte_stream_reader *Reader, u8 *OutValue);
b32 byte_stream_try_read16(engine *Engine, byte_stream_reader *Reader, u16 *OutValue);
b32 byte_stream_try_read32(engine *Engine, byte_stream_reader *Reader, u32 *OutValue);

typedef struct byte_stream_writer
{
	u8 *Data;
} byte_stream_writer;

byte_stream_writer ByteStreamWriter(struct engine *Engine);
void byte_stream_destroy(byte_stream_writer *Stream);

void byte_stream_write8(struct engine *Engine, byte_stream_writer *Stream, u8 Value);
void byte_stream_write16(struct engine *Engine, byte_stream_writer *Stream, u16 Value);
void byte_stream_write32(struct engine *Engine, byte_stream_writer *Stream, u32 Value);
void byte_stream_write_string(struct engine *Engine, byte_stream_writer *Stream, const char *String);

void network_update(engine *Engine, f32 DeltaTime);

#endif
