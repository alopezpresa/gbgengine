GbGEngine handles gpu ready compressed textures like DXT and ETC.
That requires that png or jpeg assets made by artists to be processed by a tool and the output to be placed in the right folder.

Following is a step by step process per platform.

All tools are inside the `tools` folder

#Android#
##Common issues##
Use Power of two textures

##Asset anatomy##
Android will copy all assets inside the folder `android\GbGEngine\app\src\main\assets\` so they can be open later by the app.
Developers should copy/transform assets into those folders.
There's no easy way to automatize this step, maybe in a future GbGEngine will enforce an asset structure that will automatically be exported into the android folder

Android handles ETC1, ETC2 and ASTC textures. GbGEngine uses ETC2 for most platforms because it targets OpenglEs 3.0 and ASTC it's not guarantied in that version. Also ETC1 has a poor alpha handling.

1. Inside `MaLi` folder you'll find _etcpack.exe_ file. That file will let you export any graphic format to ETC2 file ready to be consumed by GbGEngine.
2. _etcpack_ <in file> <out directory> 

Android looks for the textures inside `android\GbGEngine\app\src\main\assets\textures`
