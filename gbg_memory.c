#include "gbg_memory.h"
#include "gbg_types.h"

#include <math.h>

#define BlockSize 256

struct memory_header
{
	u32 Next;
	b32 Busy;
};

memory_partition memory_init(u8 *Ptr, u32 Size, PLATFORM_MUTEX MutexFunc)
{
	memory_partition Part = {0};
	Part.MaxSize = Size;
	Part.Data = Ptr;
	Part.Mutex = MutexFunc;

	u32 BlockCount = Size/BlockSize;
	struct memory_header *Header = (struct memory_header*)Ptr;
	Header->Next = BlockCount;
	Header->Busy = 0;

	return Part;
}

#if DEBUG_MEMORY
void mem_dump(memory_partition *Mem)
{
	Mem->DebugLog("MEMORY DEBUG: === dump ===\n");
	struct memory_header *Header = (struct memory_header*)Mem->Data;
	u32 HeadCount = 0;
	u32 BlockCount = 0;
	while (1)
	{
		Mem->DebugLog("\t [%d] (%p) Header: Busy %d. Next %d\n", ++HeadCount, (Header+1), Header->Busy, Header->Next);
		if (Header->Next + BlockCount >= Mem->MaxSize/BlockSize)
			break;	

		BlockCount += Header->Next;
		Header = (struct memory_header*)((u8*)Header + Header->Next * BlockSize);
	}
}
#endif

void *memory_alloc(memory_partition *Mem, u32 Size)
{
	struct memory_header *Header = (struct memory_header*)Mem->Data;
	u32 SizeInBlocks = (u32)ceilf(((f32)(Size + sizeof(struct memory_header)))/BlockSize);	
	u32 CurBlockCount = 0;
	u32 MaxBlocks = Mem->MaxSize/BlockSize;

	Mem->Mutex(&Mem->Lock);	

#if DEBUG_MEMORY
	{
	Mem->DebugLog("MEMORY DEBUG: trying to alloc: %d bytes. %d blocks. Memory blocks total: %d\n", Size, SizeInBlocks, MaxBlocks);

	mem_dump(Mem);
	}
#endif

	while (1)
	{
		if (!Header->Busy && Header->Next >= SizeInBlocks)
		{
			Header->Busy = 1;

			//Only shrink Next header if Size is less than free blocks
			if (Header->Next > SizeInBlocks)
			{
				struct memory_header *Next = (struct memory_header*)((u8*)Header + (SizeInBlocks*BlockSize));
				Next->Next = Header->Next - SizeInBlocks;
			}

			Header->Next = SizeInBlocks;
			
#if DEBUG_MEMORY
	{
	Mem->DebugLog("MEMORY DEBUG: Alloc result\n");
	mem_dump(Mem);
	}
#endif
			Mem->Lock = 0;
			return (u8*)Header + sizeof(struct memory_header);
		}

		CurBlockCount += Header->Next;
		if (CurBlockCount >= MaxBlocks)
		{
			Mem->Lock = 0;
			return 0;
		}

		Header = (struct memory_header*)((u8*)Header + Header->Next * BlockSize);
	}

	Mem->Lock = 0;
	return 0;
}

void *memory_realloc(memory_partition *Mem, void *Ptr, u32 NewSize)
{
#if DEBUG_MEMORY
	{
	Mem->DebugLog("MEMORY REALLOC\n");
	}
#endif

	if (Ptr == 0)
		return memory_alloc(Mem, NewSize);

	struct memory_header *Header = (struct memory_header*)((u8*)Ptr - sizeof(struct memory_header));
	if (NewSize > (BlockSize * Header->Next) - sizeof(struct memory_header))
	{
		//ALLOC
		void *RePtr = memory_alloc(Mem, NewSize);
		//COPY
		u32 SizeToCopy = (Header->Next*BlockSize) - sizeof(struct memory_header);
		for (u32 i=0; i<SizeToCopy; ++i)
		{
			((u8*)RePtr)[i] = ((u8*)Ptr)[i];
		}
		
		memory_free(Mem, Ptr);
		return RePtr;
	}

	return Ptr;
}

void memory_free(memory_partition *Mem, void *Ptr)
{
	if (Ptr == 0)
		return;

	struct memory_header *MemToRemove = (struct memory_header*)((u8*)Ptr - sizeof(struct memory_header));

	Mem->Mutex(&Mem->Lock);

#if DEBUG_MEMORY
	{
	Mem->DebugLog("MEMORY DEBUG: trying to remove: %p . %d blocks. (%d busy?)\n", Ptr, MemToRemove->Next, MemToRemove->Busy);

	mem_dump(Mem);
	}
#endif

	assert(MemToRemove->Next != 0);

	if (!MemToRemove->Busy)
	{
		Mem->Lock = 0;
		return;
	}

	//mark all in between blocks as free
	for (u32 i = 0; i<MemToRemove->Next; ++i)
	{
		struct memory_header *Header = (struct memory_header*)((u8*)MemToRemove + i*BlockSize);
		Header->Busy = 0;
	}

	struct memory_header *Next = (struct memory_header*)((u8*)MemToRemove + (MemToRemove->Next*BlockSize));
	if (!Next->Busy)
	{
		MemToRemove->Next += Next->Next;
	}

#if DEBUG_MEMORY
	{
	Mem->DebugLog("MEMORY DEBUG: Removed result\n");

	mem_dump(Mem);
	}
#endif

	Mem->Lock = 0;
}

#undef BlockSize
