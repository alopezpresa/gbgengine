
#include "gbg_utils.h"
#include "gbg_engine.h"
#include "gbg_memory.h"

//TODO: Think ways for making the array reentrant
struct dyn_array_header
{
	u32 Capacity;
	u32 Size;
	memory_partition *Mem;
};

void *dynamic_array_create_(engine *Engine, u32 ElementSize)
{
	struct dyn_array_header *Header = (struct dyn_array_header*)memory_alloc(&Engine->Memory, sizeof(struct dyn_array_header) + ElementSize);
	Header->Capacity = 1;
	Header->Size = 0;
	Header->Mem = &Engine->Memory;

	return Header + 1;
}

static struct dyn_array_header *dynamic_array_grow_one(void **Array, u32 ElementSize)
{
	struct dyn_array_header *Header = (struct dyn_array_header*)*Array - 1;
	assert(*Array);
	if (Header->Size == Header->Capacity)
	{
		++Header->Capacity;
		Header = (struct dyn_array_header*)memory_realloc(Header->Mem, Header, (Header->Capacity*ElementSize) + sizeof(struct dyn_array_header));
		*Array = Header + 1;
	}

	return Header;
}

void dynamic_array_add_(void **Array, void *Data, u32 ElementSize)
{
	struct dyn_array_header *Header = (struct dyn_array_header*)*Array - 1;

	Header = dynamic_array_grow_one(Array, ElementSize);

	u8 *Dest = ((u8*)*Array) + (ElementSize*Header->Size);
	for (u32 i=0; i<ElementSize; ++i)
	{
		Dest[i] = ((u8*)Data)[i];
	}

	++Header->Size;
}

//TODO: Maybe add is an add_at_ (last) ?
void dynamic_array_add_at_(void **Array, void *Data, u32 ElementSize, u32 Index)
{
	struct dyn_array_header *Header = (struct dyn_array_header*)*Array - 1;

	assert(Index <= Header->Size);

	Header = dynamic_array_grow_one(Array, ElementSize);
	for (s32 i=Header->Size;i>(s32)Index; --i)
	{
		u8 *LastEl = ((u8*)*Array) + (ElementSize*i);
		u8 *PrevEl = ((u8*)*Array) + (ElementSize*(i-1));
		for (u32 b=0; b<ElementSize; ++b)
		{
			LastEl[b] = PrevEl[b];
		}
	}
	u8 *Dest = ((u8*)*Array) + (ElementSize*Index);
	for (u32 i=0; i<ElementSize; ++i)
	{
		Dest[i] = ((u8*)Data)[i];
	}

	++Header->Size;
}

u32 dynamic_array_len(void *Array)
{
	assert(Array);
	struct dyn_array_header *Header = (struct dyn_array_header*)Array - 1;
	return Header->Size;
}

void dynamic_array_clear(void *Array)
{
	assert(Array);
	struct dyn_array_header *Header = (struct dyn_array_header*)Array - 1;
	Header->Size = 0;
}

void dynamic_array_destroy(void *Array)
{
	assert(Array);
	struct dyn_array_header *Header = (struct dyn_array_header*)Array - 1;
	memory_free(Header->Mem, (u8*)Header);
}

void dynamic_array_remove_at_(void *Array, u32 Index, u32 ElementSize)
{
	assert(Array);
	struct dyn_array_header *Header = (struct dyn_array_header*)Array - 1;
	assert(Index < Header->Size);
	--Header->Size;
	for (u32 i=0; i<ElementSize; ++i)
	{
		((u8*)Array)[Index*ElementSize+i] = ((u8*)Array)[Header->Size*ElementSize+i];
	}
}

void dynamic_array_remove_at_preserve_(void *Array, u32 Index, u32 ElementSize)
{
	assert(Array);
	struct dyn_array_header *Header = (struct dyn_array_header*)Array - 1;
	assert(Index < Header->Size);
	--Header->Size;
	for (u32 i=Index; i<Header->Size; ++i)
	{
		for (u32 e=0; e<ElementSize; ++e)
		{
			((u8*)Array)[i*ElementSize+e] = ((u8*)Array)[(i+1)*ElementSize+e];
		}
	}
}

void dynamic_array_reserve_(void **Array, u32 Capacity, u32 ElementSize)
{
	struct dyn_array_header *Header = (struct dyn_array_header*)*Array - 1;
	if (Capacity > Header->Capacity)
	{
		u32 Enlarge = Capacity - Header->Capacity;
		Header->Capacity += Enlarge;
		Header = (struct dyn_array_header*)memory_realloc(Header->Mem, Header, (Header->Capacity*ElementSize) + sizeof(struct dyn_array_header));
		*Array = Header + 1;
	}
}

void dynamic_array_grow_(void **Array, u32 Capacity, u32 ElementSize)
{
	dynamic_array_reserve_(Array, Capacity, ElementSize);
	struct dyn_array_header *Header = (struct dyn_array_header*)*Array - 1;
	if (Header->Size < Capacity)
		Header->Size = Capacity;
}

void mem_copy(void *Source, void *Dest, u32 Len)
{
	for (u32 i=0; i<Len; ++i)
	{
		((u8*)Dest)[i] = ((u8*)Source)[i];
	}
}
