#ifndef GBG_MATH_H
#define GBG_MATH_H

#include "gbg_types.h"

#include <math.h>

f32 clamp(f32 Val, f32 Min, f32 Max);

#define PI 3.141592653589793f
#define PI_HALF (PI/2.0f)
#define PI_2 (PI*2.0f)

#define DEG_TO_PI (PI/180.0f)

#define GBG_EPSILON 0.0001f

typedef struct vec2
{
	union
	{
		struct
		{
			f32 x;
			f32 y;
		};
		f32 E[2];
	};
} vec2;

vec2 Vec2(f32 X, f32 Y);

vec2 vec2_add(vec2 A, vec2 B);
vec2 vec2_sub(vec2 A, vec2 B);
vec2 vec2_mul(vec2 V, f32 M);
vec2 vec2_ewise_mul(vec2 A, vec2 B);
vec2 vec2_div(vec2 V, f32 M);
f32 vec2_length(vec2 V);
f32 vec2_length_squared(vec2 V);
f32 vec2_distance(vec2 A, vec2 B);
f32 vec2_distance_squared(vec2 A, vec2 B);
vec2 vec2_normalize(vec2 V);
vec2 vec2_lerp(vec2 From, vec2 To, f32 T);

typedef struct vec3
{
	union
	{
		struct
		{
			f32 x;
			f32 y;
			f32 z;
		};
		struct
		{
			f32 r;
			f32 g;
			f32 b;
		};
		f32 E[3];
	};
}vec3;

vec3 Vec3(f32 X, f32 Y, f32 Z);
vec3 vec2_to3(vec2 V2, f32 z);
vec2 vec3_to2(vec3 V3);
vec3 vec3_add(vec3 A, vec3 B);

typedef struct vec4
{
	union
	{
		struct
		{
			f32 x;
			f32 y;
			f32 z;
			f32 w;
		};
		struct
		{
			f32 r;
			f32 g;
			f32 b;
			f32 a;
		};
		f32 E[4];
	};
}vec4;

vec4 Vec4(f32 X, f32 Y, f32 Z, f32 A);

//(1-t)*A + T*B;
vec4 vec4_lerp(vec4 From, vec4 To, f32 T);

f32 lerp(f32 From, f32 To, f32 T);

vec4 mat4_mul_vec4(f32 *Mat, vec4 Pos);

//collision primitives

GOBJECT()
typedef struct circle
{
	vec2 Pos;
	f32 Rad;
}circle;

b32 collision_test_circle_point(circle Circle, vec2 Pos);

GOBJECT()
typedef struct rect
{
	vec2 Pos;
	vec2 Rad;
} rect;

rect Rect(vec2 Pos, vec2 Rad);

b32 collision_test_rect_point(rect Rect, vec2 Pos);

b32 collision_ray_rect(vec2 StartPos, vec2 Direction, rect Rect, f32 *TMin, vec2 *HitP, vec2 *HitN);

//RANDOM

typedef struct random_state
{
	u64 state0;
	u64 state1;
}random_state;

random_state Random(u64 Seed);
u64 random_next(random_state *Random);
f32 random_01(random_state *Random);

//random [From, To) works perfect for random array indexes
u32 random_range(random_state *Random, u32 From, u32 To);

vec2 random_vec2(random_state *Random);

#endif
