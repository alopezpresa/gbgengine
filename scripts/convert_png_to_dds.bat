@echo "Usage %~n0 [from folder] [to folder]"
@echo "Converting all png files from " %1
@echo "And exporting them as dds to " %2

set "orig=%1"
set "dest=%2"

for %%f in ("%orig%\*.png") do (
	echo processing file: "%%f" to "%dest%%%~nf.dds"
	..\\..\\gbgengine\tools\Compressonator_4.2.5185\compressonatorcli.exe -fd DXT5 "%%f" "%dest%%%~nf.dds"

)

