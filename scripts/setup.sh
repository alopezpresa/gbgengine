OutputDir="../../linux"
mkdir $OutputDir

echo Copy icon
#cp ../linux/icon.rc $OutputDir/icon.rc

mkdir ../../assets/res
cp ../assets/res/appicon.ico ../../assets/res/appicon.ico

echo Copy basic shaders
cp -r ../assets/shaders/gl/ ../../assets/shaders/

echo Copy compile.bat
cp ../linux/compile.sh $OutputDir/

echo Installing dev packages, need sudo password
sudo apt install libx11-dev libgl-dev gcc libasound2-dev libpulse-dev
