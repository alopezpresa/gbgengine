set OutputDir= ..\..\android
set AssetsDir=%OutputDir%\GbGEngine\app\src\main\assets

REM Copying android project folder...
mkdir %OutputDir%\
xcopy ..\android\ %OutputDir%\ /s /e
REM Done.

REM Copying basic shaders
mkdir %AssetsDir%
mkdir %AssetsDir%\gles
xcopy ..\assets\shaders\gles\ %AssetsDir%\shaders\gles\

