REM Copy icon
set OutputDir= ..\..\win64
mkdir %OutputDir%\
copy ..\win64\icon.rc %OutputDir%\icon.rc

mkdir ..\..\assets\res
copy ..\assets\res\appicon.ico ..\..\assets\res\appicon.ico

REM Copy basic shaders
xcopy ..\assets\shaders\gl\ ..\..\assets\shaders\gl\ /e /y

REM Copy compile.bat
copy ..\win64\compile.bat %OutputDir%\
copy ..\win64\icon.rc %OutputDir%\
