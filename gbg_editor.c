#include "gbg_editor.h"
#include "gbg_rtti.h"
#include "gbg_imgui.h"
#include "gbg_world.h"

static u32 read_record_header(engine *Engine, file_handle File, u32 ReadFrom, rtti_record **OutRecord)
{
	char *RecordName = 0;
	u32 Read = read_string(Engine, File, ReadFrom, &RecordName);
	*OutRecord = rtti_record_get_by_name(Engine, RecordName);
	string_delete(Engine, RecordName);

	return Read;
}

static u32 read_record(engine *Engine, file_handle File, u32 ReadFrom, rtti_record *Record, u8 *Obj)
{
	u32 InitialReadFrom = ReadFrom;

	for (u32 i=0; i<dynamic_array_len(Record->Fields); ++i)
	{
		rtti_field_type FieldType;
		ReadFrom += Engine->Platform->FileRead(File, (u8*)&FieldType, sizeof(FieldType),  ReadFrom);

		rtti_field *Field = Record->Fields + i;
		if (Field->Type != FieldType)
		{
			Engine->Platform->Log("Error! File has %d type, but record %s has type %d\n.", FieldType, Record->Name, Field->Type);
			return 0;
		}

		if (Field->Type == RFT_Struct || Field->Type == RFT_Union)
		{
			rtti_record *SubRecord = 0;
			ReadFrom += read_record_header(Engine, File, ReadFrom, &SubRecord);
			if (string_compare(SubRecord->Name, "named_pointer"))
			{
				named_pointer *NP = (named_pointer*)(Obj + Field->Offset);
				ReadFrom += read_string(Engine, File, ReadFrom, &NP->RecordName);
				rtti_record *NPRecord = rtti_record_get_by_name(Engine, NP->RecordName);
				NP->Ptr = memory_alloc(&Engine->Memory, rtti_record_get_size(Engine, NPRecord));
				ReadFrom += read_record(Engine, File, ReadFrom, NPRecord, NP->Ptr);
			}
			else if (string_compare(SubRecord->Name, "texture_ref"))
			{
				texture_ref *TexRef = (texture_ref*)(Obj + Field->Offset);
				ReadFrom += read_string(Engine, File, ReadFrom, &TexRef->PathName);
			}
			else
			{
				u32 Read = read_record(Engine, File, ReadFrom, SubRecord, Obj + Field->Offset);
				if (Read)
				{
					ReadFrom += Read;
				}
				else
				{
					return 0;
				}
			}
		}
		else
		{
			u32 Read = Engine->Platform->FileRead(File, (u8*)Obj + Field->Offset, Field->Size, ReadFrom);
			if (Read == Field->Size)
			{
				ReadFrom += Read;
			}
			else
			{
				Engine->Platform->Log("Error reading from file. Expected %d. Got %d. Skipping Record %s.\n", Field->Size, Read, Record->Name);
				return 0;
			}
		}
	}

	return ReadFrom - InitialReadFrom;
}

static void editor_read_record(engine *Engine, char *FileName, rtti_record **Record, void **Obj)
{
	file_handle Handle = Engine->Platform->FileOpen(FileName);
	//TODO: add file handle check for validity
	u8 Version;
	char GEA[3];

	u32 ReadFrom = Engine->Platform->FileRead(Handle, &Version, 1, 0);
	if (Version == 1)
	{
		ReadFrom += Engine->Platform->FileRead(Handle, GEA, 3, ReadFrom);
		if (GEA[0] == 'G' && GEA[1] == 'E' && GEA[2] == 'A')
		{
			char *RecordName = 0;
			ReadFrom += read_string(Engine, Handle, ReadFrom, &RecordName);
			*Record = rtti_record_get_by_name(Engine, RecordName);
			string_delete(Engine, RecordName);
			u32 RecSize = rtti_record_get_size(Engine, *Record);
			*Obj = memory_alloc(&Engine->Memory, RecSize);
			u32 Read = read_record(Engine, Handle, ReadFrom, *Record, (u8*)*Obj);
			if (Read == 0)
			{
				Engine->Platform->Log("Error loading file %s. Unable to load %s\n", FileName, RecordName);
				memory_free(&Engine->Memory, *Obj);
			}
		}
	}

	Engine->Platform->FileClose(Handle);
}	

void editor_init(engine *Engine)
{
	Engine->Editor = memory_alloc_type(&Engine->Memory, editor);
	memset(Engine->Editor, 0, sizeof(editor));

	editor *Editor = Engine->Editor;

	Editor->Config.HideImprints = dynamic_array_create(Engine, char*);

	Editor->LevelsDir = string_new(Engine,"%s/levels", Engine->AssetDatabase.RootPath);
	Engine->Platform->DirectoryCreate(Editor->LevelsDir);

	Editor->SavedImprints = dynamic_array_create(Engine, imprint);

	char *TemplateDir = string_new(Engine, "%s/imprints/*.gea", Engine->AssetDatabase.RootPath);
	Engine->Platform->ListFileStart(TemplateDir);
	for (;;) 
	{
		char *FileName = Engine->Platform->ListFileNext();
		if (FileName == 0)
			break;

		char *FullPath = string_new(Engine, "%s/imprints/%s", Engine->AssetDatabase.RootPath, FileName);

		rtti_record *Record = 0;
		entity *Ent = 0;
		editor_read_record(Engine, FullPath, &Record, &Ent);
		char *ImprintName = string_substr(Engine, FileName, 0, string_index_of(FileName, '.'));
		imprint EdObj = {ImprintName, Ent};
		dynamic_array_add(&Engine->Editor->SavedImprints, EdObj);
		string_delete(Engine, FullPath);
	} 
	string_delete(Engine, TemplateDir);

	Editor->TextureNames = dynamic_array_create(Engine, char*);
	char *Empty = 0;
	dynamic_array_add(&Editor->TextureNames, Empty);

	char *TexturesDir = string_new(Engine, "%s/textures/*.dds", Engine->AssetDatabase.RootPath);
	Engine->Platform->ListFileStart(TexturesDir);
	for (;;)
	{
		char *FileName = Engine->Platform->ListFileNext();
		if (FileName == 0)
			break;

		char *FullPath = string_new(Engine, "textures/%s", FileName);
		asset_load_texture(Engine, FullPath);
		//TODO: This should be selectable from the editor, the blend mode and order
		render_sprite_batch_add(Engine, FullPath, 0, BlendMode_Normal);

		dynamic_array_add(&Editor->TextureNames, FullPath);
	}

	string_delete(Engine, TexturesDir);

	Engine->Editor->RecordNames = dynamic_array_create(Engine, char*);
	dynamic_array_add(&Engine->Editor->RecordNames, Empty);

	for (u32 r=0; r<dynamic_array_len(Engine->Rtti->Records); ++r)
	{
		rtti_record *R = Engine->Rtti->Records + r;
		dynamic_array_add(&Engine->Editor->RecordNames, R->Name);
	}

	Engine->Rtti->CachedEntityRecord = rtti_record_get_by_name(Engine, "entity");

	Engine->Editor->ListBoxIndices = dynamic_array_create(Engine, u32);
	//Green
	//Engine->ImGui->Theme.DefaultColor = Vec4(0.188f, 0.796f, 0.388f, 1.0f);
	//Blue
//	Engine->ImGui->Theme.DefaultColor = Vec4(0.51f, 0.565f, 0.824f, 1.0f);
	Engine->ImGui->FontColor = Vec4(1, 1, 1, 1);
	//Violet
//	Engine->ImGui->Theme.DefaultColor = Vec4(0.171f, 0.132f, 0.281f, 1.0f);
//	Black
	Engine->ImGui->Theme.DefaultColor = Vec4(0.131f, 0.131f, 0.131f, 1.0f);
	Engine->ImGui->Theme.MouseOverColor = Vec4(0.333f, 0.333f, 0.333f, 1.0f);
	Engine->ImGui->Theme.PressedColor = Vec4(0.222f, 0.222f, 0.222f, 1.0f);
	Engine->ImGui->Theme.BorderColor = Vec4(0.833f, 0.833f, 0.833f, 1.0f);
	Engine->ImGui->Theme.SelectedColor = Vec4(0.333f, 0.333f, 0.333f, 1.0f);
	Engine->ImGui->Theme.SelectedBorderColor = Vec4(0.648f, 0.699f, 0.968f, 1.0f);

	Editor->CollapsedStatus = dynamic_array_create(Engine, b32);
}

static u32 write_record_header(engine *Engine, file_handle File, rtti_record *Record, u32 From)
{
	u32 StringLen = string_len(Record->Name);
	u32 Written = write_string(Engine, File, Record->Name, StringLen, From);

	return Written;
}

static u32 write_record(engine *Engine, file_handle File, rtti_record *Record, u8 *Object, u32 From)
{
	u32 Written = 0;

	if (string_compare(Record->Name, "named_pointer"))
	{
		named_pointer *NP = (named_pointer*)Object;
		rtti_record *NPRec = rtti_record_get_by_name(Engine, NP->RecordName);
		Written += write_record_header(Engine, File, NPRec, From);
		Written += write_record(Engine, File, NPRec, NP->Ptr, From + Written);

		return Written;
	}
	else if (string_compare(Record->Name, "texture_ref"))
	{
		texture_ref *TexRef = (texture_ref*)Object;
		Written += write_string(Engine, File, TexRef->PathName, string_len(TexRef->PathName), From);

		return Written;
	}

	for (u32 i=0; i<dynamic_array_len(Record->Fields); ++i)
	{
		rtti_field *Field = Record->Fields + i;

		//TODO: Save field name to check if the record changed between serialization and loading
		Written += Engine->Platform->FileWrite(File, (u8*)&Field->Type, sizeof(Field->Type), From + Written);

		u8 *Obj = (u8*)Object + Field->Offset;

		//NOTE: What's the best way to save this? is that an array?
		if (Field->IsPointer)
		{
			if (Field->Type == RFT_String)
			{
				Written += write_string(Engine, File, Obj, string_len((char*)Obj), From + Written);
			}
		}
		else
		{
			assert(Field->Type != RFT_Unknown);
			if (Field->Type == RFT_Struct) 
			{
				//recurse
				rtti_record *SubRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);
				Written += write_record_header(Engine, File, SubRecord, From + Written);
				Written += write_record(Engine, File, SubRecord, Obj, From + Written);
			}
			else if (Field->Type == RFT_Union)
			{
				rtti_record *SubRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);
				assert(SubRecord->IsUnion);
				rtti_field *BiggestField = 0;
				u32 BiggestSize = 0;
				//Find the biggest field, only serialize that one
				for (u32 f=0; f<dynamic_array_len(SubRecord->Fields); ++f)
				{
					rtti_field *Field = SubRecord->Fields + f;
					if (BiggestSize < Field->Size)
					{
						BiggestSize = Field->Size;
						BiggestField = Field;
					}
				}

				if (BiggestField->Type == RFT_Struct || BiggestField->Type == RFT_Union)
				{
					//recurse
					rtti_record *BiggestRecord = rtti_record_get_by_name(Engine, BiggestField->TypeAsString);
					Written += write_record_header(Engine, File, BiggestRecord, From + Written);
					Written += write_record(Engine, File, BiggestRecord, Obj, From + Written);
				}
				else
				{
					Written += Engine->Platform->FileWrite(File, Obj, BiggestField->Size, From + Written);
				}
			}
			else
			{
				//TODO: Use hton or something similar for byte order
				//TODO: Also copy vector in parts
				Written += Engine->Platform->FileWrite(File, Obj, Field->Size, From + Written);
			}
		}
	}

	return Written;
}

static void editor_write_record(struct engine *Engine, rtti_record *Record, void *Object, char *ImprintName)
{
	char *TemplateDir = string_new(Engine, "%s/imprints", Engine->AssetDatabase.RootPath);
	Engine->Platform->DirectoryCreate(TemplateDir);
	char *TemplateName = string_new(Engine, "%s/%s.gea", TemplateDir, ImprintName);

	//TODO: Check for file validity
	file_handle File = Engine->Platform->FileCreate(TemplateName);
	u8 Version = 1;
	u32 Written = Engine->Platform->FileWrite(File, &Version, 1, 0);
	u8 Code[] = "GEA";
	Written += Engine->Platform->FileWrite(File, Code, 3, Written);
	//TODO: Is useful to write the size of the file?

	Written += write_record_header(Engine, File, Record, Written);

	//Write full record, recursive
	write_record(Engine, File, Record, Object, Written);

	Engine->Platform->FileClose(File);

	string_delete(Engine, TemplateName);
	string_delete(Engine, TemplateDir);
	Engine->Editor->NewSelected = 0;
	Engine->Editor->TempObject = 0;
}

static b32 is_mouse_on_editor_entity(engine *Engine)
{
	entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
	while (Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				rect R = {vec3_to2(Ent->Pos), Ent->RigidBody.Rect.Rad};
				if (collision_test_rect_point(R, Engine->Input->MouseState.MousePos))
				{
					return 1;
				}
			}
		}

		Cluster = Cluster->Next;
	}
	return 0;
}

static void select_entity(editor *Editor, entity *Ent)
{
	Editor->LastEntityClicked.Mode = SM_EntityLevel;
	imprint *Selected = &Editor->LastEntityClicked.Imprint;
	Selected->Entity = Ent;
}

void editor_update(engine *Engine)
{
	editor *Editor = Engine->Editor;
	if (Editor->Show)
	{
		rect R = {Vec2(710, 500), Vec2(35, 8)};
		render_set_default_color(Engine, Vec4(1, 1, 1, 1));
		render_draw_line(Engine, Vec3(650, 0, 0), Vec3(650, 540, 0));
		{
			if (Editor->CurrentWorldName)
			{
				//TODO: Normalize path separator?
				s32 SepIndex = string_reverse_index_of(Editor->CurrentWorldName, '\\') + 1;
				char *LevelLabel = string_new(Engine, "Current Level: %s", Editor->CurrentWorldName + SepIndex);
				imgui_label(Engine, LevelLabel, R.Pos, TextAlign_Left);
				string_delete(Engine, LevelLabel);
			}
			R.Pos.y -= 20;

			if (imgui_button(Engine, "New Map", R))
			{
				if (Editor->CurrentWorldName)
				{
					string_delete(Engine, Editor->CurrentWorldName);
					Editor->CurrentWorldName = 0;
				}

				world_clear_level_editor(Engine);
				Editor->NewSelected = 0;
				Editor->LastEntityClicked.Imprint.Entity = 0;
			}
			R.Pos.x += 70;
			if (imgui_button(Engine, "Open Map", R))
			{
				char *LevelName = Engine->Platform->FilePicker(Editor->LevelsDir, FP_Open, "Game Level", "*.lvl");
				if (LevelName)
				{
					world_clear_level_editor(Engine);

					file_handle LvlHandle = Engine->Platform->FileOpen(LevelName);
					char LVL[3];
					u32 ReadFrom = Engine->Platform->FileRead(LvlHandle, LVL, sizeof(LVL), 0);
					if (LVL[0] == 'L' && LVL[1] == 'V' && LVL[2] == 'L')
					{
						u32 EntCount = 0;
						ReadFrom += Engine->Platform->FileRead(LvlHandle, (u8*)&EntCount, sizeof(EntCount), ReadFrom);
						for (u32 i=0; i < EntCount; ++i)
						{
							entity *NewEnt = world_entity_create_in_editor(Engine);
							ReadFrom += read_record(Engine, LvlHandle, ReadFrom, Engine->Rtti->CachedEntityRecord, (u8*)NewEnt);
						}
					}
					Engine->Platform->FileClose(LvlHandle);
					Engine->Editor->CurrentWorldName = LevelName;
				}
			}
			R.Pos.x += 70;

			if (imgui_button(Engine, "Save Map", R))
			{
				char *LevelName = 0;

				if (!Editor->CurrentWorldName)
					Editor->CurrentWorldName = Engine->Platform->FilePicker(Editor->LevelsDir, FP_Save, "Game Level", "*.lvl");

				if (Editor->CurrentWorldName)
				{
					file_handle LvlHandle = Engine->Platform->FileCreate(Editor->CurrentWorldName);
					u32 Written = Engine->Platform->FileWrite(LvlHandle, "LVL", 3, 0);
					u32 EntCount = 0;
					Written += Engine->Platform->FileWrite(LvlHandle, (u8*)&EntCount, sizeof(EntCount), Written);

					entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
					while (Cluster)
					{
						for (u32 i=0; i<static_array_len(Cluster->E); ++i)
						{
							entity *EdEnt = Cluster->E + i;
							if (EdEnt->IsValid)
							{
								Written += write_record(Engine, LvlHandle, Engine->Rtti->CachedEntityRecord, (u8*)EdEnt, Written);
								++EntCount;
							}
						}
						Cluster = Cluster->Next;
					}

					Written += Engine->Platform->FileWrite(LvlHandle, (u8*)&EntCount, sizeof(EntCount), 3);
					Engine->Platform->FileClose(LvlHandle);
				}
			}

			R.Pos.x = 700;
			R.Pos.y -= 20;
			R.Rad.x = 25;
			if (Engine->World->CurrentState == WorldState_Stop)
			{
				if (imgui_button(Engine, "Play", R))
				{
					Engine->World->SetTransition = WorldTransition_Play;
				}

				//draw lines
				render_set_default_color(Engine, Vec4(1, 1, 1, 0.5));
				for (u32 i=0; i<13; ++i)
				{
					f32 X = (i*51)+26;
					render_draw_line(Engine, Vec3(X, 212, 0), Vec3(X, 513, 0));
				}
				for (u32 i=0; i<13; ++i)
				{
					f32 Y = (i*25)+213;
					render_draw_line(Engine, Vec3(26, Y, 0), Vec3(638, Y, 0));
				}
			}
			else
			{
				imgui_label(Engine, "Playing", R.Pos, TextAlign_Center);
			}


			R.Pos.x += 50;
			if (Engine->World->CurrentState == WorldState_Play)
			{
				if (imgui_button(Engine, "Stop", R))
				{
					Engine->World->SetTransition = WorldTransition_Stop;
				}
			}
			else
			{
				imgui_label(Engine, "Stopped", R.Pos, TextAlign_Center);
			}
		}

		R.Pos.x = 718;
		R.Pos.y -= 24;
		R.Rad = Vec2(44, 10);

		if (!Engine->Editor->NewSelected)
		{
			if (Engine->Editor->Config.HideEntityCreation == 0)
			{
				if (imgui_button(Engine, "New Entity...", R))
				{
					Engine->Editor->TempObject = memory_alloc(&Engine->Memory, sizeof(entity));
					entity Ent = {.Scale = {1, 1}, 
						.Material = {.Color = {1, 1, 1, 1}
						},
					};
					*(entity*)Engine->Editor->TempObject = Ent;
					Engine->Editor->NewSelected = Engine->Rtti->CachedEntityRecord;
					Engine->Editor->LastEntityClicked.Imprint.Entity = 0;

					dynamic_array_clear(Engine->Editor->ListBoxIndices);
					Engine->Editor->ListBoxCurIndex = 0;
				}
			}

			//Show already saved imprints
			u32 BtnX = 694;
			for (u32 i = 0; i<dynamic_array_len(Engine->Editor->SavedImprints); ++i)
			{
				imprint *Obj = Engine->Editor->SavedImprints + i;

				b32 ShowImprint = 1;
				for (u32 HideIx=0; HideIx<dynamic_array_len(Engine->Editor->Config.HideImprints); ++HideIx)
				{
					char *HideName = Engine->Editor->Config.HideImprints[HideIx];
					if (string_compare(HideName, Obj->Name))
					{
						ShowImprint = 0;
						break;
					}
				}	

				if (ShowImprint == 0)
					continue;

				entity *Ent = (entity*)Obj->Entity;
				Ent->Pos.x = BtnX;
				Ent->Pos.y = R.Pos.y - 34;
				Ent->Pos.z = -0.4;

				BtnX += 42;

				vec2 OldScale = Ent->Scale;
				float Scale = 16.0f / Ent->RigidBody.Rect.Rad.x;
				Ent->Scale = Vec2(Scale, Scale);

				entity_render(Engine, Ent);	
				Ent->Scale = OldScale;

				rect BtnR;
				BtnR.Pos = vec3_to2(Ent->Pos);
				BtnR.Rad = Vec2(20, 20);
				if (imgui_button(Engine, "", BtnR))
				{
					//TODO: Delete the old instance
					imprint Copy;
					Copy.Name = Obj->Name;
					Copy.Entity = rtti_duplicate(Engine, Engine->Rtti->CachedEntityRecord, Ent);

					Editor->LastEntityClicked.Imprint = Copy;
					Editor->LastEntityClicked.Mode = SM_Imprint;
				}
			}
		}
		else
		{
			rtti_record *S = Engine->Editor->NewSelected;
			char *Title = string_new(Engine, "Create new %s", S->Name);
			imgui_label(Engine, Title, Vec2(680, 450), TextAlign_Left);
			string_delete(Engine, Title);
			imgui_label(Engine, "Name", Vec2(680, 430), TextAlign_Left);
			rect R = {Vec2(850, 430), Vec2(90, 10)};
			imgui_edit_text(Engine, Engine->Editor->NameOut, static_array_len(Engine->Editor->NameOut), R);

			vec2 P = {680, 400};

			Engine->Editor->ListBoxCurIndex = 0;
			Engine->Editor->CollapseCount = 0;
			Engine->Editor->ListBoxCurIndex = 0;
			editor_show_record(Engine, S, Engine->Editor->TempObject, &P);

			P.y -= 20;
			P.x = 800;
			R.Pos = P;
			R.Rad = Vec2(40, 10);

			if (imgui_button(Engine, "Save", R))
			{
				void *TmpObj = Engine->Editor->TempObject;
				imprint Obj = {string_dup(Engine, Engine->Editor->NameOut), TmpObj};
				dynamic_array_add(&Engine->Editor->SavedImprints, Obj);
				editor_write_record(Engine, Engine->Rtti->CachedEntityRecord, TmpObj, Engine->Editor->NameOut);
			}

			R.Pos.x += 80;
			if (imgui_button(Engine, "Cancel", R))
			{
				Engine->Editor->NewSelected = 0;
				memory_free(&Engine->Memory, Engine->Editor->TempObject);
			}
		}

		//update clicked entity
		if (Editor->LastEntityClicked.Imprint.Entity && Engine->World->CurrentState == WorldState_Stop)
		{
			vec2 P = {680, R.Pos.y - 70};
			b32 CurReadMode = Engine->ImGui->ReadOnlyMode;
			if (Editor->Config.HideEntityCreation && Editor->LastEntityClicked.Mode == SM_Imprint)
			{
				Engine->ImGui->ReadOnlyMode = 1;
			}

			Engine->Editor->ListBoxCurIndex = 0;
			Engine->Editor->CollapseCount = 0;
			Engine->Editor->ListBoxCurIndex = 0;
			editor_show_record(Engine, Engine->Rtti->CachedEntityRecord, Editor->LastEntityClicked.Imprint.Entity, &P);

			Engine->ImGui->ReadOnlyMode = CurReadMode;

			P.x += 40;
			P.y -= 20;
			rect R = {P, {40, 10}};

			if (Editor->LastEntityClicked.Mode == SM_Imprint)
			{
				if (!Editor->Config.HideEntityCreation)
				{
					if (imgui_button(Engine, "Save", R))
					{
						editor_write_record(Engine, Engine->Rtti->CachedEntityRecord, Editor->LastEntityClicked.Imprint.Entity, Editor->LastEntityClicked.Imprint.Name);
					}
					R.Pos.x += 90;
					if (imgui_button(Engine, "Cancel", R))
					{
						Editor->LastEntityClicked.Imprint.Entity = 0;
					}
				}

				vec2 MousePos = Engine->Input->MouseState.MousePos;
				if (MousePos.x > 25 && MousePos.x < 624 && MousePos.y > 216 && MousePos.y < 510)
				{
					entity *Ent = Editor->LastEntityClicked.Imprint.Entity;

					s32 X = ((Engine->Input->MouseState.MousePos.x+25)/51);
					X *= 51;

					s32 Y = ((Engine->Input->MouseState.MousePos.y+12)/25);
					Y *= 25;
					Ent->Pos = Vec3(X, Y, 0);
					entity_render(Engine, Ent);

					if (input_was_button_clicked(&Engine->Input->MouseState.LeftButton))
					{
						if (is_mouse_on_editor_entity(Engine))
						{
							select_entity(Editor, Ent);
						}
						else
						{
							world_entity_create_from_imprint_in_editor(Engine, &Editor->LastEntityClicked.Imprint);
						}
					}
				}
			}
			else 
			{
				b32 DeletePressed = 0;
				for (u32 i=0; i<dynamic_array_len(Engine->Input->PlayerControllers->Keys); ++i)
				{
					key *K = Engine->Input->PlayerControllers->Keys + i;
					if (K->State == ButtonState_Down)
					{
						if (K->KeyCode == KeyCode_Delete)
						{
							DeletePressed = 1;
							break;
						}
					}
				}
				if (imgui_button(Engine, "Delete", R) || DeletePressed)
				{
					entity_destroy(Engine, Editor->LastEntityClicked.Imprint.Entity);
					Editor->LastEntityClicked.Imprint.Entity = 0;
				}
			}
		}

		if (input_was_button_clicked(&Engine->Input->MouseState.RightButton))
		{
			Editor->LastEntityClicked.Imprint.Entity = 0;
		}

		//Highlight mouse over entities
		if (Editor->LastEntityClicked.Imprint.Entity == 0 || Editor->LastEntityClicked.Mode == SM_EntityLevel)
		{
			entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
			while (Cluster)
			{
				for (u32 i=0; i<static_array_len(Cluster->E); ++i)
				{
					entity *Ent = Cluster->E + i;
					if (Ent->IsValid)
					{
						b32 IsEntSelected = Ent == Editor->LastEntityClicked.Imprint.Entity;
						rect R = {vec3_to2(Ent->Pos), Ent->RigidBody.Rect.Rad};
						if (IsEntSelected || collision_test_rect_point(R, Engine->Input->MouseState.MousePos))
						{
							render_set_default_color(Engine, Vec4(1, 1, 1, 1));
							if (Ent->RigidBody.Type == RB_Circle)
							{
								render_draw_circle(Engine, Ent->Pos, Ent->RigidBody.Circle.Rad * 1.1f);
							}
							else
							{
								vec3 Pos = Ent->Pos;
								Pos.z += 0.02;
								render_draw_rect(Engine, Pos, vec2_mul(Ent->RigidBody.Rect.Rad, 2.1));
							}

							if (input_was_button_clicked(&Engine->Input->MouseState.LeftButton))
							{
								select_entity(Editor, Ent);
							}
						}
					}
				}
				Cluster = Cluster->Next;
			}
		}
	}
}

void editor_show_record(engine *Engine, rtti_record *Record, void *Obj, vec2 *StartPos)
{
	if (Engine->Editor->Config.HideEntityEdit)
		return;

	f32 YInc = 18.0f;
	f32 XInc = 20.0f;
	f32 FieldW = 8.0f;
	rtti *Rtti = Engine->Rtti;
	char TempField[256];
	memset(TempField, 0, static_array_len(TempField));
	u8 *ObjPtr = (u8*)Obj;
	for (u32 f=0; f<dynamic_array_len(Record->Fields); ++f)
	{
		rtti_field *Field = Record->Fields + f;

		if (Engine->Editor->Config.AllowOnlyGameDataEdit)
		{
			if (Field->Type != RFT_Struct)
				continue;

			if (string_compare(Field->TypeAsString, "named_pointer") == 0)
				continue;
		}

		if (Field->Name)
		{
			imgui_label(Engine, Field->Name, *StartPos, TextAlign_Left);
		}
		rect FieldR = {*StartPos};
		FieldR.Pos.x += 140;
		FieldR.Rad = Vec2(60, FieldW);

		switch (Field->Type)
		{
			case RFT_Boolean:
			{
				FieldR.Rad = Vec2(8, FieldW);
				b32 *BoolVal = (b32*)(ObjPtr + Field->Offset);
				if (imgui_tick_box(Engine, *BoolVal, FieldR))
				{
					*BoolVal = !*BoolVal;
				}
			}	break;
			case RFT_Integer:
			{
				s32 *IntVal = (s32*)(ObjPtr + Field->Offset);
				imgui_edit_int(Engine, IntVal, FieldR);
			}	break;
			case RFT_Float:
			{
				f32 *FloatVal = (f32*)(ObjPtr + Field->Offset);
				imgui_edit_float(Engine, FloatVal, FieldR);
			}	break;
			case RFT_Char:
				break;
			case RFT_Enum:
				for (u32 i=0; i<dynamic_array_len(Rtti->Enums); ++i)
				{
					rtti_enum *Enum = Rtti->Enums + i;
					if (string_compare(Enum->Name, Field->TypeAsString))
					{
						char **ListNames = dynamic_array_create(Engine, char*);
						for (u32 f=0; f<dynamic_array_len(Enum->Fields); ++f)
						{
							char *Name = (Enum->Fields + f)->Name;
							dynamic_array_add(&ListNames, Name);
						}
						u32 *EnumValue = (u32*)(ObjPtr + Field->Offset);

						imgui_list_box(Engine, ListNames, FieldR, EnumValue);
						dynamic_array_destroy(ListNames);
					}
				}
				break;
			case RFT_Vec:
				if (Field->Size/sizeof(f32) == 2)
				{
					vec2 *VecVal = (vec2*)(ObjPtr + Field->Offset);
					FieldR.Pos.x -= 150;

					FieldR.Pos.x += 100;

					char *Header[] = {"X", "Y"};
					FieldR.Rad = Vec2(30, FieldW);
					for (u32 i=0; i<2; ++i)
					{
						imgui_label(Engine, Header[i], FieldR.Pos, TextAlign_Left);
						FieldR.Pos.x += 40;
						imgui_edit_float(Engine, &VecVal->E[i], FieldR);
						FieldR.Pos.x += 40;
					}
				}
				else if (Field->Size/sizeof(f32) == 3)
				{
					vec3 *VecVal = (vec3*)(ObjPtr + Field->Offset);
					FieldR.Pos.x -= 150;

					FieldR.Pos.x += 100;

					FieldR.Rad = Vec2(20, FieldW);
					char *Header[] = {"X", "Y", "Z"};
					for (u32 i=0; i<3; ++i)
					{
						imgui_label(Engine, Header[i], FieldR.Pos, TextAlign_Left);
						FieldR.Pos.x += 30;
						imgui_edit_float(Engine, &VecVal->E[i], FieldR);
						FieldR.Pos.x += 30;
					}
				}
				else
				{
					vec4 *VecVal = (vec4*)(ObjPtr + Field->Offset);
					FieldR.Pos.x -= 150;

					FieldR.Pos.x += 100;

					FieldR.Rad = Vec2(16, FieldW);
					char *Header[] = {"R", "G", "B", "A"};
					for (u32 i=0; i<4; ++i)
					{
						imgui_label(Engine, Header[i], FieldR.Pos, TextAlign_Left);
						FieldR.Pos.x += 22;
						imgui_edit_float(Engine, &VecVal->E[i], FieldR);
						FieldR.Pos.x += 22;
					}
				}
				break;
			case RFT_Struct:
				if (string_compare(Field->TypeAsString, "named_pointer"))
				{
					//show struct selector
					named_pointer *NP = (named_pointer*)(ObjPtr + Field->Offset);
					if (Engine->Editor->ListBoxCurIndex == dynamic_array_len(Engine->Editor->ListBoxIndices))
					{
						u32 Val = 0;
						for (u32 i=1; i<dynamic_array_len(Engine->Editor->RecordNames); ++i)
						{
							if (string_compare(NP->RecordName, Engine->Editor->RecordNames[i]))
							{
								Val = i;
								break;
							}
						}
						dynamic_array_add(&Engine->Editor->ListBoxIndices, Val);
					}
					
					rtti_record *SelectedRec = 0;

					u32 *SelectedVal = &Engine->Editor->ListBoxIndices[Engine->Editor->ListBoxCurIndex];
					b32 OldMode = Engine->ImGui->ReadOnlyMode;
					if (Engine->Editor->Config.AllowOnlyGameDataEdit)
					{
						Engine->ImGui->ReadOnlyMode = 1;
					}
					if (imgui_list_box(Engine, Engine->Editor->RecordNames, FieldR, SelectedVal))
					{
						if (NP->Ptr)
						{
							//TODO: change it for a rtti_delete();
							memory_free(&Engine->Memory, NP->Ptr);
						}
						NP->RecordName = Engine->Editor->RecordNames[*SelectedVal];
						if (NP->RecordName == 0)
							break;

						SelectedRec = rtti_record_get_by_name(Engine, NP->RecordName);
						u32 MemSize = rtti_record_get_size(Engine, SelectedRec);
						//TODO: Don't leak this on Cancel click
						NP->Ptr = memory_alloc(&Engine->Memory, MemSize);
						memset(NP->Ptr, 0, MemSize);
					}

					Engine->ImGui->ReadOnlyMode = OldMode;

					if (NP->Ptr)
					{
						SelectedRec = rtti_record_get_by_name(Engine, NP->RecordName);
						StartPos->y -= YInc;
						StartPos->x += XInc;

						b32 OldConfig = Engine->Editor->Config.AllowOnlyGameDataEdit;
						Engine->Editor->Config.AllowOnlyGameDataEdit = 0;
						editor_show_record(Engine, SelectedRec, NP->Ptr, StartPos);
						Engine->Editor->Config.AllowOnlyGameDataEdit = OldConfig;
						StartPos->y += YInc;
						StartPos->x -= XInc;
					}

					++Engine->Editor->ListBoxCurIndex;
					break;
				}
				else if (string_compare(Field->TypeAsString, "texture_ref"))
				{
					//show asset selector
					texture_ref *TexRef = (texture_ref*)(ObjPtr + Field->Offset);
					if (Engine->Editor->ListBoxCurIndex == dynamic_array_len(Engine->Editor->ListBoxIndices))
					{
						u32 Val = 0;
						for (u32 i=1; i<dynamic_array_len(Engine->Editor->TextureNames); ++i)
						{
							if (string_compare(TexRef->PathName, Engine->Editor->TextureNames[i]))
							{
								Val = i;
								break;
							}
						}

						dynamic_array_add(&Engine->Editor->ListBoxIndices, Val);
					}

					u32 *SelectedVal = &Engine->Editor->ListBoxIndices[Engine->Editor->ListBoxCurIndex];
					rect R = FieldR;
					R.Rad.x += 20;
					if (imgui_list_box(Engine, Engine->Editor->TextureNames, R, SelectedVal))
					{
						TexRef->PathName = Engine->Editor->TextureNames[*SelectedVal];
						TexRef->Asset = asset_get(Engine, TexRef->PathName);
					}
					++Engine->Editor->ListBoxCurIndex;
					break;
				}
				else
				{
					if (Engine->Editor->CollapseCount == dynamic_array_len(Engine->Editor->CollapsedStatus))
					{
						b32 Val = 0;
						dynamic_array_add(&Engine->Editor->CollapsedStatus, Val);
					}
					rect CollapseButtonRect = FieldR;
					CollapseButtonRect.Pos = *StartPos;
					CollapseButtonRect.Pos.x -= 16;
					CollapseButtonRect.Rad.x = FieldW;
					b32 *ColVal = Engine->Editor->CollapsedStatus + Engine->Editor->CollapseCount;
					char *ColLbl = *ColVal? "+" : "-";
					if (imgui_button(Engine, ColLbl, CollapseButtonRect))
					{
						*ColVal = !*ColVal;
						//Regenerate list box indices, the collapse could have change the order of the list boxes
						dynamic_array_clear(Engine->Editor->ListBoxIndices);
						Engine->Editor->ListBoxCurIndex = 0;
					}
					++Engine->Editor->CollapseCount;

					if (*ColVal)
					{
						break;
					}
				}
				//falling under! that's ok
			case RFT_Union:
				for (u32 s=0; s<dynamic_array_len(Engine->Rtti->Records); ++s)
				{
					rtti_record *S = Engine->Rtti->Records + s;
					if (string_compare(S->Name, Field->TypeAsString))
					{
						void *SubObj = ObjPtr + Field->Offset;
						if (Field->Name)
						{
							StartPos->y -= YInc;
							StartPos->x += XInc;
						}
						editor_show_record(Engine, S, SubObj, StartPos);
						if (Field->Name)
							StartPos->x -= XInc;
						StartPos->y += YInc;
						break;
					}
				}
				break;
		}
		StartPos->y -= YInc;
	}
}

