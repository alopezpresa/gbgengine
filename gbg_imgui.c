#include "gbg_imgui.h"
#include "gbg_engine.h"

vec2 imgui_get_text_metrics(engine *Engine, bmp_font *Font, u32 FontSize, const char *Label, u32 LabelSize)
{
	assert(LabelSize <= string_len(Label));

	f32 Scale = FontSize/(f32)Font->GlyphH;
	f32 Width = Font->GlyphW*Scale;

	f32 W = LabelSize*Width;
	f32 H = Font->GlyphH*Scale;

	vec2 Ret = {W, H};
	return Ret;
}

static void imgui_set_possible_focus(imgui *Gui, u32 Sid)
{
	Gui->PossibleFocus.ZLayer = Gui->ZLayer;
	Gui->PossibleFocus.Sid = Sid;
	Gui->FramesInFocus = 0;
}

b32 imgui_button(engine *Engine, const char *Label, rect Rect)
{
	imgui *Gui = Engine->ImGui;
	Gui->ObjectCount++;
	imgui_label(Engine, Label, Rect.Pos, TextAlign_Center);

	u32 Sid = string_sid(Label);
	//TODO: Create another sid for buttons without Label
	if (Sid == 0)
		Sid = Gui->ObjectCount;

	b32 Click = 0;
	b32 MouseTest = collision_test_rect_point(Rect, Engine->Input->MouseState.MousePos);
	if ((MouseTest || Gui->ObjectCount == Gui->Selected) && !Gui->ReadOnlyMode)
	{
		render_set_default_color(Engine, Gui->Theme.DefaultColor);
		if (Gui->CurrentFocus == 0)
		{
			if (Gui->PossibleFocus.Sid == 0)
			{
				imgui_set_possible_focus(Gui, Sid);
			}
			else
			{
				if (Gui->PossibleFocus.ZLayer > Gui->ZLayer)
				{
					imgui_set_possible_focus(Gui, Sid);
				}
			}
		}
		else if (Gui->CurrentFocus == Sid)
		{
			b32 PlayMouseOverSound = 0;

			if (Gui->FramesInFocus == 0)
				PlayMouseOverSound = 1;

			Gui->FramesInFocus++;

			if (Gui->ObjectCount != Gui->Selected)
			{
				Gui->Selected = 0;
			}

			if (Engine->Input->MouseState.LeftButton.CurState == ButtonState_Down)
			{
				render_set_default_color(Engine, Gui->Theme.PressedColor);
			}
			else
			{
				if (Gui->Theme.Button.TextureName)
					Gui->Theme.Button.TexturePos.y = Gui->Theme.Button.TextureSize.y;
				else
				{
					render_set_default_color(Engine, Gui->Theme.MouseOverColor);
				}

				if (Gui->Theme.OnOverSound)
				{
					if (PlayMouseOverSound)
					{
						sound_play(Engine, Gui->Theme.OnOverSound);
					}
				}

				if ((MouseTest && Engine->Input->MouseState.LeftButton.PrevState == ButtonState_Down) || (Gui->Selected == Gui->ObjectCount && (input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Gamepad_A]) || Gui->EnterPressed)))
				{
					Click = 1;
					Gui->CurrentFocus = 0;
					Gui->FramesInFocus = 0;
					Gui->Selected = 0;
					Gui->PossibleFocus.Sid = 0;
					Gui->PossibleFocus.ZLayer = 0;
					Gui->CaptureInput = 0;
					if (!Gui->InsideList)
					{
						Gui->ShowList = 0;
					}
				}
			}
		}
		else
		{
			//Mouse over another button, but not the one in focus 
			if (Gui->PossibleFocus.ZLayer >= Gui->ZLayer)
			{
				imgui_set_possible_focus(Gui, Sid);
			}
		}
	}
	else
	{
		if (Gui->CurrentFocus == Sid)
		{
			Gui->CurrentFocus = 0;
			Gui->FramesInFocus = 0;
			Gui->PossibleFocus.Sid = 0;
		}

		if (Gui->Theme.Button.TextureName)
			Gui->Theme.Button.TexturePos.y = 0;
		else
			render_set_default_color(Engine, Gui->Theme.DefaultColor);
	}

	if (Gui->Theme.Button.TextureName)
	{
		render_draw_sprite(Engine, Gui->Theme.Button, vec2_to3(Rect.Pos, 0.05f), vec2_mul(Rect.Rad, 2), 0, 0);
	}
	else
	{
		//Assume baseline in the center of the glyph...
		render_draw_rect(Engine, vec2_to3(Rect.Pos, Gui->ZLayer), vec2_mul(Rect.Rad, 2));

		render_set_default_color(Engine, Gui->Theme.BorderColor);
		f32 ZP = Engine->ImGui->ZLayer - 0.02f;
		render_draw_line(Engine, vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP), vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP));
		render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP), vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP));
		render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP), Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP));
		render_draw_line(Engine, Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP), vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP));
	}

	return Click;
}

void imgui_label(engine *Engine, const char *Label, vec2 Pos, text_align Align)
{
	imgui *Gui = Engine->ImGui;
	vec2 Extents = imgui_get_text_metrics(Engine, &Gui->Font, Gui->FontSize, Label, string_len(Label));
	f32 Scale = Gui->FontSize/(f32)Gui->Font.GlyphH;
	if (Align == TextAlign_Center)
		Pos.x -= Extents.x/2 - ((Gui->Font.GlyphW*Scale)/2);

	f32 ZInc = 0.02f;
	Gui->ZLayer -= ZInc;
	render_draw_text_bmf(Engine, &Gui->Font, Gui->FontSize, Label, Pos, Gui->FontColor);
	Gui->ZLayer += ZInc;
}

//TODO: What is the meaning of the Rect here? the label? the slider? everything?
b32 imgui_slider(struct engine *Engine, const char *Label, f32 *Value, rect Rect, vec2 HandlerSize)
{
	imgui *Gui = Engine->ImGui;
	vec2 Extents = imgui_get_text_metrics(Engine, &Gui->Font, Gui->FontSize, Label, string_len(Label));
	imgui_label(Engine, Label, Rect.Pos, TextAlign_Left);
	
	vec2 SliderStartPos = Vec2((Extents.x/2) + Rect.Pos.x + 10, Rect.Pos.y);
	if (Gui->Theme.Slider.TextureName)
	{
		render_draw_sprite(Engine, Gui->Theme.Slider, vec2_to3(Vec2(SliderStartPos.x+50, SliderStartPos.y), 0.75f), Vec2(100, Rect.Rad.y), 0, 0);
	}
	else
	{
		render_set_default_color(Engine, Vec4(1, 1, 1, 1));
		f32 ZP = Engine->ImGui->ZLayer;
		render_draw_line(Engine, vec2_to3(SliderStartPos, ZP), vec2_to3(vec2_add(SliderStartPos, Vec2(100, 0)), ZP));
	}

	f32 CurVal = fmaxf(0, fminf(*Value, 1));
	rect BtnRect = {Vec2(SliderStartPos.x + 100*CurVal, Rect.Pos.y), HandlerSize};
	imgui_button(Engine, "", BtnRect);

	rect ColRect = {Vec2(SliderStartPos.x+50, Rect.Pos.y), Vec2(50 , 16)};

	b32 Ret = 0;
	u32 StringId = string_sid(Label);
	b32 AlreadyInFocus = Gui->CurrentFocus == StringId;
	if (AlreadyInFocus || collision_test_rect_point(ColRect, Engine->Input->MouseState.MousePos))
	{
		if (Engine->Input->MouseState.LeftButton.CurState == ButtonState_Down)
		{
			Gui->CurrentFocus = StringId;
			f32 MouseX = clamp(Engine->Input->MouseState.MousePos.x - SliderStartPos.x, 0, 100);
			CurVal = MouseX / 100;
			Ret = 1;
		}
	}

	if (Gui->Selected == Gui->ObjectCount)
	{
		if (input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Gamepad_DpadLeft]))
		{
			Ret = 1;
			CurVal -= 0.1f;
			if (CurVal < 0)
				CurVal = 0;
		}
		if (input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Gamepad_DpadRight]))
		{
			Ret = 1;
			CurVal += 0.1f;
			if (CurVal > 1)
				CurVal = 1;
		}
	}

	*Value = CurVal;

	return Ret;
}

static void imgui_move_cursor_down(engine *Engine)
{
	imgui *Gui = Engine->ImGui;
	Gui->Selected++;
	if (Gui->Selected > Gui->ObjectCount)
		Gui->Selected = Gui->ObjectCount;
}

static void imgui_move_cursor_up(engine *Engine)
{
	imgui *Gui = Engine->ImGui;
	Gui->Selected--;
	if (Gui->Selected < 1)
		Gui->Selected = 1;
}

void imgui_update(struct engine *Engine)
{
	imgui *Gui = Engine->ImGui;
	Gui->EnterPressed = 0;
	Gui->ZLayer = 0;

	if (Engine->Input->Gamepads[0].IsConnected)
	{
		if (input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Gamepad_DpadDown]))
		{
			imgui_move_cursor_down(Engine);
		}
		if (input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Gamepad_DpadUp]))
		{
			imgui_move_cursor_up(Engine);
		}
	}

	for (u32 k=0; k<dynamic_array_len(Engine->Input->Keys); ++k)
	{
		key *Key = Engine->Input->Keys + k;
		if (Key->State == ButtonState_Down)
		{
			if (Key->KeyCode == KeyCode_Down)
			{
				imgui_move_cursor_down(Engine);
			}
			else if (Key->KeyCode == KeyCode_Up)
			{
				imgui_move_cursor_up(Engine);
			}
			else if (Key->KeyCode == KeyCode_Enter)
			{
				Gui->EnterPressed = 1;
			}
		}
	}

	Gui->ObjectCount = 0;

	Gui->CurrentFocus = Gui->PossibleFocus.Sid;
}

typedef enum input_filter_type
{
	InputType_Text,
	InputType_Int,
	InputType_Float,
} input_filter_type;

static b32 imgui_edit_text_filter_input(struct engine *Engine, char *Text, u32 TextSize, rect Rect, input_filter_type InputType)
{
	imgui *Gui = Engine->ImGui;
	Gui->ObjectCount++;

	char *Name = string_new(Engine, "textedit%d", Gui->ObjectCount);
	u32 Sid = string_sid(Name);
	memory_free(&Engine->Memory, Name);

	vec4 BorderColor = Gui->Theme.BorderColor;

	b32 UserClicked = !Gui->ReadOnlyMode && (input_was_button_clicked(&Engine->Input->MouseState.LeftButton) || input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Gamepad_A]));

	char *CurText = Text;
	u32 CurTextSize = string_len(Text);
	if (Gui->CaptureInput == Sid)
	{
		CurText = Gui->EditText.CurText;
		CurTextSize = string_len(CurText);
		vec2 TextRect = imgui_get_text_metrics(Engine, &Gui->Font, Gui->FontSize, CurText, CurTextSize);
		//TODO: Assuming text center align
		f32 InitialPos = -TextRect.x/2.0f;

		vec2 SelectedSize = {0};
		f32 CursorPos = 0;

		vec2 Size = imgui_get_text_metrics(Engine, &Gui->Font, Gui->FontSize, CurText, Gui->EditText.Pos);
		//TODO: Depends of the text align, assuming center
		CursorPos = InitialPos + Size.x;

		if (Gui->EditText.Selection)
		{
			//Draw selection box
			SelectedSize = TextRect;
			BorderColor = Gui->Theme.SelectedBorderColor;
			render_set_default_color(Engine, Gui->Theme.SelectedColor);
			vec3 P = {CursorPos - InitialPos + Rect.Pos.x, Rect.Pos.y, Gui->ZLayer-0.02f};
			render_draw_rect(Engine, P, SelectedSize);
		}
		else
		{
			//Draw blikning cursor
			render_set_default_color(Engine, Gui->Theme.BorderColor);
			Gui->EditText.CursorBlinkTime += 0.2f; //TODO: Pass deltatime here?. Engine should hold the dt to use
			f32 BlinkTotalTime = 2;
			if (!Gui->EditText.CursorHidden)
			{
				BlinkTotalTime = 5;
				u32 Space = 4;
				u32 Len = string_len(CurText);

				f32 StartX = Rect.Pos.x + CursorPos + (SelectedSize.x/2.0f);

				f32 ZP = Engine->ImGui->ZLayer - 0.04f;
				vec3 CursorStart = {StartX, Rect.Pos.y - Rect.Rad.y + Space, ZP};
				vec3 CursorEnd = {StartX, Rect.Pos.y + Rect.Rad.y - Space, ZP};

				render_draw_line(Engine, CursorStart, CursorEnd);
			}

			if (Gui->EditText.CursorBlinkTime > BlinkTotalTime)
			{
				Gui->EditText.CursorHidden = !Gui->EditText.CursorHidden;
				Gui->EditText.CursorBlinkTime -= BlinkTotalTime;
			}
		}

		for (u32 i=0; i<dynamic_array_len(Engine->Input->Keys); i++)
		{
			key *K = Engine->Input->Keys + i;
			if (K->State == ButtonState_Down)
			{
				if (K->KeyCode == KeyCode_BackSpace)
				{
					if (Gui->EditText.Selection > 0)
					{
						u32 Cursor = Gui->EditText.Pos;
						for (u32 i=Gui->EditText.Selection; i<CurTextSize; ++i)
						{
							CurText[Cursor++] = CurText[i];	
						}
						CurText[Cursor] = 0;

						Gui->EditText.Pos = 0;
						Gui->EditText.Selection = 0;
						string_copy(CurText, Text);
						return 1;
					}
					else if (Gui->EditText.Pos > 0)
					{
						CurText[--Gui->EditText.Pos] = 0;
						string_copy(CurText, Text);
						return 1;
					}
				}
				else if (K->KeyCode == KeyCode_Enter)
				{
					Gui->CaptureInput = 0;
					Gui->CurrentFocus = 0;
					Gui->Selected = 0;
					//TODO: Make string copy safe
					string_copy(CurText, Text);
					return 1;
				}
				else if (K->KeyCode == KeyCode_Right)
				{
					if (Gui->EditText.Pos + Gui->EditText.Selection < CurTextSize)
					{
						++Gui->EditText.Pos;
					}
					else
					{
						Gui->EditText.Pos = CurTextSize;
					}
					Gui->EditText.Selection = 0;
				}
				else if (K->KeyCode == KeyCode_Left)
				{
					if (Gui->EditText.Pos > 0)
					{
						--Gui->EditText.Pos;
					}
					else
					{
						Gui->EditText.Pos = 0;
					}
					Gui->EditText.Selection = 0;
				}
				else
				{
					if (K->KeyChar[0])
					{
						//TODO: Support utf8
						char AChar = K->KeyChar[0];
						//TODO: Filter chars that the font can handle. For now skip symbols
						if (AChar == KeyCode_Tab || AChar == KeyCode_BackSpace)
							return 0;

						if (InputType == InputType_Int)
						{
							if (AChar < '0' || AChar > '9')
								return 0;
						}

						if (InputType == InputType_Float)
						{
							if (AChar < '0' || AChar > '9')
							{	
								if (AChar != '.')
									return 0;
							}
							//. is float sep? 
							//TODO: Use float separator from localization data from host
						}

						if (Gui->EditText.Selection > 0)
						{
							u32 Cursor = Gui->EditText.Pos;
							CurText[Cursor++] = AChar;
							for (u32 i=Gui->EditText.Selection; i<CurTextSize; ++i)
							{
								CurText[Cursor++] = CurText[i];	
							}
							CurText[Cursor] = 0;
							Gui->EditText.Pos = Cursor;
							Gui->EditText.Selection = 0;
							string_copy(CurText, Text);
							return 1;
						}
						else if (Gui->EditText.Pos < TextSize -1)
						{
							CurText[CurTextSize+1] = 0;
							for (u32 i=CurTextSize; i>Gui->EditText.Pos; --i)
							{
								CurText[i] = CurText[i-1];
							}
							CurText[Gui->EditText.Pos++] = AChar;
							string_copy(CurText, Text);
							return 1;
						}
					}
				}
			}
		}
	}

	if (collision_test_rect_point(Rect, Engine->Input->MouseState.MousePos) || Gui->ObjectCount == Gui->Selected)
	{
		//TODO: Change cursor
		render_set_default_color(Engine, Gui->Theme.DefaultColor);
		Gui->CurrentFocus = Sid;
		if (UserClicked)
		{
			//TODO: Click everywhere else should reset the capture
			Gui->CaptureInput = Sid;
			Gui->EditText.Pos = 0;
			Gui->EditText.Selection = CurTextSize;
			Engine->Platform->KeyboardShow();
			//TODO: make string copy safe
			string_copy(Text, Gui->EditText.CurText);
		}
	}
	else
	{
		if (Gui->CurrentFocus == Sid)
		{
			Gui->CurrentFocus = 0;
		}

		render_set_default_color(Engine, Gui->Theme.DefaultColor);
	}

	render_draw_rect(Engine, vec2_to3(Rect.Pos, Gui->ZLayer), vec2_mul(Rect.Rad, 2));

	f32 ZP = Engine->ImGui->ZLayer - 0.02f;
	render_set_default_color(Engine, BorderColor);
	render_draw_line(Engine, vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP), vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP));
	render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP), vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP));
	render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP), Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP));
	render_draw_line(Engine, Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP), vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP));

	imgui_label(Engine, CurText, Rect.Pos, TextAlign_Center);

	return 0;
}

b32 imgui_edit_text(struct engine *Engine, char *Text, u32 TextSize, rect Rect)
{
	return imgui_edit_text_filter_input(Engine, Text, TextSize, Rect, InputType_Text);
}

b32 imgui_edit_int(struct engine *Engine, s32 *IntValue, rect Rect)
{
	char TempField[64];
	snprintf(TempField, static_array_len(TempField), "%d", *IntValue);
	b32 Ret = imgui_edit_text_filter_input(Engine, TempField, static_array_len(TempField), Rect, InputType_Int);
	if (Ret)
	{
		sscanf(TempField, "%d", IntValue);
	}

	return Ret;
}

b32 imgui_edit_float(struct engine *Engine, f32 *FloatValue, rect Rect)
{
	char TempField[64];
	snprintf(TempField, static_array_len(TempField), "%.2f", *FloatValue);
	b32 Ret = imgui_edit_text_filter_input(Engine, TempField, static_array_len(TempField), Rect, InputType_Float);

	if (Ret)
	{
		sscanf(TempField, "%f", FloatValue);
	}
	return Ret;
}

b32 imgui_list_box(struct engine *Engine, const char **Labels, rect Rect, u32 *SelectedValue)
{
	imgui *ImGui = Engine->ImGui;
	ImGui->ObjectCount++;

	if (Labels == 0)
		return 0;

	u32 ElementsCount = dynamic_array_len(Labels);
	if (ElementsCount == 0)
		return 0;

	if (*SelectedValue >= ElementsCount)
	{
		*SelectedValue = 0;
	}

	const char *CurLabel = Labels[*SelectedValue];
	if (imgui_button(Engine, CurLabel, Rect))
		ImGui->ShowList = ImGui->ObjectCount;

	s32 ElementSize = ImGui->FontSize + 4;
	s32 Direction = 1;
	if (Rect.Pos.y - (ElementSize * ElementsCount) < Engine->Render->DesignScreenOrigin.y)
	{
		Direction = -1;
	}
	
	if (ImGui->ShowList == ImGui->ObjectCount)
	{
		f32 ZInc = 0.04f;
		ImGui->ZLayer -= ZInc;
		ImGui->InsideList = 1;
		for (u32 i=0; i<dynamic_array_len(Labels); ++i)
		{
			Rect.Pos.y -= ElementSize * Direction;
			const char *Label = Labels[i];
			if (imgui_button(Engine, Label, Rect))
			{
				ImGui->ShowList = 0;
				*SelectedValue = i;
				ImGui->InsideList = 0;
				return 1;
			}
		}
		ImGui->InsideList = 0;
		ImGui->ZLayer += ZInc;
	}

	return 0;
}

b32 imgui_tick_box(struct engine *Engine, b32 Value, rect Rect)
{
	f32 ZP = Engine->ImGui->ZLayer - 0.02f;
	if (Value)
	{
		render_set_default_color(Engine, Engine->ImGui->Theme.BorderColor);
//		render_draw_line(Engine, vec2_sub(Rect.Pos, Rect.Rad), vec2_add(Rect.Pos, Rect.Rad));
////		render_draw_line(Engine, Vec2(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y), Vec2(Rect.Pos.x + Rect.Rad.x, Rect.Pos.y - Rect.Rad.y));
		render_draw_rect(Engine, vec2_to3(Rect.Pos, ZP), vec2_mul(Rect.Rad, 1.2f));
	}

	render_set_default_color(Engine, Engine->ImGui->Theme.BorderColor);
	render_draw_line(Engine, vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP), vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP));
	render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP), vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP));
	render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP), Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP));
	render_draw_line(Engine, Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP), vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP));

	vec4 PrevColor = Engine->ImGui->Theme.DefaultColor;
	b32 Ret = imgui_button(Engine, "", Rect);

	return Ret;
}
