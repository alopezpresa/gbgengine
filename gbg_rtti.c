#include "gbg_rtti.h"
#include "gbg_utils.h"
#include "gbg_string.h"

#include <stdbool.h>

static rtti_field RttiField()
{
	rtti_field F = {0};
	F.Count = 1;
	return F;
}

static rtti_record RttiRecord(engine *Engine)
{
	rtti_record Obj = {0};
	Obj.Fields = dynamic_array_create(Engine, rtti_field);

	return Obj;
}

static rtti_enum RttiEnum(engine *Engine)
{
	rtti_enum Enum = {0};
	Enum.Fields = dynamic_array_create(Engine, rtti_enum_field);

	return Enum;
}

typedef enum rtti_parser_state
{
	RttiParserState_Parsing,
	RttiParserState_Name,
	RttiParserState_EnumName,
	RttiParserState_RecordFields,
	RttiParserState_EnumFields,
	RttiParserState_ObjectParams,
	RttiParserState_ObjectEnumParams,
	RttiParserState_EndObject,
	RttiParserState_PossibleCommentStart,
	RttiParserState_OneLineComment,
	RttiParserState_MultipleLinesComment,
	RttiParserState_MultipleLinesCommentPossibleEnd,
	RttiParserState_UnionBegin,
	RttiParserState_UnionFields,
	RttiParserState_UnionEnd,

	RttiParserState_Error,
} rtti_parser_state;

typedef struct rtti_parser_data
{
	char *Identifier;
	b32 Validate;
	rtti_record *RecordsStack;
	rtti_record CurrentRecord;
	rtti_field CurrentField;
	rtti_enum CurrentEnum;
	u32 CurrentEnumValue;
	rtti_parser_state PrevState;
	u32 AnonymRecordId;
} rtti_parser_data;

typedef rtti_parser_state (*rtti_parser_consume_fn)(engine *Engine, rtti_parser_data *Data, char C);

rtti_parser_state rtti_parser_consume_end_object_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '}')
	{
		Data->Validate = 1;
		return RttiParserState_EndObject;
	}
	else if (C == ';')
	{
		if (Data->Validate)
		{
			//Create new struct object 
			
			return RttiParserState_Parsing;
		}
	}
	else if (C == '/')
	{
		Data->PrevState = RttiParserState_EndObject;
		return RttiParserState_PossibleCommentStart;
	}

	return RttiParserState_EndObject;
}

rtti_parser_state rtti_parser_consume_parsing_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == ' ' || C == '\n' || C == '\t')
	{
		dynamic_array_clear(Data->Identifier);
	}
	else if (C == '(')
	{
		char *Token = Data->Identifier;
		string_strip(Token, dynamic_array_len(Data->Identifier));
		if (string_compare(Token, "GOBJECT"))
		{
			dynamic_array_clear(Data->Identifier);
			return RttiParserState_ObjectParams;
		}
		else if (string_compare(Token, "GENUM"))
		{
			dynamic_array_clear(Data->Identifier);
			return RttiParserState_ObjectEnumParams;
		}
	}
	else if (C == '/')
	{
		Data->PrevState = RttiParserState_Parsing;
		return RttiParserState_PossibleCommentStart;
	}

	dynamic_array_add(&Data->Identifier, C);

	return RttiParserState_Parsing;
}

static rtti_parser_state start_parsing_fields(engine *Engine, rtti_parser_data *Data)
{
	Data->Validate = 0;
	Data->CurrentRecord.Name = string_dup(Engine, Data->Identifier);
	dynamic_array_clear(Data->CurrentRecord.Fields);
	Data->CurrentField = RttiField();
	dynamic_array_clear(Data->Identifier);
	return RttiParserState_RecordFields;
}

rtti_parser_state rtti_parser_consume_enum_name_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '{')
	{
		char *Name = Data->Identifier;
		string_strip(Name, dynamic_array_len(Data->Identifier));
		Data->CurrentEnum.Name = string_dup(Engine, Name);
		dynamic_array_clear(Data->Identifier);
		return RttiParserState_EnumFields;
	}
	else if (C == '/')
	{
		Data->PrevState = RttiParserState_EnumName;
		return RttiParserState_PossibleCommentStart;
	}

	dynamic_array_add(&Data->Identifier, C);
	return RttiParserState_EnumName;
}

rtti_parser_state rtti_parser_consume_name_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == ' ' || C == '\t' || C == '{' || C == '\n')
	{
		if (C == '{')
		{
			if (Data->Validate)
			{
				return start_parsing_fields(Engine, Data);
			}
			else
			{
				Engine->Platform->Log("Read Identifier %s, but struct keyword is missing\n", Data->Identifier);
				return RttiParserState_Error;
			}
		}
		else
		{
			char *Token = Data->Identifier;
			string_strip(Token, dynamic_array_len(Data->Identifier));
			if (string_compare(Token, "typedef"))
			{
				//skip and clear identifier
				dynamic_array_clear(Data->Identifier);
				return RttiParserState_Name;
			}
			else if (string_compare(Token, "struct"))
			{
				//clear, we are ok
				dynamic_array_clear(Data->Identifier);
				Data->Validate = 1;
				return RttiParserState_Name;
			}
			else if (string_compare(Token, "enum"))
			{
				//clear, we are ok
				dynamic_array_clear(Data->Identifier);
				Data->Validate = 1;
				return RttiParserState_EnumName;
			}
			else if (Data->Validate)
			{
				return start_parsing_fields(Engine, Data);
			}
			else
			{
				Engine->Platform->Log("Read Identifier %s, but struct keyword is missing\n", Data->Identifier);
				return RttiParserState_Error;
			}
		}
	}
	else if (C == '/')
	{
		Data->PrevState = RttiParserState_Name;
		return RttiParserState_PossibleCommentStart;
	}
	else
	{
		dynamic_array_add(&Data->Identifier, C);
		return RttiParserState_Name;
	}
}

rtti_parser_state rtti_parser_consume_enum_fields_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == ',')
	{
		char *Name = Data->Identifier;
		string_strip(Name, dynamic_array_len(Data->Identifier));
		rtti_enum_field F = {string_dup(Engine, Name), Data->CurrentEnumValue};
		++Data->CurrentEnumValue;
		dynamic_array_add(&Data->CurrentEnum.Fields, F);
		dynamic_array_clear(Data->Identifier);
		return RttiParserState_EnumFields;
	}
	else if (C == '}')
	{
		dynamic_array_add(&Engine->Rtti->Enums, Data->CurrentEnum);
		Data->CurrentEnum = RttiEnum(Engine);
		Data->CurrentEnumValue = 0;
		dynamic_array_clear(Data->Identifier);
		return RttiParserState_Parsing;
	}
	else if (C == '/')
	{
		Data->PrevState = RttiParserState_EnumFields;
		return RttiParserState_PossibleCommentStart;
	}

	dynamic_array_add(&Data->Identifier, C);
	return RttiParserState_EnumFields;
}

static b32 rtti_parse_field(engine *Engine, rtti_parser_data *Data)
{
	char *Line = Data->Identifier;
	u32 Len = dynamic_array_len(Data->Identifier);
	string_strip(Line, Len);
	b32 ParsingType = 1;
	Len = string_len(Line);
	char *Type = 0;
	char *Name = 0;

	for (u32 i=0; i<Len; ++i)
	{
		if (ParsingType)
		{
			char L = Line[i];
			if (!string_is_alpha(L))
			{
				Type = string_substr(Engine, Line, 0, i);
				ParsingType = 0;
			}
		}
		else
		{
			char L = Line[i];
			if (string_is_alpha(L))
			{
				Name = string_substr(Engine, Line, i, Len);	
				break;
			}
		}	
	}

	if (Name == 0 || Type == 0)
	{
		return 0;
	}

	s32 Index = string_index_of(Name, '*');
	if (Index >= 0)
	{
		Data->CurrentField.IsPointer = 1;
		Name[Index] = ' ';
		string_strip(Name, string_len(Name));
	}
	else
	{
		Index = string_index_of(Type, '*');
		if (Index >= 0)
		{
			Data->CurrentField.IsPointer = 1;
			Type[Index] = 0;
			string_strip(Type, string_len(Type));
		}
	}

	//static array?
	Index = string_index_of(Name, '[');
	if (Index > 0)
	{
		Name[string_index_of(Name, ']')] = 0;
		Data->CurrentField.Count = atoi(Name + Index + 1);
		Name[Index] = 0;
		string_strip(Name, string_len(Name));
	}

	Data->CurrentField.Name = Name;
	Data->CurrentField.TypeAsString = Type;
	dynamic_array_clear(Data->Identifier);
	dynamic_array_add(&Data->CurrentRecord.Fields, Data->CurrentField);
	Data->CurrentField = RttiField();

	return 1;
}

rtti_parser_state rtti_parser_consume_record_fields_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == ';')
	{
		if (rtti_parse_field(Engine, Data))
			return RttiParserState_RecordFields;
		return RttiParserState_Error;
	}
	else if (C == '{')
	{
		//Checking for anonymous unions. 
		char *Line = Data->Identifier;
		u32 Len = dynamic_array_len(Data->Identifier);
		string_strip(Line, Len);
		if (string_compare(Line, "union"))
		{
			dynamic_array_add(&Data->RecordsStack, Data->CurrentRecord);
			Data->CurrentRecord = RttiRecord(Engine);
			Data->CurrentRecord.IsUnion = 1;
			//Name? I assume anonymous here
			++Data->AnonymRecordId;
			char *UnionName = string_new(Engine, "%s.%d", Data->RecordsStack->Name, Data->AnonymRecordId);
			Data->CurrentRecord.Name = UnionName;
			dynamic_array_clear(Data->Identifier);
			return RttiParserState_UnionFields;	
		}
		//Discarding opening blocks. I should be counting them.
		//I can relax because the real compiler is doing the real checking. I assume this is well structed
		return RttiParserState_RecordFields;
	}
	else if (C == '}')
	{
		//Over with this object. Copy object to database and clean up current object
		//Back to general parsing (not doing ; checking)
		dynamic_array_add(&Engine->Rtti->Records, Data->CurrentRecord);
		Data->CurrentRecord = RttiRecord(Engine);
		return RttiParserState_Parsing;
	}
	else if (C == '/')
	{
		Data->PrevState = RttiParserState_RecordFields;
		return RttiParserState_PossibleCommentStart;
	}
	else
	{
		dynamic_array_add(&Data->Identifier, C);
		return RttiParserState_RecordFields;
	}
}

rtti_parser_state rtti_parser_consume_object_enum_params_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '\n')
	{
		dynamic_array_clear(Data->Identifier);
		return RttiParserState_Name;
	}
	else if (C == '/')
	{
		Data->PrevState = RttiParserState_ObjectEnumParams;
		return RttiParserState_PossibleCommentStart;
	}
	else
	{
		dynamic_array_add(&Data->Identifier, C);
	}

	return RttiParserState_ObjectEnumParams;
}
	
rtti_parser_state rtti_parser_consume_object_params_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '/')
	{
		Data->PrevState = RttiParserState_ObjectParams;
		return RttiParserState_PossibleCommentStart;
	}

	if (C == ')') 
	{
		char *Attributes = Data->Identifier;
		Attributes[dynamic_array_len(Data->Identifier)] = 0;
		string_strip(Attributes, dynamic_array_len(Data->Identifier));
		for (;;)
		{
			s32 IndexTo = string_index_of(Attributes, ',');
			if (IndexTo >= 0)
			{
				//TODO: This part of the code never got called...
				Attributes[IndexTo] = 0;
				string_strip(Attributes, IndexTo);
				if (string_compare("Editor", Attributes))
				{
					Data->CurrentRecord.Editor = 1; 
				}
				Attributes = Attributes + IndexTo + 1;
			}
			else
			{
				if (string_compare("Editor", Attributes))
				{
					Data->CurrentRecord.Editor = 1; 
				}
				break;
			}
		}
	}
	else if (C == '\n')
	{
		dynamic_array_clear(Data->Identifier);
		return RttiParserState_Name;
	}
	else
	{
		dynamic_array_add(&Data->Identifier, C);
	}

	return RttiParserState_ObjectParams;
}

rtti_parser_state rtti_parser_consume_possible_comment_start_state(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '/')
		return RttiParserState_OneLineComment;
	else if (C == '*')
		return RttiParserState_MultipleLinesComment;

	return Data->PrevState;
}

rtti_parser_state rtti_parser_consume_one_line_comment(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '\n')
		return Data->PrevState;

	return RttiParserState_OneLineComment;
}

rtti_parser_state rtti_parser_consume_multiple_line_comment(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '*')
		return RttiParserState_MultipleLinesCommentPossibleEnd;

	return RttiParserState_MultipleLinesComment;
}

rtti_parser_state rtti_parser_consume_multiple_line_comment_possible_end(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == '/')
	{
		return Data->PrevState;
	}

	return RttiParserState_MultipleLinesComment;
}

rtti_parser_state rtti_parser_consume_union_begin(engine *Engine, rtti_parser_data *Data, char C)
{
	//Unused? I left it here if we ever defined a named inner union
	return RttiParserState_Error;
}

rtti_parser_state rtti_parser_consume_union_end(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == ';')
	{
		char *FieldName = 0;
		u32 Len = dynamic_array_len(Data->Identifier);
		if (Len > 0)
		{
			FieldName = Data->Identifier;
			string_strip(FieldName, Len);
		}

		Data->CurrentField.TypeAsString = Data->CurrentRecord.Name;
		if (FieldName)
		{
			Data->CurrentField.Name = string_dup(Engine, FieldName);
		}

		dynamic_array_add(&Engine->Rtti->Records, Data->CurrentRecord);

		//Pop element
		u32 LastEl = dynamic_array_len(Data->RecordsStack)-1;
		Data->CurrentRecord = Data->RecordsStack[LastEl];
		dynamic_array_remove_at(Data->RecordsStack, LastEl);

		dynamic_array_add(&Data->CurrentRecord.Fields, Data->CurrentField);
		Data->CurrentField = RttiField();

		dynamic_array_clear(Data->Identifier);
		return RttiParserState_RecordFields;
	}

	dynamic_array_add(&Data->Identifier, C);
	return RttiParserState_UnionEnd;
}

rtti_parser_state rtti_parser_consume_union_fields(engine *Engine, rtti_parser_data *Data, char C)
{
	if (C == ';')
	{
		if (rtti_parse_field(Engine, Data))
			return RttiParserState_UnionFields;
		return RttiParserState_Error;
	}
	else if (C == '}')
	{
		dynamic_array_clear(Data->Identifier);
		return RttiParserState_UnionEnd;
	}

	dynamic_array_add(&Data->Identifier, C);
	return RttiParserState_UnionFields;
}

rtti_parser_consume_fn RttiParserStateTable[] = {rtti_parser_consume_parsing_state, 
	rtti_parser_consume_name_state, 
	rtti_parser_consume_enum_name_state, 
	rtti_parser_consume_record_fields_state, 
	rtti_parser_consume_enum_fields_state, 
	rtti_parser_consume_object_params_state, 
	rtti_parser_consume_object_enum_params_state, 
	rtti_parser_consume_end_object_state,
	rtti_parser_consume_possible_comment_start_state,
	rtti_parser_consume_one_line_comment,
	rtti_parser_consume_multiple_line_comment,
	rtti_parser_consume_multiple_line_comment_possible_end,
	rtti_parser_consume_union_begin,
	rtti_parser_consume_union_fields,
	rtti_parser_consume_union_end,
};

void rtti_parser_parse(engine *Engine, const char *FileName)
{
	Engine->Platform->Log("Parsing file %s\n", FileName);	

	u32 Size;
	char *Text = Engine->Platform->FileReadFully(FileName, &Size);
	if (Text == 0)
	{
		Engine->Platform->Log("Error reading file %s\n", FileName);	
		memory_free(&Engine->Memory, Text);
		return;
	}

	rtti_parser_data Data = {0};
	Data.CurrentRecord = RttiRecord(Engine);
	Data.CurrentEnum = RttiEnum(Engine);
	Data.Identifier = dynamic_array_create(Engine, char);
	Data.RecordsStack = dynamic_array_create(Engine, rtti_record);
	rtti_parser_state State = RttiParserState_Parsing;

	u32 FileHeader = 0;
	for(;;)
	{
		char C = Text[FileHeader++];
		State = RttiParserStateTable[State](Engine, &Data, C);

		if (State == RttiParserState_Error)
			break;

		if (C == 0)
			break;
	}

	dynamic_array_destroy(Data.Identifier);
	dynamic_array_destroy(Data.RecordsStack);
	memory_free(&Engine->Memory, Text);
}

static u32 calculate_memory_align(u32 Offset, u32 VariableSize)
{
	//TODO: use a bitmask solution for this? maybe?
	if (Offset % VariableSize)
	{
		return ((Offset/VariableSize) + 1) * VariableSize;
	}

	return Offset;
}

static u32 rtti_get_aligned_offset(engine *Engine, u32 Offset, rtti_field *Field)
{
	u32 TypeToSize[RFT_Max] = {0, 4, 4, 4, 1, 4, 4};

	assert(Field->Type < static_array_len(TypeToSize));
	u32 VariableSize = TypeToSize[Field->Type];

	if (Field->IsPointer)
	{
		VariableSize = sizeof(void*);
	}
	else if (Field->Type == RFT_Struct || Field->Type == RFT_Union)
	{
		rtti_record *R = rtti_record_get_by_name(Engine, Field->TypeAsString);
		return rtti_get_aligned_offset(Engine, Offset, R->Fields);
	}
	
	return calculate_memory_align(Offset, VariableSize);
}

static void rtti_set_field_size(engine *Engine, rtti_field *Field)
{
	typedef struct type_size
	{
		char *TypeName;
		rtti_field_type Type;
		u8 Size;
	} type_size;
	type_size TypeSizeTable[] = {"u8", RFT_Integer, sizeof(u8), "u16", RFT_Integer, sizeof(u16), "u32", RFT_Integer, sizeof(u32), "char", RFT_Char, sizeof(char),
		"s8", RFT_Integer, sizeof(s8), "s16", RFT_Integer, sizeof(s16), "s32", RFT_Integer, sizeof(s32), "b32", RFT_Boolean, sizeof(b32), "f32", RFT_Float, sizeof(f32), "f64", RFT_Float, sizeof(f64), "vec2", RFT_Vec, sizeof(f32)*2, "vec3", RFT_Vec, sizeof(f32)*3, "vec4", RFT_Vec, sizeof(f32)*4, "void", RFT_Unknown, sizeof(void*)};

	if (Field->IsPointer)
	{
		Field->Size = sizeof(void*) * Field->Count;
		//Set type as well

		for (u32 i=0; i<static_array_len(TypeSizeTable); ++i)
		{
			type_size *TypeSize = TypeSizeTable + i;
			if (string_compare(Field->TypeAsString, TypeSize->TypeName))
			{
				Field->Type = TypeSize->Type;

				//Check for char and convert it to string
				if (Field->Type == RFT_Char)
				{
					Field->Type = RFT_String;
				}

				return;
			}
		}
		//Search in records and unions
		for (u32 i=0; i<dynamic_array_len(Engine->Rtti->Records); ++i)
		{
			rtti_record *Obj = Engine->Rtti->Records + i;
			if (string_compare(Obj->Name, Field->TypeAsString))
			{
				if (Obj->IsUnion)
					Field->Type = RFT_Union;
				else
					Field->Type = RFT_Struct;
				return;
			}
		}
		//Maybe it's an enum?
		for (u32 i=0; i<dynamic_array_len(Engine->Rtti->Enums); ++i)
		{
			rtti_enum *Enum = Engine->Rtti->Enums + i;
			if (string_compare(Enum->Name, Field->TypeAsString))
			{
				Field->Type = RFT_Enum;
				return;
			}
		}

		//Type is unknown but it doesn't matter, it's a pointer.
		//It will matter if type is needed, though
		return;
	}

	for (u32 i=0; i<static_array_len(TypeSizeTable); ++i)
	{
		type_size *TypeSize = TypeSizeTable + i;
		if (string_compare(Field->TypeAsString, TypeSize->TypeName))
		{
			Field->Size = TypeSize->Size * Field->Count;
			Field->Type = TypeSize->Type;
			return;
		}
	}

	//not a basic type. search in object database
	for (u32 i=0; i<dynamic_array_len(Engine->Rtti->Records); ++i)
	{
		rtti_record *Obj = Engine->Rtti->Records + i;
		if (string_compare(Obj->Name, Field->TypeAsString))
		{
			u32 ObjSize = 0;
			if (Obj->IsUnion)
			{
				for (u32 f=0; f<dynamic_array_len(Obj->Fields); ++f)
				{
					rtti_field *Field = Obj->Fields + f;
					if (Field->Size)
					{
						if (ObjSize < Field->Size)
						{
							ObjSize = Field->Size;
						}
					}
					else
					{
						rtti_set_field_size(Engine, Field);
						if (ObjSize < Field->Size)
						{
							ObjSize = Field->Size;
						}
					}
				}

				Field->Type = RFT_Union;
				Field->Size = rtti_get_aligned_offset(Engine, ObjSize, Field);
			}
			else
			{
				for (u32 f=0; f<dynamic_array_len(Obj->Fields); ++f)
				{
					rtti_field *Field = Obj->Fields + f;
					if (Field->Size)
						ObjSize += Field->Size;
					else
					{
						rtti_set_field_size(Engine, Field);
						ObjSize += Field->Size;
					}
				}

				Field->Type = RFT_Struct;
				Field->Size = rtti_get_aligned_offset(Engine, ObjSize, Field);
			}
			return;
		}
	}

	//Maybe it's an enum?
	for (u32 i=0; i<dynamic_array_len(Engine->Rtti->Enums); ++i)
	{
		rtti_enum *Enum = Engine->Rtti->Enums + i;
		if (string_compare(Enum->Name, Field->TypeAsString))
		{
			Field->Size = sizeof(int);
			Field->Type = RFT_Enum;
			return;
		}
	}

	Engine->Platform->Log("%s object not found!\n", Field->Name);
}

static void rtti_parse_dir(engine *Engine, char *DirPath)
{
	char *Filter = string_new(Engine, "%s/*.h", DirPath);
	Engine->Platform->ListFileStart(Filter);
	for (;;) 
	{
		char *FileName = Engine->Platform->ListFileNext();
		if (FileName == 0)
			break;

		char *FullPath = string_new(Engine, "%s/%s", DirPath, FileName);
		rtti_parser_parse(Engine, FullPath);
		memory_free(&Engine->Memory, FullPath);
	} 

	string_delete(Engine, Filter);
}

void rtti_init(engine *Engine)
{
	Engine->Rtti = memory_alloc_type(&Engine->Memory, rtti);
	Engine->Rtti->Records = dynamic_array_create(Engine, rtti_record);
	Engine->Rtti->Enums = dynamic_array_create(Engine, rtti_enum);

	char *DbPath = string_new(Engine, "%s/rtti.db", Engine->AssetDatabase.RootPath);

#ifdef DEBUG
	rtti_parse_dir(Engine, "../../gbgengine");
	rtti_parse_dir(Engine, "../../source");

	for (u32 i=0; i<dynamic_array_len(Engine->Rtti->Records); ++i)
	{
		rtti_record *Obj = Engine->Rtti->Records + i;

		u32 Offset = 0;
		for (u32 e=0; e<dynamic_array_len(Obj->Fields); ++e)
		{
			rtti_field *Field = Obj->Fields + e;
			rtti_set_field_size(Engine, Field);
			Offset = rtti_get_aligned_offset(Engine, Offset, Field);
			Field->Offset = Offset;
			if (!Obj->IsUnion)
			{
				Offset += Field->Size;
			}
		}
	}

	//write database rtti
	file_handle FileHandle = Engine->Platform->FileCreate(DbPath);
	string_delete(Engine, DbPath);

	char Code[] = "RDB";
	u32 Written = Engine->Platform->FileWrite(FileHandle, Code, 3, 0);
	u8 Version = 1;
	Written += Engine->Platform->FileWrite(FileHandle, &Version, 1, Written);
	u32 RecordsLen = dynamic_array_len(Engine->Rtti->Records);
	Written += Engine->Platform->FileWrite(FileHandle, (u8*)&RecordsLen, 4, Written);
	for (u32 i=0; i<RecordsLen; ++i)
	{
		rtti_record *Rec = Engine->Rtti->Records + i;
		Written += write_string(Engine, FileHandle, Rec->Name, string_len(Rec->Name), Written);
		Written += Engine->Platform->FileWrite(FileHandle, (u8*)&Rec->Editor, 4, Written);
		Written += Engine->Platform->FileWrite(FileHandle, (u8*)&Rec->IsUnion, 4, Written);

		u32 FieldsLen = dynamic_array_len(Rec->Fields);
		Written += Engine->Platform->FileWrite(FileHandle, (u8*)&FieldsLen, 4, Written);
		for (u32 f=0; f<FieldsLen; ++f)
		{
			rtti_field *Field = Rec->Fields + f;
			Written += write_string(Engine, FileHandle, Field->Name, string_len(Field->Name), Written);
			Written += write_string(Engine, FileHandle, Field->TypeAsString, string_len(Field->TypeAsString), Written);
			Written += Engine->Platform->FileWrite(FileHandle, (u8*)&Field->Type, 1, Written);
			Written += Engine->Platform->FileWrite(FileHandle, (u8*)&Field->Count, 4, Written);
			Written += Engine->Platform->FileWrite(FileHandle, (u8*)&Field->IsPointer, 4, Written);
			Written += Engine->Platform->FileWrite(FileHandle, (u8*)&Field->Offset, 4, Written);
			Written += Engine->Platform->FileWrite(FileHandle, (u8*)&Field->Size, 4, Written);
		}
	}

	u32 EnumsLen = dynamic_array_len(Engine->Rtti->Enums);
	Written += Engine->Platform->FileWrite(FileHandle, (u8*)&EnumsLen, 4, Written);
	for (u32 i=0; i<EnumsLen; ++i)
	{
		rtti_enum *Enum = Engine->Rtti->Enums + i;
		Written += write_string(Engine, FileHandle, Enum->Name, string_len(Enum->Name), Written);

		u32 FieldsLen = dynamic_array_len(Enum->Fields);
		Written += Engine->Platform->FileWrite(FileHandle, (u8*)&FieldsLen, 4, Written);
		for (u32 f=0; f<FieldsLen; ++f)
		{
			rtti_enum_field *EnumField = Enum->Fields + f;
			Written += write_string(Engine, FileHandle, EnumField->Name, string_len(EnumField->Name), Written);
			Written += Engine->Platform->FileWrite(FileHandle, (u8*)&EnumField->Value, 1, Written);
		}
	}

	Engine->Platform->FileClose(FileHandle);
#else
	//read database rtti
	file_handle FileHandle = Engine->Platform->FileOpen(DbPath);
	string_delete(Engine, DbPath);
	char Code[3];
	u32 ReadFrom = Engine->Platform->FileRead(FileHandle, Code, sizeof(Code), 0);
	if (Code[0] == 'R' && Code[1] == 'D' && Code[2] == 'B')
	{
		u8 Version;
		ReadFrom += Engine->Platform->FileRead(FileHandle, &Version, 1, ReadFrom);
		if (Version == 1)
		{
			u32 RecLen;
			ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&RecLen, 4, ReadFrom);
			dynamic_array_grow(&Engine->Rtti->Records, RecLen);
			for (u32 r=0; r<RecLen; ++r)
			{
				rtti_record *Rec = Engine->Rtti->Records + r;
				ReadFrom += read_string(Engine, FileHandle, ReadFrom, &Rec->Name);
				ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&Rec->Editor, 4, ReadFrom);
				ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&Rec->IsUnion, 4, ReadFrom);

				u32 FieldsLen;
				ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&FieldsLen, 4, ReadFrom);
				Rec->Fields = dynamic_array_create(Engine, rtti_field);
				dynamic_array_grow(&Rec->Fields, FieldsLen);
				for (u32 f=0; f<FieldsLen; ++f)
				{
					rtti_field *Field = Rec->Fields + f;
					ReadFrom += read_string(Engine, FileHandle, ReadFrom, &Field->Name);
					ReadFrom += read_string(Engine, FileHandle, ReadFrom, &Field->TypeAsString);
					ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&Field->Type, 1, ReadFrom);
					ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&Field->Count, 4, ReadFrom);
					ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&Field->IsPointer, 4, ReadFrom);
					ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&Field->Offset, 4, ReadFrom);
					ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&Field->Size, 4, ReadFrom);
				}
			}

			u32 EnumsLen;
			ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&EnumsLen, 4, ReadFrom);
			dynamic_array_grow(&Engine->Rtti->Enums, EnumsLen);
			for (u32 i=0; i<EnumsLen; ++i)
			{
				rtti_enum *Enum = Engine->Rtti->Enums + i;
				ReadFrom += read_string(Engine, FileHandle, ReadFrom, &Enum->Name);

				u32 FieldsLen;
				ReadFrom += Engine->Platform->FileRead(FileHandle, (u8*)&FieldsLen, 4, ReadFrom);
				Enum->Fields = dynamic_array_create(Engine, rtti_enum_field);
				dynamic_array_grow(&Enum->Fields, FieldsLen);
				for (u32 f=0; f<FieldsLen; ++f)
				{
					rtti_enum_field *EnumField = Enum->Fields + f;
					ReadFrom += read_string(Engine, FileHandle, ReadFrom, &EnumField->Name);
					ReadFrom += Engine->Platform->FileRead(FileHandle, &EnumField->Value, 1, ReadFrom);
				}
			}
		}
	}
	Engine->Platform->FileClose(FileHandle);
#endif

	Engine->Rtti->CachedEntityRecord = rtti_record_get_by_name(Engine, "entity");
}

static void _rtti_field_get_max_field(engine *Engine, rtti_field *Field, b32 Recurse, rtti_field **MaxFieldOut, u32 *MaxFieldSizeOut)
{
	rtti_record *Record = rtti_record_get_by_name(Engine, Field->TypeAsString);

	u32 MaxFieldSize = 0;
	rtti_field *MaxField = 0;

	for (u32 i=0; i<dynamic_array_len(Record->Fields); ++i)
	{
		rtti_field *F = Record->Fields + i;

		u32 CurFSize = F->Size;
		rtti_field *CurField = F;

		if (F->IsPointer)
			CurFSize = sizeof(void*);
		else if (F->Type == RFT_Vec)
			CurFSize = sizeof(f32);
		else if (F->Type == RFT_Struct || F->Type == RFT_Union)
		{
			if (Recurse)
			{
				_rtti_field_get_max_field(Engine, F, Recurse, &CurField, &CurFSize);
			}
			else
			{
				CurFSize = F->Size;
			}
		}

		if (CurFSize > MaxFieldSize)
		{
			MaxFieldSize = CurFSize;
			MaxField = CurField;
		}
	}

	*MaxFieldOut = MaxField;
	*MaxFieldSizeOut = MaxFieldSize;
}

static u32 rtti_field_get_max_basic_type_size_recurse(engine *Engine, rtti_field *F)
{
	assert(F->Type == RFT_Struct || F->Type == RFT_Union);
	u32 Size;
	rtti_field *Field;
	_rtti_field_get_max_field(Engine, F, true, &Field, &Size);
	return Size;
}

rtti_field *rtti_union_get_max_field(engine *Engine, rtti_field *UnionField)
{
	assert(UnionField->Type == RFT_Union);

	u32 Size;
	rtti_field *Field;
	_rtti_field_get_max_field(Engine, UnionField, false, &Field, &Size);
	return Field;
}

u32 rtti_record_get_size(engine *Engine, rtti_record *Record)
{
	u32 LastIx = dynamic_array_len(Record->Fields);
	rtti_field *LastF = Record->Fields + LastIx - 1;

	u32 Size = LastF->Offset + LastF->Size;

	//Get Max field size, that's how struct size of the compiler is calculated
	u32 MaxFieldSize = 0;
	for (u32 i=0; i<LastIx; ++i)
	{
		rtti_field *F = Record->Fields + i;
		u32 CurFSize = F->Size;

		if (F->IsPointer)
			CurFSize = sizeof(void*);
		else if (F->Type == RFT_Vec)
			CurFSize = sizeof(f32);
		else if (F->Type == RFT_Struct || F->Type == RFT_Union)
		{
			CurFSize = rtti_field_get_max_basic_type_size_recurse(Engine, F);
		}

		if (CurFSize > MaxFieldSize)
			MaxFieldSize = CurFSize;
	}

	return calculate_memory_align(Size, MaxFieldSize);
}

rtti_record *rtti_record_get_by_name(engine *Engine, const char *RecordName)
{
	for (u32 i=0; i<dynamic_array_len(Engine->Rtti->Records); ++i)
	{
		rtti_record *Rec = Engine->Rtti->Records + i;
		if (string_compare(Rec->Name, RecordName))
		{
			return Rec;
		}
	}

	return 0;
}

static void rtti_field_copy(engine *Engine, rtti_field *Field, u8 *Source, u8 *Dest)
{
	if (string_compare(Field->TypeAsString, "named_pointer"))
	{
		named_pointer *NPSource = (named_pointer*)Source;
		named_pointer *NPDest = (named_pointer*)Dest;
		NPDest->RecordName = string_dup(Engine, NPSource->RecordName);
		NPDest->Ptr = rtti_duplicate(Engine, rtti_record_get_by_name(Engine, NPSource->RecordName), NPSource->Ptr);
	}
	else if (Field->Type == RFT_String)
	{
		char **S = (char**)Dest;
		char *SourceString = (char*)(*(char**)Source);
		*S = string_dup(Engine, SourceString);
	}
	else if (Field->Type == RFT_Struct)
	{
		rtti_record *SubRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);
		rtti_record_copy(Engine, SubRecord, Source, Dest);
	}
	else if (Field->Type == RFT_Union)
	{
		rtti_field *MaxField = rtti_union_get_max_field(Engine, Field);
		rtti_field_copy(Engine, MaxField, Source, Dest);
	}
	else if (Field->IsPointer)
	{
		//What to do here?
	}
	else
	{
		mem_copy(Source, Dest, Field->Size);
	}
}

void rtti_record_copy(engine *Engine, rtti_record *Record, void *Source, void *Dest)
{
	u8 *SObj = (u8*)Source;
	u8 *DObj = (u8*)Dest;

	for (u32 i=0; i<dynamic_array_len(Record->Fields); ++i)
	{
		rtti_field *Field = Record->Fields + i;
		u8 *DestFieldObj = DObj + Field->Offset;
		u8 *SourceFieldObj = SObj + Field->Offset;
		
		rtti_field_copy(Engine, Field, SourceFieldObj, DestFieldObj);
	}
}

void *rtti_duplicate(struct engine *Engine, rtti_record *Record, void *DefaultData)
{
	u32 RecSize = rtti_record_get_size(Engine, Record);
	void *Object = memory_alloc(&Engine->Memory, RecSize);
	memset(Object, 0, RecSize);

	rtti_record_copy(Engine, Record, DefaultData, Object);

	return Object;
}
