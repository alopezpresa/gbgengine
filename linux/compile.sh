if [ "$1" = "" ] || [ "$1" = "debug" ]
then
	OutputDir="debug"
	CompilerFlags="-DDEBUG -DPLAT_LINUX -g -I../../gbgengine"
fi

if [ "$1" = "release" ]
then
	OutputDir="release"
	CompilerFlags="-O2 -DPLAT_LINUX -I../../gbgengine"
fi

echo "param $OutputDir"

if [ "$OutputDir" != "debug" ] && [ "$OutputDir" != "release" ]
then
	echo "Invalid parameter $1"
	echo "Valid params 'debug' or 'release'"
	exit 1
fi

mkdir $OutputDir
cd $OutputDir

gcc $CompilerFlags ../../gbgengine/linux/linux.c -o puyo -lX11 -lGL -ldl -lm -lpulse -lpulse-simple -lpthread

touch lock
gcc $CompilerFlags ../../source/game.c -fPIC -shared -Wl,-soname,libgame.so -o libgame.so -lGL -lm 
rm lock

if [ "$1" = "release" ]
then
	cp -r ../../assets .
fi

cd -

