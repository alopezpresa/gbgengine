#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/inotify.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include <limits.h>
#include <linux/joystick.h>
#include <linux/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <netdb.h>
#include <pwd.h>

#include "gbg_render.h"
#include "gbg_engine.h"
#include "gbg_platform.h"
#include "gbg_input.h"
#include "gbg_net.h"

//NOTE: glx will pull some opengl from this platform. It should be included AFTER gbg_render.h
#include <GL/glx.h>
#include "third_party/glcorearb.h"
#include "third_party/glxext.h"

#include "gbg_assets.c"
#include "gbg_memory.c"
#include "gbg_engine.c"
#include "gbg_input.c"
#include "gbg_math.c"
#undef Success //Fuck X11.h
#include "gbg_render.c"
#include "gbg_string.c"
#include "gbg_sound.c"
#include "gbg_utils.c"
#include "gbg_imgui.c"
#include "gbg_net.c"
#include "gbg_world.c"

//stolen from xinput documentation
#define XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE  7849
#define XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE 8689
#define XINPUT_GAMEPAD_TRIGGER_THRESHOLD    30

typedef void (*game_update_func)(engine*, f32);
game_update_func game_update = 0;

// Helper to check for extension string presence.  Adapted from:
//   http://www.opengl.org/resources/features/OGLextensions/
static bool isExtensionSupported(const char *extList, const char *extension)
{
	const char *start;
	const char *where, *terminator;

	/* Extension names should not have spaces. */
	where = strchr(extension, ' ');
	if (where || *extension == '\0')
		return false;

	/* It takes a bit of care to be fool-proof about parsing the
	   OpenGL extensions string. Don't be fooled by sub-strings,
	   etc. */
	for (start=extList;;) {
		where = strstr(start, extension);

		if (!where)
			break;

		terminator = where + strlen(extension);

		if ( where == start || *(where - 1) == ' ' )
			if ( *terminator == ' ' || *terminator == '\0' )
				return true;

		start = terminator;
	}

	return false;
}

static bool ctxErrorOccurred = false;
static int ctxErrorHandler( Display *dpy, XErrorEvent *ev )
{
	ctxErrorOccurred = true;
	return 0;
}

static engine Engine = {};

static void platform_get_user_name(char *UserName, u32 Len)
{
	getlogin_r(UserName, Len);
}

static void platform_get_ipv4_address(char *Buffer, u32 BufferLen)
{
	int Sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (Sock < 0)
	{
		printf("Error creating socket %d\n", Sock);
		return;
	}

	struct ifreq IfReq;
	IfReq.ifr_addr.sa_family = AF_INET;

	//Iface 1 is loopback, take the second one
	IfReq.ifr_ifindex = 2;
	ioctl(Sock, SIOCGIFNAME, &IfReq);

	ioctl(Sock, SIOCGIFADDR, &IfReq);

	char *Addr = inet_ntop(AF_INET, &((struct sockaddr_in *)&IfReq.ifr_addr)->sin_addr, Buffer, BufferLen);
	if (Addr == 0)
		Buffer = 0;

	close(Sock);
}

static b32 create_socket_and_bind(const char *Host, const char *Port)
{
	int Sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (Sock < 0)
	{
		printf("Error creating socket %d\n", Sock);
		return 0;
	}

	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		printf("error on getaddrinfo %d\n", Err);
		close(Sock);
		return 0;
	}

	Engine.Net->Socket.Descriptor = Sock;

	char LHost[NI_MAXHOST], Service[NI_MAXSERV];
	getnameinfo(AddrInfo->ai_addr, AddrInfo->ai_addrlen, LHost, NI_MAXHOST, Service, NI_MAXSERV, NI_NUMERICSERV);

    Err = bind(Sock, AddrInfo->ai_addr, AddrInfo->ai_addrlen);

	freeaddrinfo(AddrInfo);
	return 1;
}

static void platform_client_exit()
{
	memory_free(&Engine.Memory, Engine.Net->Host.Address);	
	memory_free(&Engine.Memory, Engine.Net->Host.Port);	
	shutdown(Engine.Net->Socket.Descriptor, SHUT_RD);
	close(Engine.Net->Socket.Descriptor);
	Engine.Net->Socket.Descriptor = 0;
}

static b32 platform_client_join(const char *Host, const char *Port)
{
	//TODO: Check all paths will free this memory up
	Engine.Net->Host.Address = string_dup(&Engine, Host);
	Engine.Net->Host.Port = string_dup(&Engine, Port);

	char LocalHostIp[64];
	platform_get_ipv4_address(LocalHostIp, static_array_len(LocalHostIp));
	b32 Ret = create_socket_and_bind(LocalHostIp, Port);

	return Ret;
}

static s32 platform_socket_write(file_handle Handle, u8 *Data, u32 DataLen, const char *Host, const char *Port)
{
	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		printf("error on getaddrinfo %d. Can't send message\n", Err);
		return 0;
	}

	int Bytes = sendto(Handle.Descriptor, Data, DataLen, 0, AddrInfo->ai_addr, AddrInfo->ai_addrlen);
	freeaddrinfo(AddrInfo);

	return Bytes;
}

static s32 platform_socket_read(file_handle Handle, u8 *Data, u32 DataLen, char *OutHost, char *OutPort)
{
	struct sockaddr SockAddr;
	socklen_t SockAddrLen = sizeof(SockAddr);	

	int Bytes = recvfrom(Handle.Descriptor, Data, DataLen, 0, &SockAddr, &SockAddrLen);

	if (OutHost != 0 && OutPort != 0)
	{
		getnameinfo(&SockAddr, SockAddrLen, OutHost, NI_MAXHOST, OutPort, NI_MAXSERV, NI_NUMERICSERV | NI_NUMERICHOST);
	}

	if (Bytes < 0)
	{
		shutdown(Handle.Descriptor, SHUT_RD);
		close(Handle.Descriptor);
	}

	return Bytes;
}

static u16 platform_net_to_host_16(u16 NetShort)
{
	return ntohs(NetShort);
}

static u32 platform_net_to_host_32(u32 NetInt)
{
	return ntohl(NetInt);
}

static u16 platform_host_to_net_16(u16 HostShort)
{
	return htons(HostShort);
}

static u32 platform_host_to_net_32(u32 HostInt)
{
	return htonl(HostInt);
}

static b32 platform_server_start(const char *Host, const char *Port)
{
	return create_socket_and_bind(Host, Port);
}

static void platform_server_stop()
{
	int fd = Engine.Net->Socket.Descriptor;
	shutdown(fd, SHUT_RDWR);
	close(fd);
	Engine.Net->Socket.Descriptor = 0;
}

static void platform_create_thread(void *(*Func)(void *Param), void *Param)
{
	pthread_t ServerThreadId;
	pthread_create(&ServerThreadId, 0, Func, Param);
}

static void platform_exit_thread()
{
	pthread_exit(0);
}

static void platform_mutex(b32 *Lock)
{
	while (__sync_val_compare_and_swap(Lock, 0, 1))
		;
}

static file_handle platform_file_open(const char *FileName)
{
	file_handle Ret;
	Ret.Descriptor = open(FileName, O_RDWR);
	return Ret;
}

static u32 platform_file_read(file_handle FileHandle, u8 *Buffer, u32 BufferLen, u32 From)
{
	return pread(FileHandle.Descriptor, Buffer, BufferLen, From);
}

static void platform_file_close(file_handle FileHandle)
{
	close(FileHandle.Descriptor);
}

u8* platform_file_read_fully(const char *FullPath, u32 *Size)
{
	u8 *Buffer = 0;
	*Size = 0;
	int fd = open(FullPath, O_RDONLY);
	if (fd >= 0)
	{
		struct stat FileStat;
		fstat(fd, &FileStat);
		*Size = FileStat.st_size;
		Buffer = memory_alloc(&Engine.Memory, FileStat.st_size);
		u32 ReadSize = read(fd, Buffer, FileStat.st_size);
		if (ReadSize != *Size)
		{
			close(fd);
			return 0;
		}
		close(fd);
	}
	else
	{
		perror("Read fully failed");
	}

	return Buffer;
}

void platform_file_write_fully(const char *FullPath, u8 *Content, u32 Size)
{
	int fd = open(FullPath, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
	if (fd >= 0)
	{
		write(fd, Content, Size);
		close(fd);
	}
	else
	{
		perror("Write fully failed");
	}
}

void *platform_load_gl_function(const char *Name)
{
	void *FnPtr = glXGetProcAddress(Name);
	return FnPtr;
}

void platform_log(const char *Fmt, ...)
{
	va_list args;
	va_start(args, Fmt);

	vdprintf(STDOUT_FILENO, Fmt, args);
}

static Display *display = 0;
static Window window = 0;
static void platform_set_window_title(const char *Title)
{
	XStoreName(display, window, Title);

	XClassHint *ClassHint = XAllocClassHint();

	ClassHint->res_name = strdup(Title);
	ClassHint->res_class = ClassHint->res_name;
	XSetClassHint(display, window, ClassHint);
	XFree(ClassHint);
}

static u64 platform_get_file_modified_date(const char *Name)
{
	struct stat FileStat;
	if (stat(Name, &FileStat) == 0)
		return FileStat.st_mtim.tv_sec;

	return 0;
}

static GLXContext ctx = 0;
static Colormap cmap;
static float UserAspectRatio = 0.75f;

void platform_set_window_size(u32 Width, u32 Height)
{
	UserAspectRatio = Height/(float)Width;
	XResizeWindow(display, window, Width, Height);
}

void platform_hide_cursor()
{
	char data[1] = {};
	XColor color;
	color.red = color.green = color.blue = 0;
	Pixmap pixmap = XCreateBitmapFromData(display, window, data, 1, 1);
	Cursor EmptyC = XCreatePixmapCursor(display, pixmap, pixmap, &color, &color, 0, 0);
	XFreePixmap(display, pixmap);
	XDefineCursor(display, window, EmptyC);
}

f64 platform_get_local_time()
{
	struct timeval Time;
	gettimeofday(&Time, 0);

	return ((Time.tv_sec*1000.0) + (Time.tv_usec/1000.0))/1000.0f;
}

void mouse_set_button_state(mouse_state *MouseState, int Button, button_state State)
{
	if (Button == 1)
		MouseState->LeftButton.CurState = State;
	else if (Button == 3)
		MouseState->RightButton.CurState = State;
}

#define GameSoPath "./libgame.so"
#define GameSoTempPath "./libgametemp.so"
static struct stat GameSoStatOrig;
static void *GameSo = 0;

void load_game_func()
{
	if (GameSo)
	{
		dlclose(GameSo);
		GameSo = 0;
		game_update = 0;
	}

	int TempFd = creat(GameSoTempPath, S_IRWXU);
	if (TempFd == -1)
	{
		printf("Error creating " GameSoTempPath  "\n");
		return;
	}

	int Fd = open(GameSoPath, O_RDONLY);
	if (Fd == -1)
	{
		printf("Error opening " GameSoPath  " for copying\n");
		return;
	}

	char Buffer[4096];
	ssize_t ReadSize;
	while ((ReadSize = read(Fd, Buffer, 4096)) > 0)
	{
		write(TempFd, Buffer, ReadSize);
	}
	close(TempFd);
	close(Fd);

	stat(GameSoTempPath, &GameSoStatOrig);
	GameSo = dlopen(GameSoTempPath, RTLD_LAZY|RTLD_LOCAL);
	if (GameSo == 0)
	{
		printf("Error opening libgame.so\n");
		return;
	}

	game_update = dlsym(GameSo, "game_update");
	if (game_update == 0)
	{
		printf("Error loading fun game_update\n");
		return;
	}
}

#include <pulse/pulseaudio.h>
#include <pulse/volume.h>

static struct pa_mainloop *PAMainLoop = 0;
static struct pa_stream *PlayStream = 0;
static struct pa_context *PAContext = 0;

void pa_state_cb(pa_context *Context, void *UserData)
{
	int* UserState = (int*)UserData;
	pa_context_state_t ContextState = pa_context_get_state(Context);
	switch(ContextState)
	{
		case PA_CONTEXT_READY:
			*UserState = 1;
			break;
		case PA_CONTEXT_FAILED:
			*UserState = -1;
			break;
	}
}

static void stream_request_cb(pa_stream *s, size_t Length, void *userdata) 
{
	char StreamBuffer[Length];
	sound_request_buffer_mix(&Engine, StreamBuffer, Length);

	pa_stream_write(s, StreamBuffer, Length, 0, 0LL, PA_SEEK_RELATIVE);
}

pa_sample_spec pulseaudio_get_sample_spec()
{
	pa_sample_spec ss = {};
	ss.rate = 44100;
	ss.channels = 2;
	ss.format = PA_SAMPLE_S16LE;

	return ss;
}

pa_buffer_attr pulseaudio_create_bufferattr(pa_usec_t Usec, pa_sample_spec *Spec)
{
	pa_buffer_attr Bufferattr = {};
	Bufferattr.fragsize = -1;
	Bufferattr.maxlength = -1;
	Bufferattr.minreq = -1;
	Bufferattr.prebuf = (uint32_t)-1;
	Bufferattr.tlength = pa_usec_to_bytes(40000, Spec);	

	return Bufferattr;
}

void underflow_cb(pa_stream *s, void *UserData)
{
	pa_usec_t Usec;
	if (pa_stream_get_latency(s, &Usec, 0) == 0)
	{
		pa_sample_spec Spec = pulseaudio_get_sample_spec();
		//Increase a 10% latency?
		Usec += Usec/10.0f;
		pa_buffer_attr Bufattr = pulseaudio_create_bufferattr(Usec, &Spec);
		pa_stream_set_buffer_attr(s, &Bufattr, 0, 0);
	}
}

void overflow_cb(pa_stream *s, void *UserData)
{
	printf("overflow\n");
}

void init_sound()
{
	int StateReady = 0;
	// Create a mainloop API and connection to the default server
	PAMainLoop = pa_mainloop_new();
	struct pa_mainloop_api *PAMainLoopApi = pa_mainloop_get_api(PAMainLoop);

	PAContext = pa_context_new(PAMainLoopApi, "Missile command");
	if (PAContext == 0)
	{
		printf("Error creating pulseaudio context\n");
		return;
	}
	pa_context_connect(PAContext, NULL, 0, NULL);
	pa_context_set_state_callback(PAContext, pa_state_cb, &StateReady);

	while (StateReady == 0)
	{
		pa_mainloop_iterate(PAMainLoop, 1, 0);
	}

	if (StateReady == -1)
	{
		printf("Error connecting to pulse audio \n");
		return;
	}

	printf("-> Connection to pulse audio success\n");

	pa_sample_spec Spec = pulseaudio_get_sample_spec();
	PlayStream = pa_stream_new(PAContext, "Playback", &Spec, 0);
	if (PlayStream == 0)
	{
		printf("Error creating play stream\n");
		return;
	}

	pa_stream_set_underflow_callback(PlayStream, underflow_cb, 0);
	pa_stream_set_overflow_callback(PlayStream, overflow_cb, 0);
	pa_stream_set_write_callback(PlayStream, stream_request_cb, 0);

	pa_buffer_attr Bufferattr = pulseaudio_create_bufferattr(40000, &Spec);
	
	int r = pa_stream_connect_playback(PlayStream, NULL, &Bufferattr,
			PA_STREAM_INTERPOLATE_TIMING
			|PA_STREAM_ADJUST_LATENCY
			|PA_STREAM_AUTO_TIMING_UPDATE, NULL, NULL);
	if (r < 0) {
		// Old pulse audio servers don't like the ADJUST_LATENCY flag, so retry without that
		r = pa_stream_connect_playback(PlayStream, NULL, &Bufferattr,
				PA_STREAM_INTERPOLATE_TIMING|
				PA_STREAM_AUTO_TIMING_UPDATE, NULL, NULL);
	}
	if (r < 0) {
		printf("pa_stream_connect_playback failed\n");
		return;
	}
}

static void *sound_thread(void *Param)
{
	engine *LocalEng = (engine*)Param;
	while (1)
	{
		int Quit = 0;
		int itererr = pa_mainloop_iterate(PAMainLoop, 0, &Quit);

		if (Quit)
			pthread_exit(0);

		if (itererr < 0)
		{
			printf("error on iterate\n");
		}

		sound_update(LocalEng);
		usleep(2000);
	}
}

static int x_error_handler(Display *D, XErrorEvent *E)
{
	E->error_code += 0;
}

void init_x()
{
	display = XOpenDisplay(NULL);

	XSetErrorHandler(x_error_handler);

	if (!display)
	{
		printf("Failed to open X display\n");
		exit(1);
	}

	// Get a matching FB config
	static int visual_attribs[] =
	{
		GLX_X_RENDERABLE    , True,
		GLX_DRAWABLE_TYPE   , GLX_WINDOW_BIT,
		GLX_RENDER_TYPE     , GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE   , GLX_TRUE_COLOR,
		GLX_RED_SIZE        , 8,
		GLX_GREEN_SIZE      , 8,
		GLX_BLUE_SIZE       , 8,
		GLX_ALPHA_SIZE      , 8,
		GLX_DEPTH_SIZE      , 24,
		GLX_STENCIL_SIZE    , 8,
		GLX_DOUBLEBUFFER    , True,
		//GLX_SAMPLE_BUFFERS  , 1,
		//GLX_SAMPLES         , 4,
		None
	};

	int glx_major, glx_minor;

	// FBConfigs were added in GLX version 1.3.
	if ( !glXQueryVersion( display, &glx_major, &glx_minor ) || 
			( ( glx_major == 1 ) && ( glx_minor < 3 ) ) || ( glx_major < 1 ) )
	{
		printf("Invalid GLX version");
		exit(1);
	}

	printf("glx version %d.%d\n", glx_major, glx_minor);
	int fbcount;
	GLXFBConfig* fbc = glXChooseFBConfig(display, DefaultScreen(display), visual_attribs, &fbcount);
	if (!fbc)
	{
		printf( "Failed to retrieve a framebuffer config\n" );
		exit(1);
	}

	// Pick the FB config/visual with the most samples per pixel
	int best_fbc = -1, worst_fbc = -1, best_num_samp = -1, worst_num_samp = 999;

	int i;
	for (i=0; i<fbcount; ++i)
	{
		XVisualInfo *vi = glXGetVisualFromFBConfig( display, fbc[i] );
		if ( vi )
		{
			int samp_buf, samples;
			glXGetFBConfigAttrib( display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf );
			glXGetFBConfigAttrib( display, fbc[i], GLX_SAMPLES       , &samples  );

			if ( best_fbc < 0 || samp_buf && samples > best_num_samp )
				best_fbc = i, best_num_samp = samples;
			if ( worst_fbc < 0 || !samp_buf || samples < worst_num_samp )
				worst_fbc = i, worst_num_samp = samples;
		}
		XFree( vi );
	}

	GLXFBConfig bestFbc = fbc[ best_fbc ];

	// Be sure to free the FBConfig list allocated by glXChooseFBConfig()
	XFree( fbc );

	// Get a visual
	XVisualInfo *vi = glXGetVisualFromFBConfig( display, bestFbc );

	XSetWindowAttributes swa;
	swa.colormap = cmap = XCreateColormap( display,
			RootWindow( display, vi->screen ), 
			vi->visual, AllocNone );
	swa.background_pixmap = None ;
	swa.border_pixel      = 0;
	swa.event_mask        = StructureNotifyMask | ExposureMask | StructureNotifyMask | PointerMotionMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask;

	window = XCreateWindow( display, RootWindow( display, vi->screen ), 
			0, 0, 800, 600, 0, vi->depth, InputOutput, 
			vi->visual, 
			CWBorderPixel|CWColormap|CWEventMask, &swa );
	if ( !window )
	{
		printf( "Failed to create window.\n" );
		exit(1);
	}

	// Done with the visual info data
	XFree( vi );

	XMapWindow( display, window );

	// Get the default screen's GLX extension list
	const char *glxExts = glXQueryExtensionsString( display, DefaultScreen( display ) );

	// NOTE: It is not necessary to create or make current to a context before
	// calling glXGetProcAddressARB
	PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB = 0;
	glXCreateContextAttribsARB = (PFNGLXCREATECONTEXTATTRIBSARBPROC) glXGetProcAddressARB( (const GLubyte *) "glXCreateContextAttribsARB" );

	PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT = 0;

	// Install an X error handler so the application won't exit if GL 3.0
	// context allocation fails.
	//
	// Note this error handler is global.  All display connections in all threads
	// of a process use the same error handler, so be sure to guard against other
	// threads issuing X commands while this code is running.
	ctxErrorOccurred = false;
	int (*oldHandler)(Display*, XErrorEvent*) = XSetErrorHandler(&ctxErrorHandler);

	// Check for the GLX_ARB_create_context extension string and the function.
	// If either is not present, use GLX 1.3 context creation method.
	if ( !isExtensionSupported( glxExts, "GLX_ARB_create_context" ) || !glXCreateContextAttribsARB )
	{
		printf( "glXCreateContextAttribsARB() not found" " ... using old-style GLX context\n" );
		ctx = glXCreateNewContext( display, bestFbc, GLX_RGBA_TYPE, 0, True );
	}

	// If it does, try to get a GL 3.0 context!
	else
	{
		int context_attribs[] =
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
			GLX_CONTEXT_MINOR_VERSION_ARB, 2,
			//GLX_CONTEXT_FLAGS_ARB        , GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
			None
		};

		ctx = glXCreateContextAttribsARB( display, bestFbc, 0, True, context_attribs );

		// Sync to ensure any errors generated are processed.
		XSync( display, False );
		if ( !ctxErrorOccurred && ctx )
		{
			printf( "Created GL 3.0 context\n" );
		}
		else
		{
			// Couldn't create GL 3.0 context.  Fall back to old-style 2.x context.
			// When a context version below 3.0 is requested, implementations will
			// return the newest context version compatible with OpenGL versions less
			// than version 3.0.
			// GLX_CONTEXT_MAJOR_VERSION_ARB = 1
			context_attribs[1] = 1;
			// GLX_CONTEXT_MINOR_VERSION_ARB = 0
			context_attribs[3] = 0;

			ctxErrorOccurred = false;

			printf( "Failed to create GL 3.0 context"
					" ... using old-style GLX context\n" );
			ctx = glXCreateContextAttribsARB( display, bestFbc, 0, True, context_attribs );
		}
	}

	// Sync to ensure any errors generated are processed.
	XSync( display, False );

	// Restore the original error handler
	XSetErrorHandler( oldHandler );

	if ( ctxErrorOccurred || !ctx )
	{
		printf( "Failed to create an OpenGL context\n" );
		exit(1);
	}

	// Verifying that context is a direct context
	if ( ! glXIsDirect ( display, ctx ) )
	{
		printf( "Indirect GLX rendering context obtained. Performance may be slow\n" );
	}

	glXMakeCurrent( display, window, ctx );

	if (isExtensionSupported(glxExts, "GLX_EXT_swap_control" ))
	{
		glXSwapIntervalEXT = (PFNGLXSWAPINTERVALEXTPROC)glXGetProcAddress("glXSwapIntervalEXT");
		glXSwapIntervalEXT(display, glXGetCurrentDrawable(), 1);
	}
}

struct joy_info
{
	//Number after js
	int Id;
	int Fd;
};
static input Input = {};
static struct joy_info JoystickFd[GG_GAMEPAD_DEVICES] = {};
static int JoyInotifyFd;

#define JOY_FILE "/dev/input/js%d"
void gamepad_init()
{
	char JoyPath[sizeof(JOY_FILE)];
	for (int g=0; g<GG_GAMEPAD_DEVICES; ++g)
	{
        struct joy_info *JoyInfo = JoystickFd + g;
		snprintf(JoyPath, sizeof(JOY_FILE), JOY_FILE, g);
        JoyInfo->Fd = open(JoyPath, O_NONBLOCK | O_RDONLY);
		if (JoyInfo->Fd > 0)
			JoyInfo->Id = g;
		else
			JoyInfo->Id = -1;
	}

	JoyInotifyFd = inotify_init1(IN_NONBLOCK | IN_CLOEXEC);
	if (JoyInotifyFd == -1)
	{
		platform_log("ERROR: Joy inotify couldn't be created\n");
		return;
	}

	int WatchId = inotify_add_watch(JoyInotifyFd, "/dev/input", IN_CREATE | IN_DELETE | IN_ATTRIB);
	if (WatchId == -1)
		platform_log("ERROR: inotify watch couldn't be created\n");
}

void handle_gamepad()
{
	if (JoyInotifyFd > 0)
	{
		char EventBuffer[4096];
		struct inotify_event *Event = 0;
		int r = read(JoyInotifyFd, EventBuffer, sizeof(EventBuffer));
		int Size = 0;
		Event = (struct inotify_event*)EventBuffer;
		while (r > 0)
		{
			if (Event->mask & (IN_CREATE | IN_ATTRIB))
			{
				if (strstr(Event->name, "js"))
				{
					int Id;
					sscanf(Event->name, "js%d", &Id);
					for (int g=0; g<GG_GAMEPAD_DEVICES; ++g)
					{
						if (JoystickFd[g].Id == Id) //Already handled
							break;

						if (JoystickFd[g].Fd == -1)
						{
							char Path[128];
							sprintf(Path, "/dev/input/%s", Event->name);
							int Fd = open(Path, O_NONBLOCK | O_RDONLY);
							if (Fd > 0)
							{
								JoystickFd[g].Id = Id;
								JoystickFd[g].Fd = Fd;
							}
							break;
						}
					}
				}
			}
			if (Event->mask & IN_DELETE)
			{
				if (strstr(Event->name, "js"))
				{
					int Id;
					sscanf(Event->name, "js%d", &Id);
					for (int g=0; g<GG_GAMEPAD_DEVICES; ++g)
					{
						if (JoystickFd[g].Id == Id)
						{
							JoystickFd[g].Fd = -1;
							JoystickFd[g].Id = -1;
							break;
						}
					}
				}
			}

			int RealLen = sizeof(struct inotify_event) + Event->len;
			Size += RealLen;
			r -= RealLen;
			Event = (struct inotify_event*)(EventBuffer + Size);
		}
	}

	char JoyPath[sizeof(JOY_FILE)];

	for (int g=0; g<GG_GAMEPAD_DEVICES; ++g)
	{
        struct joy_info *Joy = JoystickFd + g;

		gamepad *Gamepad = Input.Gamepads + g;
		Gamepad->IsConnected = Joy->Fd > 0;

		if (Joy->Fd == -1)
		{
			continue;
		}

		vec2 LeftThumb = {};
		vec2 RightThumb = {};
		while (1)
		{
			struct js_event JsEvent;
			int r = read(Joy->Fd, &JsEvent, sizeof(struct js_event));

			if (r > 0)
			{
				JsEvent.type &= ~JS_EVENT_INIT; 
				if (JsEvent.type == 1)
				{
					if (JsEvent.number == 0)
					{
						Gamepad->Buttons[Gamepad_A].CurState = JsEvent.value;
					}
					else if (JsEvent.number == 1)
					{
						Gamepad->Buttons[Gamepad_B].CurState = JsEvent.value;
					}
					else if (JsEvent.number == 2)
					{
						Gamepad->Buttons[Gamepad_X].CurState = JsEvent.value;
					}
					else if (JsEvent.number == 3)
					{
						Gamepad->Buttons[Gamepad_Y].CurState = JsEvent.value;
					}
					else if (JsEvent.number == 4)
					{
						Gamepad->Buttons[Gamepad_LeftShoulder].CurState = JsEvent.value;
					}
					else if (JsEvent.number == 5)
					{
						Gamepad->Buttons[Gamepad_RightShoulder].CurState = JsEvent.value;
					}
					else if (JsEvent.number == 6)
					{
						Gamepad->Buttons[Gamepad_Back].CurState = JsEvent.value;
					}
					else if (JsEvent.number == 7)
					{
						Gamepad->Buttons[Gamepad_Start].CurState = JsEvent.value;
					}
				}

				if (JsEvent.type == 2)
				{
					if (JsEvent.number == 0)
					{
						LeftThumb.x = JsEvent.value;
					}
					if (JsEvent.number == 1)
					{
						LeftThumb.y = -JsEvent.value;
					}
					if (JsEvent.number == 2)
					{
						Gamepad->LeftTrigger = (JsEvent.value+32767)/65534.0f;
					}
					if (JsEvent.number == 3)
					{
						RightThumb.x = JsEvent.value;
					}
					if (JsEvent.number == 4)
					{
						RightThumb.x = -JsEvent.value;
					}
					if (JsEvent.number == 5)
					{
						Gamepad->RightTrigger = (32767-JsEvent.value)/32768.0f;
					}
					if (JsEvent.number == 6)
					{
						if (JsEvent.value < 0)
							Gamepad->Buttons[Gamepad_DpadLeft].CurState = 1;
						else if (JsEvent.value > 0)
							Gamepad->Buttons[Gamepad_DpadRight].CurState = 1;
						else
						{
							Gamepad->Buttons[Gamepad_DpadLeft].CurState = 0;
							Gamepad->Buttons[Gamepad_DpadRight].CurState = 0;
						}
					}
					if (JsEvent.number == 7)
					{
						if (JsEvent.value < 0)
							Gamepad->Buttons[Gamepad_DpadUp].CurState = 1;
						else if (JsEvent.value > 0)
							Gamepad->Buttons[Gamepad_DpadDown].CurState = 1;
						else
						{
							Gamepad->Buttons[Gamepad_DpadUp].CurState = 0;
							Gamepad->Buttons[Gamepad_DpadDown].CurState = 0;
						}
					}
				}
			}
			else
			{
				break;
			}
		}

		if ((XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE*XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) < vec2_length_squared(LeftThumb))
		{
			LeftThumb = vec2_div(LeftThumb, 32768.0f);
		}

		if ((XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE*XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) < vec2_length_squared(LeftThumb))
		{
			RightThumb = vec2_div(RightThumb, 32768.0f);
		}

		Gamepad->LeftThumb = LeftThumb;
		Gamepad->RightThumb = RightThumb;
	}

}
#undef JOY_FILE

static int WindowWidth, WindowHeight;
void handle_resize(int width, int height)
{
	GLsizei w = width;
	GLsizei h = height;
	WindowHeight = h;
	WindowWidth = w;

	GLsizei newh = w*UserAspectRatio;
	GLsizei neww = h/UserAspectRatio;

	Engine.Render->ScreenOrigin = Vec2(0, 0);
	Engine.Render->DesignScreenOrigin = Vec2(0, 0);

	if (newh > h)
	{
		s32 DiffH = newh - h;
		Engine.Render->ScreenOrigin.y = (DiffH/2);
		Engine.Render->DesignScreenOrigin.y = (DiffH/2)*(Engine.Render->DesignSize.y/newh);

		Engine.Render->OpenGL.Viewport(0, -DiffH/2, w, h+DiffH);
	}
	else if (neww > w)
	{
		s32 DiffW = neww - w;
		Engine.Render->ScreenOrigin.x = (DiffW/2);
		Engine.Render->DesignScreenOrigin.x = (DiffW/2)*(Engine.Render->DesignSize.x/neww);
		Engine.Render->OpenGL.Viewport(-DiffW/2, 0, w+DiffW, h);
	}
	else
	{
		Engine.Render->OpenGL.Viewport(0, 0, w, h);
	}
}

static void handle_key_event(XEvent *Event, button_state State)
{
	KeySym Sym;
	char Chars[8] = {};
	int Ret = XLookupString(&Event->xkey, Chars, static_array_len(Chars), &Sym, 0);
	key Key = KeyFromCode(Sym, State);
	if (Ret > 0)
		Key.KeyChar = Chars[0];
	process_key(&Engine, &Key);
}

static b32 Running = 1;

static void platform_exit()
{
	Running = 0;
}

static void platform_switch_to_fullscreen()
{
	Atom window_state = XInternAtom(display, "_NET_WM_STATE", True);

	Atom actual_type_return;
	int actual_format_return;
	unsigned long nitems_return;
	unsigned long bytes_after_return;
	unsigned char *prop_return;
	int status = XGetWindowProperty(display, window, window_state, 0L, sizeof (Atom), False,
                                XA_ATOM, &actual_type_return, &actual_format_return, &nitems_return, 
								&bytes_after_return, &prop_return);

	b32 Delete = 1;
	for (int i=0; i<nitems_return; ++i)
	{
		char *an = XGetAtomName(display, ((Atom*)prop_return)[i]);
		if (strstr(an, "_NET_WM_STATE_FULLSCREEN"))
		{
			Delete = 0;
		}
		XFree(an);
	}

	XEvent e;
	e.xclient.type = ClientMessage;
	e.xclient.window = window;
	e.xclient.display = display;
	e.xclient.message_type = window_state;//_NET_WM_STATE;
	e.xclient.format = 32;
	e.xclient.data.l[0] = Delete;
	e.xclient.data.l[1] = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", True);
	e.xclient.data.l[2] = 0;
	e.xclient.data.l[3] = 0;
	e.xclient.data.l[4] = 1;
	XSendEvent(display, XRootWindow(display, XDefaultScreen(display)), False, SubstructureRedirectMask | SubstructureNotifyMask, &e);
}

void platform_get_config_path(char *Path, u32 Size)
{
	struct passwd *Pwd = getpwuid(getuid());
	snprintf(Path, Size, "%s/.config", Pwd->pw_dir);
}

int main(int argc, char* argv[])
{
	init_x();
	init_sound();

	u32 MemSize = MB(10);
	void *LocalMem = mmap((void*)0x12341234, MemSize, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
	Engine.Memory = memory_init(LocalMem, MemSize, platform_mutex);

	platform Platform;
	Platform.FileReadFully = platform_file_read_fully;
	Platform.FileWriteFully = platform_file_write_fully;
	Platform.FileOpen = platform_file_open;
	Platform.FileRead = platform_file_read;
	Platform.FileClose = platform_file_close;
	Platform.LoadGLFunction = platform_load_gl_function;
	Platform.Log = platform_log;
	Platform.SetWindowSize = platform_set_window_size;
	Platform.HideCursor = platform_hide_cursor;
	Platform.GetLocalTime = platform_get_local_time;
	Platform.SetWindowTitle = platform_set_window_title;
	Platform.GetFileModifiedDate = platform_get_file_modified_date;
	Platform.Exit = platform_exit;
	Platform.SwitchFullScreen = platform_switch_to_fullscreen;
	Platform.Mutex = platform_mutex;
	Platform.ServerStart = platform_server_start;
	Platform.ServerStop = platform_server_stop;
	Platform.ClientJoin = platform_client_join;
	Platform.ClientExit = platform_client_exit;
	Platform.SocketRead = platform_socket_read;
	Platform.SocketWrite = platform_socket_write;
	Platform.ThreadCreate = platform_create_thread;
	Platform.ThreadExit = platform_exit_thread;
	Platform.NetToHost16 = platform_net_to_host_16;
	Platform.NetToHost32 = platform_net_to_host_32;
	Platform.HostToNet16 = platform_host_to_net_16;
	Platform.HostToNet32 = platform_host_to_net_32;
	Platform.GetIp4Address = platform_get_ipv4_address;
	Platform.GetUserName = platform_get_user_name;
	Platform.GetConfigPath = platform_get_config_path;

	Engine.Platform = &Platform;

	render Render = {};
	Engine.Render = &Render;

	for (int i=0; i<GG_GAMEPAD_DEVICES; ++i)
	{
		JoystickFd[i].Id = -1;
	    JoystickFd[i].Fd = -1;
	}

	Engine.Input = &Input;

	imgui ImGui;
	Engine.ImGui = &ImGui;

	net Net = {};
	Engine.Net = &Net;

	engine_init(&Engine);

	render_init(&Engine);
#if DEBUG
	//NOTE: This only works because this string is in the main function
	Engine.AssetDatabase.RootPath = "../../assets";
#else
	Engine.AssetDatabase.RootPath = "assets";
#endif

	gamepad_init();

	load_game_func();

	Atom WmDeleteMessage = XInternAtom(display, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(display, window, &WmDeleteMessage, 1);

	XEvent Event;
	struct timeval Time0;
	struct timeval Time1;
	gettimeofday(&Time0, 0);

	pthread_t SoundThreadId;
	pthread_create(&SoundThreadId, 0, sound_thread, &Engine);

	while (Running)
	{
		gettimeofday(&Time1, 0);
		f32 DeltaTime = (((Time1.tv_sec - Time0.tv_sec)*1000) + ((Time1.tv_usec - Time0.tv_usec)/1000.0))/1000.0f;
		gettimeofday(&Time0, 0);
		Engine.Render->OpenGL.Clear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		while (XPending(display))
		{
			XNextEvent(display, &Event);
			if (Event.type == ConfigureNotify)
			{
				handle_resize(Event.xconfigure.width, Event.xconfigure.height);
			}
			else if (Event.type == MotionNotify)
			{
				int xPos = Event.xmotion.x;
				int yPos = WindowHeight - Event.xmotion.y;
				if (Engine.Render)
				{
					vec2 Origin = Engine.Render->ScreenOrigin;
					vec2 Scale = Vec2(Engine.Render->DesignSize.x / (WindowWidth + Origin.x*2), Engine.Render->DesignSize.y / (WindowHeight + Origin.y*2));
					xPos = xPos * Scale.x;
					xPos += Origin.x * Scale.x;
					yPos = yPos * Scale.y;
					yPos += Origin.y * Scale.y;
				}
				Input.MouseState.MousePos = Vec2(xPos, yPos);
			}
			else if (Event.type == ButtonPress)
			{
				mouse_set_button_state(&Input.MouseState, Event.xbutton.button, ButtonState_Down);
			}
			else if (Event.type == ButtonRelease)
			{
				mouse_set_button_state(&Input.MouseState, Event.xbutton.button, ButtonState_Up);
			}
			else if (Event.type == KeyPress)
			{
				handle_key_event(&Event, ButtonState_Down);
			}
			else if (Event.type == KeyRelease)
			{
				handle_key_event(&Event, ButtonState_Up);
			}
			else if (Event.type == ClientMessage)
			{
				if (Event.xclient.data.l[0] == WmDeleteMessage)
				{
					//TODO: Signal quit and finish all running threads 
					Running = false;
					break;
				}
			}
		}

		handle_gamepad();
		game_update(&Engine, DeltaTime);

		engine_update(&Engine, DeltaTime);
		glXSwapBuffers(display, window);
		Engine.Reloaded = 0;

		{
			struct stat GameSoStatCheck;
			if (stat("lock", &GameSoStatCheck) == -1)
			{
				stat(GameSoPath, &GameSoStatCheck);
				if (GameSoStatCheck.st_mtim.tv_sec > GameSoStatOrig.st_mtim.tv_sec)
				{
					load_game_func();
				}
			}
		}
	}

	//TODO: Close sockets
	pa_mainloop_quit(PAMainLoop, 1);
	pa_context_disconnect(PAContext);
	pa_mainloop_free(PAMainLoop);

	glXMakeCurrent( display, 0, 0 );
	glXDestroyContext( display, ctx );

	XDestroyWindow( display, window );
	XFreeColormap( display, cmap );
	XCloseDisplay( display );

	return 0;
}
