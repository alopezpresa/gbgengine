CompilerFlags=" -O2 -DPLAT_LINUX"
LinkerFlags=" -lGL -lm"

mkdir -p release
gcc -I.. $CompilerFlags linux.c -o release/puyo -lX11 $LinkerFlags -ldl -lpulse 

gcc $CompilerFlags ../game.c -fPIC -shared -Wl,-soname,libgame.so -o release/libgame.so $LinkerFlags


cp -r ../assets release/
#zip -r "missile command.zip" mc run libgame.so ../assets
