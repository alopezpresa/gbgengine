#ifndef GBG_ENGINE_H
#define GBG_ENGINE_H

#include "gbg_assets.h"
#include "gbg_memory.h"

typedef struct engine
{
	struct platform *Platform;
	struct render *Render;
	struct input *Input;
	struct sound_system *SoundSystem;
	struct imgui *ImGui;
	struct net *Net;
	struct world *World;
	struct rtti *Rtti;
	struct editor *Editor;

	asset_database AssetDatabase;
	b32 Initialized;
	b32 Reloaded;

	//Android App lifecycle needs to free some resources when the app goes to a second plane.
	// Those resources need to be taken back, but the game is still in memory.
	b32 HasBeenSuspended;

	memory_partition Memory;

	//Game pointer, so when the game dll gets swapped the game state is not lost
	void *GamePtr;
} engine;

void engine_update(engine *Engine, f32 DeltaTime);
void engine_init(engine *Engine);

#endif
