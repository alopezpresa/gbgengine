#ifndef GBG_WORLD_H
#define GBG_WORLD_H

#include "gbg_math.h"
#include "gbg_rtti.h"

GENUM()
typedef enum rigid_body_type
{
	RB_None,
	RB_Rect,
	RB_Circle,
}rigid_body_type;

GOBJECT()
typedef struct rigid_body
{
	//TODO: It will be great if the editor shows a dropbox with possible usertag definitions
	u32 UserTag;
	rigid_body_type Type;
	union 
	{
		circle Circle;
		rect Rect;
	};
} rigid_body;

rigid_body RigidBody_Circle(vec2 Pos, f32 Rad);
rigid_body RigidBody_Rect(vec2 Pos, vec2 Rad);

GOBJECT(Editor)
typedef struct entity
{
	vec3 Pos;
	vec2 Scale;
	f32 Rot;

	rigid_body RigidBody;
	vec2 Vel;
	b32 IsSensor;

	material Material;

	b32 IsValid;

	named_pointer GameData;
} entity;

void entity_destroy(struct engine *Engine, entity *Entity);
void entity_render(struct engine *Engine, entity *Ent);

void *_entity_get_game_data(entity *Ent, const char *TypeName);

#define entity_get_game_data(Ent, Type)\
	(Type*)_entity_get_game_data((Ent), #Type)

typedef struct collision_info
{
	entity *EntA;
	entity *EntB;
	f32 Distance;
	f32 NormalizedDistance;
	vec2 P;
	vec2 N;
	b32 IsInside;
} collision_info;

b32 collision_vs(collision_info *ColInfo, u32 UserTag1, u32 UserTag2, entity **Ent1, entity **Ent2);

typedef enum world_transition
{
	WorldTransition_None,
	WorldTransition_Play,
	WorldTransition_Stop,
} world_transition;

typedef enum world_state
{
	WorldState_Stop,
	WorldState_Play,
}world_state;

//Entitites are stored in clusters, when a cluster is full a new one is created in a linked list fashion.
typedef struct entity_cluster
{
	entity E[64];
	struct entity_cluster *Next;
} entity_cluster;

typedef enum collision_filter_type
{
	CFT_Ignore,
} collision_filter_type;

typedef struct collision_filter
{
	u32 Tag1;
	u32 Tag2;
	collision_filter_type Type;
} collision_filter;

typedef struct world
{
	char *FileName;
	entity_cluster Entities;
	entity_cluster EntitiesFromEditor;
	collision_info *FrameCollisions;
	collision_filter *CollisionFilters;

	//Transition will govern the current state of the world, stop or playing (pause in a near future)
	//CurrentState should not be set directly.
	//Transition and state will happen in a delayed frame so all other subsytems can be notified from this change.
	world_transition SetTransition;
	world_transition CurrentTransition;
	world_state CurrentState;

	//Everything outside here will be removed
	rect Extent;
} world;

void world_init(struct engine *Engine);
void world_update(struct engine *Engine, f32 DeltaTime);
world_transition world_get_transition(struct engine *Engine);

entity *world_entity_create(struct engine *Engine);
entity *world_entity_create_in_editor(struct engine *Engine);

entity *world_entity_create_from_imprint(struct engine *Engine, struct imprint *Imprint);
entity *world_entity_create_from_imprint_in_editor(struct engine *Engine, struct imprint *Imprint);

void world_clear_level_editor(struct engine *Engine);

typedef void (*query_entity_fn)(struct engine *Engine, struct entity *Entity);
void world_query_entities(struct engine *Engine, u32 RigidBodyUserTag, query_entity_fn Func);

#endif
