#include "gbg_net.h"

void network_update(engine *Engine, f32 DeltaTime)
{
	net *Net = Engine->Net;

	if (Net->State == NetState_Playing)
	{
		if (Net->IsHost)
		{
			Net->SyncTimer -= DeltaTime;
			if (Net->SyncTimer <= 0)
			{
				u8 *SyncMessage = net_message_begin(Engine, NetMessageType_Sync);
				Net->ServerSyncFunc(Engine, &SyncMessage);
				network_message_send(Engine, SyncMessage, Net->Players);
				dynamic_array_destroy(SyncMessage);

				Net->SyncTimer += 1.0f/Net->Frequency;
			}

			if (Net->Players)
			{
				//Is 10 seconds enough?
				if (Engine->Platform->GetLocalTime() - Net->Players->LastTimeSeen > 10)
				{
					Net->Players->Connected = 0;
				}
			}
		}
		else
		{
			if (Engine->Platform->GetLocalTime() - Net->LastMessageSentTime > 1)
			{
				u8 *Msg = net_message_begin(Engine, NetMessageType_KeepAlive);
				network_message_send(Engine, Msg, &Net->Host);
				dynamic_array_destroy(Msg);
			}
		}
	}
}

b32 network_message_ring_read(engine *Engine, net_message *Message)
{
	net_message_ring *MsgRing = &Engine->Net->MessageRing;

	if (MsgRing->ReadHeader == MsgRing->WriteHeader)
	{
		return 0;
	}

	Engine->Platform->Mutex(&MsgRing->Lock);

	*Message = MsgRing->Messages[MsgRing->ReadHeader++];

	if (MsgRing->ReadHeader == static_array_len(MsgRing->Messages))
	{
		MsgRing->ReadHeader = 0;
	}

	MsgRing->Lock = 0;
	return 1;
}

static void network_message_ring_write(engine *Engine, net_message *Message)
{
	net_message_ring *MsgRing = &Engine->Net->MessageRing;

	Engine->Platform->Mutex(&MsgRing->Lock);

	MsgRing->Messages[MsgRing->WriteHeader++] = *Message;

	if (MsgRing->WriteHeader == static_array_len(MsgRing->Messages))
	{
		MsgRing->WriteHeader = 0;
	}

	MsgRing->Lock = 0;
}

static void *server_thread(void *Param)
{
	engine *Engine = (engine*)Param;
	net *Net = Engine->Net;

	for (;;)
	{
		u8 Data[512];
		char Host[64];
		char Port[8];
		s32 BytesRead = Engine->Platform->SocketRead(Net->Socket, Data, sizeof(Data), Host, Port);

		if (BytesRead <= 0)
		{
			//Socket close. Exiting thread
			return 0;
		}

		byte_stream_reader StreamReader = ByteStreamReader(Data, BytesRead);
		u16 MsgSize = byte_stream_read16(Engine, &StreamReader);
		if (BytesRead < MsgSize)
		{
			Engine->Platform->Log("Error reading message, message is too big! %d\n", MsgSize);
			continue;
		}

		net_message_type Type = byte_stream_read8(Engine, &StreamReader);
		if (Type == NetMessageType_ConnectionRequest)
		{
			u8 *ResponseMsg = net_message_begin(Engine, NetMessageType_ConnectionResponse);

			if (Net->State == NetState_WaitingForPlayers)
			{
				Net->State = NetState_Playing;
				net_player IncomingPlayer;
				IncomingPlayer.Address = string_dup(Engine, Host);
				IncomingPlayer.Port = string_dup(Engine, Port);
				IncomingPlayer.Name = byte_stream_read_string(Engine, &StreamReader);
				IncomingPlayer.LastTimeSeen = Engine->Platform->GetLocalTime();
				IncomingPlayer.Connected = 1;
				if (Net->Players == 0)
				{
					Net->Players = dynamic_array_create(Engine, net_player);
				}

				dynamic_array_add(&Net->Players, IncomingPlayer);

				//Accept msg
				net_message_add8(Engine, &ResponseMsg, 1);
				net_message_add_string(Engine, &ResponseMsg, Net->Host.Name);

				Net->SyncTimer = 1.0f/Net->Frequency;
			}
			else
			{
				//Denied msg
				net_message_add8(Engine, &ResponseMsg, 0);
			}

			u32 ResLen = dynamic_array_len(ResponseMsg);
			Engine->Platform->SocketWrite(Net->Socket, ResponseMsg, ResLen, Net->Players->Address, Net->Players->Port);
			dynamic_array_destroy(ResponseMsg);
		}
		else if (Type == NetMessageType_Action)
		{
			//For now we have only ONE player, identify from where is the message coming from
			Net->Players->LastTimeSeen = Engine->Platform->GetLocalTime();
			net_message Msg = {0};
			Msg.Type  = Type;
			Msg.Sync.DataLen = StreamReader.Len - StreamReader.Header;
			Msg.Sync.Data = memory_alloc(&Engine->Memory, Msg.Sync.DataLen);
			for (u32 i=0; i<Msg.Sync.DataLen; ++i)
			{
				Msg.Sync.Data[i] = StreamReader.Data[StreamReader.Header+i];
			}
			network_message_ring_write(Engine, &Msg);
		}
		else if (Type == NetMessageType_KeepAlive)
		{
			if (Net->Players)
			{
				//Dont send this message to client
				Net->Players->LastTimeSeen = Engine->Platform->GetLocalTime();
			}
			else
			{
				//Server has been shutted down, no players array exist anymore
				return 0;
			}
		}
	}
}

b32 network_start_server(engine *Engine, const char *PlayerName, const char *Host, const char *Port, network_server_sync_func ServerSyncFunc)
{
	if (Engine->Platform->ServerStart(Host, Port))
	{
		Engine->Platform->ThreadCreate(server_thread, Engine);
		Engine->Net->IsHost = 1;
		Engine->Net->Frequency = 20;
		Engine->Net->ServerSyncFunc = ServerSyncFunc;
		Engine->Net->Host.Name = string_dup(Engine, PlayerName);
		return 1;
	}

	return 0;
}

void network_stop_server(engine *Engine)
{
	Engine->Platform->ServerStop();
	Engine->Net->IsHost = 0;
	memory_free(&Engine->Memory, Engine->Net->Host.Name);

	if (Engine->Net->Players)
	{
		dynamic_array_destroy(Engine->Net->Players);
		Engine->Net->Players = 0;
		Engine->Net->MessageRing.ReadHeader = 0;
		Engine->Net->MessageRing.WriteHeader = 0;
		Engine->Net->State = 0;
	}
}

static void *network_client_thread(void *Param)
{
	engine *Engine = (engine*)Param;
	net *Net = Engine->Net;
	for(;;)
	{
		u8 Data[512];
		s32 BytesRead = Engine->Platform->SocketRead(Net->Socket, Data, sizeof(Data), 0, 0);

		if (BytesRead <= 0)
		{
			//server is down
			Net->State = NetState_ConnectionLost;
			return 0;
		}

		byte_stream_reader StreamReader = ByteStreamReader(Data, BytesRead);
		u16 Size = byte_stream_read16(Engine, &StreamReader);
		net_message_type Type = byte_stream_read8(Engine, &StreamReader);

		net_message Msg = {0};
		Msg.Type  = Type;
		switch (Type)
		{
			case NetMessageType_ConnectionResponse:
			{
				u8 Response = byte_stream_read8(Engine, &StreamReader);
				Msg.ConnectionResponse.Response = Response;
				Net->Host.Name = string_dup(Engine, byte_stream_read_string(Engine, &StreamReader));
				if (Response)
					Net->State = NetState_Playing;
			} break;

			case NetMessageType_Sync:
			//TODO: I'd really like this alloced memory to automagically frees itself
				Msg.Sync.DataLen = StreamReader.Len - StreamReader.Header;
				Msg.Sync.Data = memory_alloc(&Engine->Memory, Msg.Sync.DataLen);
				for (u32 i=0; i<Msg.Sync.DataLen; ++i)
				{
					Msg.Sync.Data[i] = StreamReader.Data[StreamReader.Header+i];
				}
			break;
			case NetMessageType_Action:
				Msg.Action.DataLen = StreamReader.Len - StreamReader.Header;
				Msg.Action.Data = memory_alloc(&Engine->Memory, Msg.Action.DataLen);
				for (u32 i=0; i<Msg.Action.DataLen; ++i)
				{
					Msg.Action.Data[i] = StreamReader.Data[StreamReader.Header+i];
				}
			break;
		}

		network_message_ring_write(Engine, &Msg);
	}
}

b32 network_join_server(engine *Engine, const char *PlayerName, const char *Host, const char *Port)
{
	b32 Ret = Engine->Platform->ClientJoin(Host, Port);	
	if (Ret == 0)
	{
		Engine->Platform->Log("Error connecting to server %s:%s\n", Host, Port);
		return 0;
	}

	u8 *JoinMsg = net_message_begin(Engine, NetMessageType_ConnectionRequest);
	net_message_add_string(Engine, &JoinMsg, PlayerName);

	u32 Bytes = Engine->Platform->SocketWrite(Engine->Net->Socket, JoinMsg, dynamic_array_len(JoinMsg), Host, Port);

	Engine->Net->LastMessageSentTime = Engine->Platform->GetLocalTime();

	dynamic_array_destroy(JoinMsg);

	Engine->Net->Frequency = 20;
	Engine->Platform->ThreadCreate(network_client_thread, Engine);

	return 1;
}

static void net_message_update_size(engine *Engine, u8 *Msg)
{
	u16 Size = dynamic_array_len(Msg);
	*(u16*)Msg = Engine->Platform->HostToNet16(Size);
}

u8 *net_message_begin(engine *Engine, net_message_type Type)
{
	u8 *Msg = dynamic_array_create(Engine, u8);

	u8 Z = 0;
	//Create some space for the size field
	for (u32 i=0; i<sizeof(u16); ++i)
	{
		dynamic_array_add(&Msg, Z);
	}

	//Add msg type
	u8 T8 = Type;
	dynamic_array_add(&Msg, T8);

	net_message_update_size(Engine, Msg);

	return Msg;
}

void net_message_add8(engine *Engine, u8 **Message, u8 Value)
{
	dynamic_array_add(Message, Value);
	net_message_update_size(Engine, *Message);
}

static void byte_array_add_n_bytes(u8 **Data, u8* Bytes, u32 Len)
{
	for (u32 i=0; i<Len; ++i)
	{
		dynamic_array_add(Data, Bytes[i]);
	}
}

static void net_message_add_n_bytes(engine *Engine, u8 **Message, u8* Bytes, u32 Len)
{
	byte_array_add_n_bytes(Message, Bytes, Len);
	net_message_update_size(Engine, *Message);
}

void net_message_add16(engine *Engine, u8 **Message, u16 Value)
{
	Value = Engine->Platform->HostToNet16(Value);
	net_message_add_n_bytes(Engine, Message, (u8*)&Value, sizeof(Value));
}

void net_message_add32(engine *Engine, u8 **Message, u32 Value)
{
	Value = Engine->Platform->HostToNet32(Value);
	net_message_add_n_bytes(Engine, Message, (u8*)&Value, sizeof(Value));
}

static void byte_array_add_string(engine *Engine, u8 **DynArray, const char *Str)
{
	u16 Len = string_len(Str);
	u16 HostLen = Engine->Platform->HostToNet16(Len);
	byte_array_add_n_bytes(DynArray, (u8*)&HostLen, sizeof(HostLen));

	for (u32 i=0; i<Len; ++i)
	{
		dynamic_array_add(DynArray, Str[i]);
	}
}

void net_message_add_string(engine *Engine, u8 **Message, const char *Str)
{
	byte_array_add_string(Engine, Message, Str);
	net_message_update_size(Engine, *Message);
}

byte_stream_reader ByteStreamReader(const u8 *Data, u32 Len)
{
	byte_stream_reader Ret = {0};
	Ret.Data = Data;
	Ret.Len = Len;

	return Ret;
}

u8 byte_stream_read8(engine *Engine, byte_stream_reader *Reader)
{
	assert(Reader->Header + 1 <= Reader->Len);
	return *(Reader->Data + Reader->Header++);
}

u16 byte_stream_read16(engine *Engine, byte_stream_reader *Reader)
{
	assert(Reader->Header + 2 <= Reader->Len);
	u16 Ret = Engine->Platform->NetToHost16(*(u16*)(Reader->Data + Reader->Header));
	Reader->Header += 2;
	return Ret;
}

u32 byte_stream_read32(engine *Engine, byte_stream_reader *Reader)
{
	assert(Reader->Header + 4 <= Reader->Len);
	u32 Ret = Engine->Platform->NetToHost32(*(u32*)(Reader->Data + Reader->Header));
	Reader->Header += 4;
	return Ret;
}

b32 byte_stream_try_read8(engine *Engine, byte_stream_reader *Reader, u8 *OutValue)
{
	if (Reader->Header + 1 <= Reader->Len)
	{
		*OutValue = *(Reader->Data + Reader->Header++);
		return 1;
	}

	return 0;
}

b32 byte_stream_try_read16(engine *Engine, byte_stream_reader *Reader, u16 *OutValue)
{
	if (Reader->Header + 1 <= Reader->Len)
	{
		*OutValue = byte_stream_read16(Engine, Reader);
		return 1;
	}

	return 0;
}

b32 byte_stream_try_read32(engine *Engine, byte_stream_reader *Reader, u32 *OutValue)
{
	if (Reader->Header + 1 <= Reader->Len)
	{
		*OutValue = byte_stream_read32(Engine, Reader);
		return 1;
	}

	return 0;
}

//TODO: Should I use a string buffer instead of allocating a new string?
char *byte_stream_read_string(engine *Engine, byte_stream_reader *Reader)
{
	u16 Size = 0;
	char *Ret = 0;
	if (byte_stream_try_read16(Engine, Reader, &Size))
	{
		assert(Reader->Header + Size <= Reader->Len);
		Ret = memory_alloc(&Engine->Memory, Size+1);
		for (u32 i=0; i<Size; ++i)
		{
			Ret[i] = *(Reader->Data + Reader->Header++);
		}
		Ret[Size] = 0;
	}

	return Ret;
}

u32 network_message_send(engine *Engine, u8 *Message, net_player *Player)
{
	u32 Len = dynamic_array_len(Message);

	Engine->Net->LastMessageSentTime = Engine->Platform->GetLocalTime();
	u32 Sent = Engine->Platform->SocketWrite(Engine->Net->Socket, Message, Len, Player->Address, Player->Port);
	return Sent;
}

byte_stream_writer ByteStreamWriter(struct engine *Engine)
{
	byte_stream_writer Stream = {0};
	Stream.Data = dynamic_array_create(Engine, u8);

	return Stream;
}

void byte_stream_destroy(byte_stream_writer *Stream)
{
	dynamic_array_destroy(Stream->Data);
	Stream->Data = 0;
}

void byte_stream_write8(engine *Engine, byte_stream_writer *Stream, u8 Value)
{
	dynamic_array_add(&Stream, Value);
}

void byte_stream_write16(engine *Engine, byte_stream_writer *Stream, u16 Value)
{
	Value = Engine->Platform->HostToNet16(Value);
	byte_array_add_n_bytes(&Stream->Data, (u8*)&Value, sizeof(Value));
}

void byte_stream_write32(engine *Engine, byte_stream_writer *Stream, u32 Value)
{
	Value = Engine->Platform->HostToNet32(Value);
	byte_array_add_n_bytes(&Stream->Data, (u8*)&Value, sizeof(Value));
}

void byte_stream_write_string(engine *Engine, byte_stream_writer *Stream, const char *String)
{
	byte_array_add_string(Engine, &Stream->Data, String);
}
