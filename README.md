# README #
Follow me on [Twitter](https://twitter.com/GbGEngine)

## Version 0.3 ##
Finally [Puyo online](https://nesdavid.itch.io/puyo-online) was released and that means a full engine version as well.
## Added Features to version 0.3 ##
### Project & Tools ###
* Added scripts to setup an initial project
* Added Compressonator and Mali tool

### Render ###
* Supports DXT1 texture
* Supports ETC_RGB8 texture (for android)
* Sprites have z-order
* Texture batchs for alpha sorting 

### Utils ###
* Added *array_add_at* 
* Added *string_compare*
* Added *string_copy*
* Byte stream reader and writer

### ImGui ###
* Buttons and Sliders can be images
* Button can play mouse over sounds
* Edit text widget

### Assets ###
* Collision handling

### Sound ###
* Audio has its own thread to mix sound

### Platform ###
* Mutexes
* Thread start/stop functions
* GetUserName
* GetconfigPath
* Android: Suspend bool for checking android life cycle
* Android: Redefine back key behavior
* Android: Added Java Activity and Application
* Android: Show soft keyboard capability

### Network ###
* New System!
* UDP sockets
* Host and client concept
* Send and receive functions
* Sync frequency for sending data

## Version 0.26 ##
Another mid term release, from this point on the game won't be shipped in the same source code as the engine. The game is two players complete and can be downloaded from here: [Puyo Online](https://nesdavid.itch.io/puyo-online)
## Added Features to version 0.26 ##
### Render ###
* Add DesignScreenOrigin to know the screen corners
* Added blend mode, "normal" or "additive"

### Sound ###
* Streaming from disk
* Loop and set/get Gain

### Input ###
* Player Controller concept, it handles their own keyboard input

### Platform ###
* Open/Read/Close files operations
* Fullscreen support and Exit command
* Extend screen size and keep aspect ratio
* Win64: Added keycodes from 0-9 , A-Z

### IMGUI ###
* Labels, Buttons and Slider 
* Control buttons with keyboard and gamepad

### Misc. ###
* Math: clamp function


## Version 0.25 ##
Mid term release. The game is still WIP and you can download it from [Puyo Online](https://nesdavid.itch.io/puyo-online)
## Added Features to version 0.25 ##

### Render ###
* Compressed textures support. DXT5 for Win64 & Linux. ETC2 for Android
* BM Font Support
* Basic Sprite functionality. Rotate, scale and tint sprites
* Draw all sprites with one drawcall
* Alpha sorting
* Particle system (WIP)

### Sound ###
* All platforms uses the same sound mixer.
* Win64 uses a double buffer and only one XAudio2 VoiceSource

### Input ###
* Array of key events and motion events (touchscreen)
* Multiple gamepads in same PC support
* Win64 also handle joysticks with DInput
* Added Keycodes for keyboard support

### Assets ###
* Hot reload for textures
* Iterate over assets of type function

### Platform ###
* Added SetWindowTitle

### Math ###
* Several vec functions and convertions
* Added mat4 vec4 multiplication
* Vec2 random
* Vec2 lerp

### Misc ###
* Fixed Memory leaks
* Dynamic Arrays
* Random is not longer global and multiple instances can be created
* Quicksort

## Version 0.2 ##
You can also download the game made with version 0.2 [Missile Command](https://nesdavid.itch.io/missile-command)

Game is available for Windows, Linux & Android
## Added Features to version 0.2 ##
### Platform ###
* Added Linux & Android support
### Render ###
* Support for OpenGL ES 3.0
* It can render meshes
* Uses only a big vertex and element array
### Memory management ###
* Block memory allocator
### Input ###
* Gamepad, xbox controller mappins
### Sound ###
* Window: XAudio2
* Linux: PulseAudio
* Android: OpenSL
* Linux and Android both handle a buffer mix.
* Window uses XAudio2 internal mixing system
### Misc ###
* Added vectors and random generator
* Lerp functions
* Build defines per platform
* Some utility scripts for building for each platform

## Version 0.1 ##
You can also download the game made with version 0.1 [TIC TAC TOE](https://nesdavid.itch.io/tic-tac-toe)

## Features ##
### Render ###
* OpenGL
* Ortographic projection
* Draw lines only
* Font render by lines xD

### Platform ###
* Windows only
* Log to visual studio output
* Read files
* Creation and window handling (no fullscren support yet)

### Memory management ###
* None, everything is static, for now xD

### Input ###
* Mouse
* Keyboard, only characters

### Sound ###
* Not yet

## How to compile ##
* TBD
