#ifndef GBG_STRING_H
#define GBG_STRING_H

#include "gbg_engine.h"
#include "gbg_memory.h"
#include <stdarg.h>

/**
 * Every char * means utf8.
 *
 * Win platform uses wchar (utf16), the engine will transform between those standards,
 * the platform will receive utf8 and transform it for internal use and return utf8.
 *
 * Currently investigating how to use utf8 in win32, it can be done since 2019 with an xaml configuration: https://learn.microsoft.com/en-us/windows/apps/design/globalizing/use-utf8-code-page
 *
 * The problem seems to be with IShellItem which is only utf16. So, altough everything is changed to use the Ascii functions, where possible, that Shell API don't have an utf8 counterpart so were are still in an hybrid state.
 *
 * Linux should use utf8 as the set locale
 *
 * Android?
 */

u32 string_sid(const char *Name);
u32 string_len(const char *Name);
void string_copy(const char *Source, char *Dest);
void string_strip(char *Str, u32 Len);
char *string_dup(engine *Engine, const char *Name);
char *string_new(engine *Engine, const char *Fmt, ...);
char *string_new_v(engine *Engine, const char *Fmt, va_list vargs);

void string_delete(engine *engine, char *Str);

char *string_substr(struct engine *Engine, const char *Source, u32 From, u32 To);

b32 string_compare(const char *Str1, const char *Str2);

b32 string_is_alpha(char C);

s32 string_index_of(const char *Str, char C);
s32 string_reverse_index_of(const char *Str, char C);

//Helper to write/red strings to/from file
u32 write_string(struct engine *Engine, file_handle File, char *Str, u32 Len, u32 From);
u32 read_string(struct engine *Engine, file_handle File, u32 ReadFrom, char **OutString);

#endif
