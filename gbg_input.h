#ifndef GBG_INPUT_H
#define GBG_INPUT_H

#include "gbg_math.h"

#ifdef PLAT_WIN64
#include "win64/keycodes.h"
#elif defined(PLAT_LINUX)
#include "linux/keycodes.h"
#elif defined(PLAT_ANDROID)
#include "keycodes.h"
#endif

#define GG_GAMEPAD_DEVICES 2

//Stolen from XIpnut. 
//TODO: support more than one gamepad type
typedef enum GamepadButtons
{
	Gamepad_None,
	Gamepad_DpadUp,        
	Gamepad_DpadDown,
	Gamepad_DpadLeft,
	Gamepad_DpadRight,
	Gamepad_Start,
	Gamepad_Back,
	Gamepad_LeftThumb,
	Gamepad_RightThumb,
	Gamepad_LeftShoulder,
	Gamepad_RightShoulder,
	Gamepad_A,
	Gamepad_B,
	Gamepad_X,
	Gamepad_Y,
	Gamepad_Max,
}GamepadButtons;

typedef enum button_state
{
	ButtonState_Up,
	ButtonState_Down,

	ButtonState_Max
}button_state;

typedef struct button
{
	button_state PrevState;
	button_state CurState;
}button;

typedef struct mouse_state
{
	button LeftButton;
	button RightButton;
	vec2 MousePos;
} mouse_state;

typedef struct motion_event
{
	button_state State;
	vec2 Pos;
} motion_event;

typedef struct key
{
	enum key_code KeyCode;
	//Unicode utf8 character
	char KeyChar[4];
	button_state State;
} key;

key KeyFromCode(enum key_code KeyCode, button_state State);

typedef struct gamepad
{
	u32 Id;
	b32 IsConnected;
	vec2 LeftThumb;
	vec2 RightThumb;
	f32 LeftTrigger;
	f32 RightTrigger;
	button Buttons[Gamepad_Max];
} gamepad;

typedef struct player_controller
{
	key *Keys;
	enum key_code *Allowed;
	vec2 Axis;
} player_controller;

player_controller *input_get_or_create_player_controller(struct engine *Engine, u32 Index);

typedef struct input
{
	mouse_state MouseState;
	key *Keys;
	gamepad Gamepads[2]; //Support only 2 pads for now
	motion_event *MotionEvents;

	player_controller *PlayerControllers;
} input;

void input_init(engine *Engine);
void input_update(engine *Engine);

void input_at_end_step(engine *Engine);
b32 input_was_button_clicked(button *Button);
b32 input_was_button_down(button *Button);

#endif
