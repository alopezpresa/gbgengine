#ifndef GBG_MEMORY_H
#define GBG_MEMORY_H

#include "gbg_types.h"
#include "gbg_platform.h"

#define DEBUG_MEMORY 0

typedef struct memory_partition
{
	u32 MaxSize;
	u8 *Data;

	b32 Lock;
	PLATFORM_MUTEX Mutex;
#if DEBUG_MEMORY
	PLATFORM_LOG DebugLog;		
#endif

} memory_partition;

memory_partition memory_init(u8* Ptr, u32 Size, PLATFORM_MUTEX MutexFunc);
void *memory_alloc(memory_partition *Mem, u32 Size);
void memory_free(memory_partition *Mem, void *Ptr);

void *memory_realloc(memory_partition *Mem, void *Ptr, u32 NewSize);

#define memory_alloc_type(Mem, Type) ((Type*)memory_alloc(Mem, sizeof(Type)))

#endif
