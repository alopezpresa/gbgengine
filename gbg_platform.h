#ifndef GBG_PLATFORM_H
#define GBG_PLATFORM_H

#include "gbg_types.h"

typedef struct file_handle
{
	union
	{
		void *Handle;
		s32 Descriptor;	
	};
}file_handle;

typedef enum file_picker_mode
{
	FP_Open,
	FP_Save,
} file_picker_mode;

#ifdef PLAT_ANDROID
typedef struct android_platform
{
	b32 OverrideBackBehavior;
} android_platform;
#endif

typedef u8* (*PLATFORM_FILE_READ_FULLY)(const char *Name, u32 *Size);
typedef file_handle (*PLATFORM_FILE_OPEN)(const char *Name);
typedef file_handle (*PLATFORM_FILE_CREATE)(const char *Name);
typedef u32 (*PLATFORM_FILE_READ)(file_handle FileHandle, u8 *Buffer, u32 BufferLen, u32 From);
typedef void (*PLATFORM_FILE_CLOSE)(file_handle FileHandle);
typedef u32 (*PLATFORM_FILE_WRITE)(file_handle FileHandle, u8 *Content, u32 Size, u32 From);
typedef void (*PLATFORM_FILE_WRITE_FULLY)(const char *Name, u8 *Content, u32 Size);
typedef u64 (*PLATFORM_GET_FILE_MODIFIED_DATE)(const char *Name);
typedef void* (*PLATFORM_LOAD_GL_FUNCTION)(const char *Name);
typedef void (*PLATFORM_LOG)(const char *Fmt, ...);
typedef void (*PLATFORM_SET_WINDOW_SIZE)(u32 Width, u32 Height);
typedef void (*PLATFORM_HIDE_CURSOR)();
typedef void (*PLATFORM_SET_WINDOW_TITLE)(const char *Title);
typedef void (*PLATFORM_SWITCH_FULLSCREEN)();

typedef b32 (*PLATFORM_DIRECTORY_CREATE)(const char *Path);

typedef void (*PLATFORM_SET_VOLUME)(f32 Volume);

//Milliseconds precision
typedef f64 (*PLATFORM_GET_LOCAL_TIME)();

typedef void (*PLATFORM_GET_CONFIG_PATH)(char *Path, u32 Size);

typedef void (*PLATFORM_EXIT)();

typedef void (*PLATFORM_MUTEX)(b32 *Lock);

struct net_address_info;
typedef b32 (*PLATFORM_SERVER_START)(const char *Host, const char *Port);
typedef void (*PLATFORM_SERVER_STOP)();
typedef b32 (*PLATFORM_CLIENT_JOIN)(const char *Host, const char *Port);
typedef void (*PLATFORM_CLIENT_EXIT)();
//OutAddress can be null
typedef s32 (*PLATFORM_SOCKET_READ)(file_handle SocketHandle, u8 *Data, u32 DataLen, char *OutHost, char *OutPort);
typedef s32 (*PLATFORM_SOCKET_WRITE)(file_handle SocketHandle, u8 *Data, u32 DataLen, const char *Host, const char *Port);
typedef u16 (*PLATFORM_NET_TO_HOST_16)(u16 NetShort);
typedef u32 (*PLATFORM_NET_TO_HOST_32)(u32 NetInt);
typedef u16 (*PLATFORM_HOST_TO_NET_16)(u16 HostShort);
typedef u32 (*PLATFORM_HOST_TO_NET_32)(u32 HostInt);

typedef void (*PLATFORM_GET_IPV4)(char *Buffer, u32 BufferLen);

typedef void (*PLATFORM_THREAD_CREATE)(void *(*Func)(void *Param), void *Param);
typedef void (*PLATFORM_THREAD_EXIT)();

typedef void (*PLATFORM_GET_USER_NAME)(char *UserName, u32 Len);

typedef void (*PLATFORM_KEYBOARD_SHOW)();
typedef void (*PLATFORM_KEYBOARD_HIDE)();

typedef void (*PLATFORM_OPEN_URL)(char *Url);

//Find files operation
typedef b32 (*PLATFORM_LIST_FILE_START)(char *Path);
typedef char *(*PLATFORM_LIST_FILE_NEXT)();

typedef char *(*PLATFORM_FILE_DIALOG_PICK)(char *Path, file_picker_mode Mode, char *Description, char *Extension);

typedef struct platform
{
	PLATFORM_FILE_READ_FULLY FileReadFully;
	PLATFORM_FILE_OPEN FileOpen;
	PLATFORM_FILE_CREATE FileCreate;
	PLATFORM_FILE_READ FileRead;
	PLATFORM_FILE_CLOSE FileClose;
	PLATFORM_FILE_WRITE FileWrite;
	PLATFORM_FILE_WRITE_FULLY FileWriteFully;

	PLATFORM_DIRECTORY_CREATE DirectoryCreate;

	PLATFORM_LOAD_GL_FUNCTION LoadGLFunction;
	PLATFORM_LOG Log;
	PLATFORM_SET_WINDOW_SIZE SetWindowSize;
	PLATFORM_SET_WINDOW_TITLE SetWindowTitle;
	PLATFORM_SWITCH_FULLSCREEN SwitchFullScreen;
	PLATFORM_HIDE_CURSOR HideCursor;
	PLATFORM_GET_LOCAL_TIME GetLocalTime;
	PLATFORM_GET_FILE_MODIFIED_DATE GetFileModifiedDate;

	PLATFORM_EXIT Exit;

	PLATFORM_MUTEX Mutex;

	PLATFORM_SERVER_START ServerStart;
	PLATFORM_SERVER_STOP ServerStop;
	PLATFORM_CLIENT_JOIN ClientJoin;
	PLATFORM_CLIENT_EXIT ClientExit;
	PLATFORM_SOCKET_READ SocketRead;
	PLATFORM_SOCKET_WRITE SocketWrite;
	PLATFORM_NET_TO_HOST_16 NetToHost16;
	PLATFORM_NET_TO_HOST_32 NetToHost32;
	PLATFORM_HOST_TO_NET_16 HostToNet16;
	PLATFORM_HOST_TO_NET_32 HostToNet32;

	PLATFORM_GET_IPV4 GetIp4Address;

	PLATFORM_THREAD_CREATE ThreadCreate;
	PLATFORM_THREAD_EXIT ThreadExit;

#undef GetUserName
	PLATFORM_GET_USER_NAME GetUserName;
	PLATFORM_GET_CONFIG_PATH GetConfigPath;

	PLATFORM_KEYBOARD_SHOW KeyboardShow;
	PLATFORM_KEYBOARD_HIDE KeyboardHide;

	PLATFORM_OPEN_URL OpenUrl;

	PLATFORM_FILE_DIALOG_PICK FilePicker;

#ifdef PLAT_ANDROID
	android_platform Android;
#endif

	PLATFORM_LIST_FILE_START ListFileStart;
	PLATFORM_LIST_FILE_NEXT ListFileNext;

} platform;

#endif
