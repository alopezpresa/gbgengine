#include "gbg_engine.h"
#include "gbg_render.h"
#include "gbg_sound.h"
#include "gbg_imgui.h"
#include "gbg_net.h"
#include "gbg_world.h"
#include "gbg_editor.h"

void engine_update(engine *Engine, f32 DeltaTime)
{
	input_update(Engine);
	world_update(Engine, DeltaTime);
	asset_update(Engine);
	render_update(Engine);
	imgui_update(Engine);
	input_at_end_step(Engine);
	network_update(Engine, DeltaTime);

	Engine->HasBeenSuspended = 0;
}

void engine_init(engine *Engine)
{
	rtti_init(Engine);
	input_init(Engine);
	asset_init(Engine);
	sound_init(Engine);
	world_init(Engine);
	editor_init(Engine);
}
